/****************************************************************************
FileName:     userInput.h
Dependencies:
Processor:
Hardware:
Complier:     
Company:     


 Author				Date			Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                 
 *****************************************************************************
General description:





usage:

 ****************************************************************************
Public Functions:

 ****************************************************************************/
#ifndef __USER_INPUT_H
#define __USER_INPUT_H


/****************************************************************************
                        Includes
 ****************************************************************************/
#include <stdint.h>    
/****************************************************************************
                        DEFINES AND ENUMARATED VALUES
 *****************************************************************************/

/****************************************************************************
                        GLOBAL TYPEDEFS
 *****************************************************************************/
typedef struct
{
    uint8_t counter;             //counter status of the pin
    uint8_t debouncedState;        // The currently debounced state of the pin
    uint8_t prevDebouncedState;    // The previous debounced state of the pin
    uint8_t changed;               // The pins that just changed debounced state
    uint8_t inActiveState;         //Pullups or pulldowns are being used
}_tDEBOUNCED_INPUT;
/****************************************************************************
                        MACROS
 *****************************************************************************/

/****************************************************************************
                        GLOBAL FUNCTION PROTOTYPES
 *****************************************************************************/
uint8_t ButtonPressed( _tDEBOUNCED_INPUT *pin );
void debounceInput( _tDEBOUNCED_INPUT *pin, uint8_t rawInput);
void initDebounce( _tDEBOUNCED_INPUT *pin, uint8_t initalState);
/****************************************************************************
                        GLOBAL VARIABLES
 ******************************************************************************/

#endif	/* __BUTTON_DEBOUNCE_H */
