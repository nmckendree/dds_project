/****************************************************************************
FileName:     Serial_Buffer.c
Dependencies:
Processor:     
Hardware:      
Complier:       
Company:
Description:


To Use:                                  
                                                       
Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                         
****************************************************************************
                            Includes
****************************************************************************/    
#include <stdint.h>
#include "serial_buffer.h"  

/****************************************************************************
                            DEFINES
****************************************************************************/
#byte TXREG1 = 0xFAD 
#byte RCREG1 = 0xFAE 
#byte RCSTA1 = 0xFAB       
/****************************************************************************
                            TYPEDEFS AND ENUMS                    
****************************************************************************/
typedef uint16_t buf_pos_t;

/* KEEP THE BUFFER SIZES A POWER OF 2 FOR EFFICIENCY. THE PUT AND GET
FUNCTIONS USE A MODULO OPERATION TO CHECK IF INDEX PAST MAX LENGTH. IF POWER OF 2
THEN THE OPERATION WILL BE OPTIMIZED AS AN AND OPERATION */
//OVERRIDE THESE SETTINGS BY DEFINING IN A SYSTEM LEVEL H FILES SUCH AS SYSTEM.H OR HARDWAREPROFILE.H
#define SERIAL_PC_RX_SZ     32               
#define SERIAL_PC_TX_SZ     32              

typedef struct
{
    buf_pos_t head;
    buf_pos_t tail;
    buf_pos_t buf_len;
    uint8_t inUse;
}_sSER_BUFF_CTL;

typedef struct
{
    _sSER_BUFF_CTL Buf_Ctl; 
    uint8_t *buffer;
    uint8_t uartNum;            
}_tSPORT_DATA;  

/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/
static uint8_t RX_Buf_Debug[SERIAL_PC_RX_SZ];
static uint8_t TX_Buf_Debug[SERIAL_PC_TX_SZ];                                              
static _tSPORT_DATA SerialPortData[SPORT_ENUM_MAX];                       
/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/
void SerialInit(void);
uint16_t serial_Add2Buffer( SPORT_ENUM port, char *data, uint16_t byteCnt);
uint16_t serial_GetFromBuffer( SPORT_ENUM port, char *data, uint16_t byteCnt);
static void uart_TxInterrupt(SPORT_ENUM port, uint8_t priority_lvl, uint8_t Enabled);
void serial_TxBuffEmpty( SPORT_ENUM port );
uint8_t serial_IsTXEmpty( SPORT_ENUM port );
uint8_t serial_IsTXBusy( SPORT_ENUM port );
uint8_t serial_StartTX( SPORT_ENUM port );                     
extern void app_serial_rx_callback(uint8_t c);
              
/*********************************************************************
Function:       SerialInit
Input:          void
Output:         void
Dependencies:
Description:    Initialize all serial buffer data elements and dma channels
 ********************************************************************/
void SerialInit(void)
{
    // SET UP SERIAL RX BUFFERS FIRST
    SerialPortData[SERIAL_PC_RX].Buf_Ctl.head = 0;  
    SerialPortData[SERIAL_PC_RX].Buf_Ctl.tail = 0;
    SerialPortData[SERIAL_PC_RX].Buf_Ctl.buf_len = SERIAL_PC_RX_SZ;
    SerialPortData[SERIAL_PC_RX].buffer = RX_Buf_Debug;
    SerialPortData[SERIAL_PC_RX].Buf_Ctl.inUse = false;

    //SET UP TX BUFFERS NOW                  
    SerialPortData[SERIAL_PC_TX].Buf_Ctl.head = 0;
    SerialPortData[SERIAL_PC_TX].Buf_Ctl.tail = 0;
    SerialPortData[SERIAL_PC_TX].Buf_Ctl.buf_len = SERIAL_PC_TX_SZ;
    SerialPortData[SERIAL_PC_TX].buffer = TX_Buf_Debug;
    SerialPortData[SERIAL_PC_TX].Buf_Ctl.inUse = false;


} /* End SerialInit() */



/*********************************************************************
Function:       serial_Add2Buffer
Input:          SPORT_ENUM port: serial port buffer to use.
                uint8_t *data:  pointer to the data stream to be transmitted.
                uint16_t byteCnt: Number of byte to transfer to buffer.
Output:         uint16_t: zero if error / else byteCnt
Dependencies:
Description:    External interface to add data into the buffer
 ********************************************************************/
uint16_t serial_Add2Buffer( SPORT_ENUM port, char *data, uint16_t byteCnt)
{
    volatile uint16_t retVal = 0;
    //verify port array first
    if ( port >= SPORT_ENUM_MAX)
    {
      return(retVal);    //bad array index
    }
    while(retVal < byteCnt)
    {
        buf_pos_t next_head = (SerialPortData[port].Buf_Ctl.head + 1) % SerialPortData[port].Buf_Ctl.buf_len;
        /* there is room */
        SerialPortData[port].buffer[SerialPortData[port].Buf_Ctl.head] = data[retVal];
        SerialPortData[port].Buf_Ctl.head = next_head;
        retVal++;
    }
   return(retVal);
}  /* End serial_Add2Buffer() */

/*********************************************************************
Function:       serial_GetFromBuffer
Input:          SPORT_ENUM port: serial port buffer to use.
                uint8_t *data:  pointer to the data stream to be transmitted.
                uint16_t byteCnt: Number of byte to transfer to buffer.
Output:         uint16_t: zero if error / else byteCnt
Dependencies:
Description:    External interface to get data from the buffer
 ********************************************************************/
uint16_t serial_GetFromBuffer( SPORT_ENUM port, char *data, uint16_t byteCnt)
{
   uint16_t  retVal = 0;

   //verify port array first
   if ( port >= SPORT_ENUM_MAX)
   {
      return(retVal);    //bad array index
   }                                  
   if(data != (void*)0)                
   {
        while(retVal < byteCnt)
        {
            if (SerialPortData[port].Buf_Ctl.head != SerialPortData[port].Buf_Ctl.tail)
            {
                data[retVal] = SerialPortData[port].buffer[SerialPortData[port].Buf_Ctl.tail];
                SerialPortData[port].Buf_Ctl.tail = (SerialPortData[port].Buf_Ctl.tail + 1) % SerialPortData[port].Buf_Ctl.buf_len;
                retVal++;
            }                                                           
            else
            {
                break;
            }
        }
   }      

   return(retVal);
}  /* End serial_GetFromBuffer() */

/*---------------------------------------------------------------------
  Function Name: serial_StartTX()
  Description:   Start the transmission process and sets TX_BUSY flag.

  Inputs:        SPORT_ENUM port: serail port buffer to use.
  Returns:       uint8_t true if success
 -----------------------------------------------------------------------*/
uint8_t serial_StartTX( SPORT_ENUM port )
{                                   
   uint8_t retval = 0;
   //uint8_t bytecnt;
   char data;

   if( SerialPortData[port].Buf_Ctl.inUse == 0)
   {
      SerialPortData[port].Buf_Ctl.inUse  = 1;

      //port is not busy so load TXREG and enable interrupt
      serial_GetFromBuffer(port, &data, 1 );
      TXREG1 = data;   
      uart_TxInterrupt(port, 3, 1);

   } /* end if() */
   return(retval);
}  /* End serial_StartTX() */

/*********************************************************************
Function:       uart_TxInterrupt
Input:          SPORT_ENUM port - serial buffer structure index
                uint8_t priority_lvl: priority level of isr
                uint8_t Enabled: Enable (true) / disable (false)
Output:         void
Dependencies:
Description:    Function called to enable TX interrupt for serial channel.
 ********************************************************************/
static void uart_TxInterrupt(SPORT_ENUM port, uint8_t priority_lvl, uint8_t Enabled)
{                   
    if(port == SERIAL_PC_TX)
    {                  
       if( Enabled == 0)
       {
          disable_interrupts(int_tbe);   
       }        
       else
       {
          if( interrupt_enabled(int_tbe) == 0 )
           {
              clear_interrupt(int_tbe);
              enable_interrupts(int_tbe);        // enable Tx when first transmission starts    
           }
       }                     
    }   
}  /* end uart_RxIsrHandler() */
                                      
/*********************************************************************
Function:       serial_TxBuffEmpty                            
Input:          SPORT_ENUM port - serial buffer structure index
Output:         void
Dependencies:
Description:    "Empties" the buffer
 ********************************************************************/
void serial_TxBuffEmpty( SPORT_ENUM port )
{
   SerialPortData[port].Buf_Ctl.inUse = false;
   SerialPortData[port].Buf_Ctl.head = SerialPortData[port].Buf_Ctl.tail;
}  /* End serial_TxBuffEmpty() */

/*********************************************************************
Function:       serial_IsTXEmpty
Input:          SPORT_ENUM port - serial buffer structure index
Output:         uint8_t: true if empty, false if not
Dependencies:
Description:    Tests if buffer is empty
 ********************************************************************/
uint8_t serial_IsTXEmpty( SPORT_ENUM port )
{
    return (( SerialPortData[port].Buf_Ctl.head == SerialPortData[port].Buf_Ctl.tail));
}  /* End serial_IsTXBusy() */

/*********************************************************************
Function:       serial_IsTXBusy
Input:          SPORT_ENUM port - serial buffer structure index
Output:         uint8_t: true if busy/in use, false if not
Dependencies:
Description:    tests if the serial port is busy transmitting
 ********************************************************************/
uint8_t serial_IsTXBusy( SPORT_ENUM port )
{
   return(SerialPortData[port].Buf_Ctl.inUse);
}  /* End serial_IsTXBusy() */


/*******************************************************************************
***************************  INTERRUPTS  ***************************************
*******************************************************************************/


/*********************************************************************
Function:       RDA_isr --- Uart1 RX ISR
Input:          void
Output:         void
Dependencies:
Description:    Receive handler for the uart
********************************************************************/      
#INT_RDA                  
void  RDA_isr(void) 
{
    uint8_t c;               
    uint16_t uart_status = 0;
                                        
    uart_status = RCSTA1;         
    {                                 
        c = RCREG1;
        clear_interrupt(int_rda);          
        serial_Add2Buffer(SERIAL_PC_RX, (char*)&c , 1);
    }                             
}  
                                  
                               
/*******************************************************************************
********************  TX INTS FOR NON DMA OPERATION ****************************
*******************************************************************************/

/*********************************************************************
Function:    TBE_isr   
Input:
Output:
Dependencies:                                                  
Description:
********************************************************************/                                         
#INT_TBE
void  TBE_isr(void) 
{                                           
    int8_t Txdata = 0;        
    clear_interrupt(int_tbe);                                      
                                         
       
    if (SerialPortData[SERIAL_PC_TX].Buf_Ctl.head != SerialPortData[SERIAL_PC_TX].Buf_Ctl.tail)
    {                
        Txdata = SerialPortData[SERIAL_PC_TX].buffer[SerialPortData[SERIAL_PC_TX].Buf_Ctl.tail];
        SerialPortData[SERIAL_PC_TX].Buf_Ctl.tail = (SerialPortData[SERIAL_PC_TX].Buf_Ctl.tail + 1) % SerialPortData[SERIAL_PC_TX].Buf_Ctl.buf_len;
        
        TXREG1 = Txdata;  
    }                                                           
    else                                
    {
        disable_interrupts(int_tbe);   
        serial_TxBuffEmpty( SERIAL_PC_TX );                   
    }  
     
}                     
                               
