#ifndef __MAIN_H
#define __MAIN_H
#include <18F46K22.h>
#device ADC=16
#device icd=true                   
#FUSES RC                        //RC
#FUSES PLLEN                     //Enable PLL             
#FUSES WDT                       //Watch Dog Timer
#FUSES WDT256                    //Watch Dog Timer uses 1:256 Postscale
#FUSES PRIMARY_ON                //Primary clock is always enabled
#FUSES FCMEN                     //Fail-safe clock monitor enabled
#FUSES IESO                      //Internal External Switch Over mode enabled
#FUSES NOPUT                     //No Power Up Timer
#FUSES NOBROWNOUT                //No brownout reset
#FUSES BORV19                    //Brownout reset at 1.9V
#FUSES PBADEN                    //PORTB pins are configured as analog input channels on RESET
#FUSES CCP3B5                    //CCP3 input/output multiplexed with RB5
#FUSES HFOFST                    //High Frequency INTRC starts clocking CPU immediately
#FUSES TIMER3C0                  //Timer3 Clock In is on pin C0
#FUSES CCP2D2                    //CCP2 input/output multiplexed with RD2
#FUSES MCLR                      //Master Clear pin enabled
#FUSES STVREN                    //Stack full/underflow will cause reset
#FUSES LVP                       // low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOXINST                   //Extended set extension and Indexed Addressing mode disabled (Legacy mode)
#FUSES NOPROTECT                 //Code not protected from reading
#FUSES NOCPB                     //No Boot Block code protection
#FUSES NOCPD                     //No EE protection
#FUSES NOWRT                     //Program memory not write protected
#FUSES NOWRTC                    //Configuration registers not write protected
#FUSES NOWRTB                    //Boot block not write protected
#FUSES NOWRTD                    //Data EEPROM not write protected
#FUSES NOEBTR                    //Memory not protected from table reads
#FUSES NOEBTRB                   //Boot block not protected from table reads

#use delay(internal=32MHz)  
 

#define LCD_DATA7       PIN_A0  
#define LCD_DATA6       PIN_A1  
#define LED_DRIVER_EN   PIN_A2
#define LCD_DATA1       PIN_A4
#define LCD_DATA4       PIN_A3  
#define LCD_DATA5       PIN_A5
#define UNUSED_A6       PIN_A6            
#define UNUSED_A7       PIN_A6  

#define LCD_DATA3       PIN_B0                                            
#define LCD_DATA2       PIN_B1       
#define LCD_DATA0       PIN_B2           
#define BTN_STOP        PIN_B3
#define BTN_PAUSE       PIN_B4           
#define BTN_SELECT      PIN_B5
#define UNUSUED_B6      PIN_B6          
#define UNUSUED_B7      PIN_B7         

#define freq_FSYNC      PIN_C0
#define freq_Interrupt  PIN_C1              
#define freq_Control    PIN_C2
#define spi_SCLK        PIN_C3
#define freq_Standby    PIN_C4
#define spi_SDATA       PIN_C5   
#define UART_RXD        PIN_C7   
#define UART_TXD        PIN_C6   

#define I2C_SCL_PIN     PIN_D0
#define I2C_SDA_PIN     PIN_D1
#define LCD_ENABLE_PIN  PIN_D2    
#define LCD_RW_PIN      PIN_D3 
#define LCD_RS_PIN      PIN_D4                                   
#define BTN_DOWN        PIN_D5
#define BTN_RIGHT       PIN_D6        
#define BTN_LEFT        PIN_D7   
                         
#define BTN_UP          PIN_E0 
#define BTN_POWER       PIN_E1 
#define LCD_RESET       PIN_E2
#define UNUSED_E3       PIN_E3      
                         
#define _500KHZ_REF_CLK

#use rs232(baud=115200,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8,UART1)
#use i2c(Master,I2C2 )//sda=PIN_D1,scl=PIN_D0)         
#use spi(MASTER, MODE=2, IDLE=1, CLK=spi_SCLK, DO=spi_SDATA,SPI1, SPI1)
#endif//__MAIN_H                                 
