/****************************************************************************  
FileName:     main.c                                                                                       
Dependencies: CCS libraries
Processor:    PIC18            
Hardware:
Complier: CCS     
Company:    
                                
                      
Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        10/2016    
                                         
*****************************************************************************
General description: Main code for the project              
                                                                    
                                                    
****************************************************************************
                            INCLUDES             
****************************************************************************/
#include "main.h"                                       
//#include <2401.C>                     
//#include <CRC.C>                                                      
// 
/* CCS is non-standard and it does not have a linker. Need to include
the C file from main which is against normal procedures!*/
#include "system_Tick.c"    
#include "user_debounce.c" 
#include "serial_buffer.c"
#include "AD5930.c"                                                   
#include "hal_eeprom.c"
#include "eeprom.c"        
#include "menu.c"     
#include "pc_interface.c"                       
#include "lcd_8bit.c"       
#include "menu_hal.c"
#include "Menu.h"  
/****************************************************************************
                            CCS SPECIFIC USE STATEMENTS            
****************************************************************************/       
                 
/****************************************************************************
                            DEFINES            
****************************************************************************/                                   
#define EEPROM_1_ADDR                   0x50    
#define TASK_10MS_PERIOD                (10 * SYSTICK_TICKS_PER_MS)
#define TASK_50MS_PERIOD                (50 * SYSTICK_TICKS_PER_MS)
#define TASK_100MS_PERIOD               (100 * SYSTICK_TICKS_PER_MS)                      
#define TASK_1000MS_PERIOD              (1000 * SYSTICK_TICKS_PER_MS)                      
#define BUILD_TEST_EEPROM_DATA               
//#define DISABLE_UART                    
/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/                                          
typedef enum { STOP, PAUSE, PLAY }_eModes;       

/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/
static _tDEBOUNCED_INPUT key_up;  
static _tDEBOUNCED_INPUT key_down;
static _tDEBOUNCED_INPUT key_left;
static _tDEBOUNCED_INPUT key_right;
static _tDEBOUNCED_INPUT key_sel; 
static _tDEBOUNCED_INPUT key_pause;
static _tDEBOUNCED_INPUT key_stop;      
static _tDEBOUNCED_INPUT key_power;      

static uint16_t subgrp_cntr = 0;     
uint16_t selected_grp_remaining_time_sec = 0;
uint16_t current_freq_index = 0;   
_tEepromDevice extMemory; 
_eModes sysMode = STOP;        
static uint8_t update_freq = false; 
static uint32_t task_10mS = 0;
//static uint32_t task_50mS = 0;
static uint32_t task_100mS = 0;
static uint32_t task_1000mS = 0;
/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/  
void task10mS(void);
//void task50ms(void);
void task100ms(void);                       
void task1s(void);                               
void init_hardware( void );  
void build_eeprom_data(void);
void test_speed_up_cntdwn(void);
void test_eeprom_1(void);
int main(void);
void pull_data_for_display(uint16_t index, void* data);
void pull_current_status_for_display(uint16_t* pRemainingTime, uint16_t *pFreqIndex, uint8_t *_sysMode);
void menu_changed_data(uint16_t grp_cnt, uint16_t subgrp_cnt, uint16_t dwellTime);
uint8_t* get_emu_eeprom_buffer(uint8_t dev_addr);
uint32_t get_frequency_result( void ) ;
static _eKEY_SELECTION get_user_input( void )  ;
void process_system_mode( void );

void test_eeprom( void );      
#ifdef BUILD_TEST_EEPROM_DATA
void build_test_libraries(void);
void build_tests(void);                       
void build_test_header(void);   
#endif     

/*********************************************************************
Function:    main
Input:       void
Output:      void
Dependencies:
Description: The main function
********************************************************************/     
void main( void )            
{          
    uint8_t i = 0;
                                 
    init_hardware();           
    initSysTick();                       
    AD5930_write_def_control();                      
    enable_interrupts(GLOBAL);     
#ifndef DISABLE_UART    
    SerialInit();
#endif    
    initDispl();                         
    menu_Init();
    InitEEPROM( &extMemory, EEPROM_1_ADDR, (128 * 500) );    
    
    WriteString((int8_t*)"RPGreen Elec"); 
    SetPostion(LINE2);                   
    WriteString((int8_t*)"LED Controller");  
    SetPostion(LINE3);
    WriteString((int8_t*)"Version 1.0");      
#ifdef BUILD_TEST_EEPROM_DATA  
    SetPostion(LINE4);                              
    WriteString((int8_t*)"Creating EEPROM Data");  
    EraseEEPROM( &extMemory );   
    build_test_libraries();
    build_tests();                       
    build_test_header();   
#endif                              
    delay_ms(1000); //startup delay to see splash screen  
    scan_eeprom( &extMemory );                  

    while( TRUE )                     
    {     
        //Execute a soft scheduler to run things at certain time intervals. 
        //Note: This is a soft scheduler because it is not an interrupt or OS task
        //and is subject to current execution. 
        if(SysTick_Ticked)
        {   
            SysTick_Ticked = 0;         
            if( timePassed( task_10mS ) >= TASK_10MS_PERIOD )
            {
                task10mS();  
                task_10mS = GetSysTick();   
            }                   
//            if( timePassed( task_50mS ) >= TASK_50MS_PERIOD )
//            {
//                task50mS();  
//                task_50mS = GetSysTick();   
//            }                  
            if( timePassed( task_100mS ) >= TASK_100MS_PERIOD )
            {                
                task100mS();  
                task_100mS = GetSysTick();   
            }                                     
            if( timePassed( task_1000mS ) >= TASK_1000MS_PERIOD )
            {                   
                task1S();  
                task_1000mS = GetSysTick();   
            }        
        }
        /*Outside of the tasks we can process anything that may need immediate
          attention or go to sleep (if needed)  */     
#ifndef DISABLE_UART
        pc_cmd_handler(); 
        if( serialMessageRdy )
        {
            if( pc_cmd_parse() == PC_MSG_HANDLED)
                serialMessageRdy = 0;
        }
#endif 
        if( update_freq )   
        {
            AD5930_write_freq( get_frequency_result() );   
            update_freq = false;
        }              
    }                           
                            
}                          
                                    
/*********************************************************************
Function:    init_hardware
Input:       void
Output:      void      
Dependencies:  
Description: init hardware components of the system        
                including the GPIO.
********************************************************************/        
void init_hardware( void )       
{    
    //TODO: Remove "magic" numbers and replace with defines. 1 = input and 0 = output
    set_tris_a( 0xC0 );                            
    set_tris_b( 0xF8 );              
    set_tris_c( 0x80 );                         
    set_tris_d( 0xE0 );         
    set_tris_e( 0x0B );                  
    
                        
    //Setup the debounce module for each user input. This will limit false triggers                         
    initDebounce( &key_up, 1);                                                      
    initDebounce( &key_down, 1);                 
    initDebounce( &key_left, 1);
    initDebounce( &key_right, 1);
    initDebounce( &key_sel, 1);
    initDebounce( &key_pause, 1);
    initDebounce( &key_stop, 1);      
    initDebounce( &key_power, 1);      
    setup_timer_0(T0_INTERNAL);        

    setup_spi (SPI_MASTER  | SPI_H_TO_L | SPI_CLK_DIV_16);  
    //setup_uart(115200);    
    i2c_init(400000);
}

/*********************************************************************
Function:    task10mS
Input:       void
Output:      void                
Dependencies:                                            
Description: Task that runs every 10mS. This is a good location
            to read raw key inputs as it will need multiple hits for
            debouncing until it is valid.    
********************************************************************/             
void task10mS(void)                          
{                                          
    debounceInput( &key_up, input(BTN_UP));       
    debounceInput( &key_down, input(BTN_DOWN));
    debounceInput( &key_left, input(BTN_LEFT));
    debounceInput( &key_right, input(BTN_RIGHT));
    debounceInput( &key_sel, input(BTN_SELECT));
    debounceInput( &key_pause, input(BTN_PAUSE));
    debounceInput( &key_stop, input(BTN_STOP));  
    debounceInput( &key_power, input(BTN_POWER));    
    
  //  if( !serial_IsTXBusy(SERIAL_PC_TX))
  //      serial_StartTX(SERIAL_PC_TX);    
                                     
}                   

/*********************************************************************
Function:    task50ms 
Input:       void               
Output:      void
Dependencies:
Description: Task that runs every 50mS. W
********************************************************************/   
//void task50ms(void)
//{
//       
//}

/*********************************************************************
Function:    task100ms
Input:       void                                   
Output:      void
Dependencies:
Description: Task that runs every 100mS. Will remove this if not needed.
********************************************************************/   
void task100ms(void)
{        
     menu((_eKEY_SELECTION)get_user_input(), 100);
}    

/*********************************************************************
Function:    task1s
Input:       void
Output:      void                                                                                        
Dependencies:
Description: Task that runs every 1S. Will remove this if not needed. 
********************************************************************/   
void task1s(void)                 
{           
    process_system_mode();                        
}
        
/*********************************************************************
Function:     pull_data_for_display
Input:        uint16_t index: Page number to read from EEPROM                                 
              void* data:Pointer to an array to store the data
Return:       void    
Description:  This function is called from the menu module ("callback")
                that pulls data from the eeprom to be displayed. 
Visibility: Public
*********************************************************************/
void pull_data_for_display(uint16_t index, void* data)
{
    ReadExtEEPROM(extMemory, (index * 128), (uint8_t*)data, 128);  
}                                     

/*********************************************************************
Function:   get_frequency_result    
Input:      uint32_t *i_freq: Pointer to return the current frequency
Return:     void                                                        
Description: This function retrieves the current frequency (or next frequency)
            from eeprom. 
Visibility: Private     
*********************************************************************/
uint32_t get_frequency_result( void ) 
{  
    uint32_t ifreq;
    uint32_t t, t1;
    freq_t freq = { 0 }; 
#if 0                   

    
    uint16_t overflow_page = current_freq_index / 33;
    uint16_t overflow_offset = 28;   

    if (current_freq_index >= 33)
    {                                            
        overflow_offset = 4;
        overflow_offset += (current_freq_index - (33 * overflow_page)); 
    }                                                             
    overflow_offset = (((subgrp_lookup[selected_subgrp] + overflow_page) * 128) + overflow_offset) - 1;       
    ReadExtEEPROM(extMemory, overflow_offset, freq.freq, 3);
#endif                                                   
    freq = menu_subgroup[selected_subgrp].lib_struct.freqList[current_freq_index];

    //The data is retrieved as 3 bytes, lSB first. Adjust it to return as 32bit value   
    //CCS wouldn't work with oring ifreq at each line? Had to break it into sep. variables
    ifreq = freq.freq[0];   
    t = (freq.freq[1] << 8);    
    t1 = (freq.freq[2] << 16);           
    
    ifreq |= (t | t1);    
                                        
    return ifreq;
}
     
/*********************************************************************
Function:    pull_current_status_for_display
Input:       uint16_t* pRemainingTime: Retruns the current systems remaining dwell time.                                        
            uint16_t *pFreqIndex: Returns the current system frequency index 
Return:     void
Description: "callback" function from menu module that gets the current
            frequency index of the current subtest and the remaining dwell time
            so it can be displayed on the display. 
Visibility: public                                                                     
*********************************************************************/
void pull_current_status_for_display(uint16_t* pRemainingTime, uint16_t *pFreqIndex, uint8_t *_sysMode)
{                                       
    *pRemainingTime = selected_grp_remaining_time_sec;        
    *pFreqIndex = current_freq_index;   
    *_sysMode = (uint8_t)sysMode;    

}

/*********************************************************************
Function:   menu_changed_data
Input:      uint16_t grp_cnt: New group ID selected   
            uint16_t subgrp_cnt: New subgroup ID selected                 
            uint16_t dwellTime: New dwelltime selected                         
Return:     void                                         
Description: The "callback" function is called from the menu module
            when a user has selected a new option in the menu for a test. 
            This will update the list maintained in main as the system's value. 
Visibility: public
*********************************************************************/        
void menu_changed_data(uint16_t _grp_cnt, uint16_t _subgrp_cnt, uint16_t dwellTime)
{                 
    //grp = _grp_cnt;              
    subgrp_cntr = _subgrp_cnt;                            
                      
    selected_grp_remaining_time_sec = dwellTime;
    current_freq_index = 0;                        
    sysMode = PLAY;    
    update_freq = true;     
        
                                    
}                                    
  
/*********************************************************************
Function:   get_user_input
Input:      void                        
Return:     _eKEY_SELECTION: key action                                         
Description: Returns key action based on debounced input 
Visibility: private
*********************************************************************/    
static _eKEY_SELECTION get_user_input( void )  
{    
    if( ButtonPressed( &key_stop))   
    {
        sysMode = STOP;                
    }  
    if( ButtonPressed( &key_pause))
    {    
        if( sysMode == PLAY)
            sysMode = PAUSE;  
        else
            sysMode = PLAY;      
    }      

    if( ButtonPressed( &key_up ) )
    {
        return eKEY_UP;                 
    }                    
    else if( ButtonPressed( &key_down ) )
    {                              
        return eKEY_DOWN;    
    } 
    else if( ButtonPressed( &key_left ) )
    {                
        return eKEY_LEFT;    
    }                     
    else if( ButtonPressed( &key_right ) )
    {
        return eKEY_RIGHT;    
    }             
    else if( ButtonPressed( &key_sel ) )
    {                    
        return eKEY_SELECT;    
    }                         
    return eKEY_NONE; 
                        
}           

void process_system_mode( void )   
{
    if (sysMode == PLAY)
    {           
        output_high(LED_DRIVER_EN);
        if (selected_grp_remaining_time_sec != 0) 
        {    
            selected_grp_remaining_time_sec--;
        }
        else
        {             
        
            update_freq = true;    
            if (++current_freq_index < menu_subgroup[selected_subgrp].lib_struct.numFrequencies)
            {      
                selected_grp_remaining_time_sec = menu_subgroup[selected_subgrp].lib_struct.dwellTime;
            }
            else
            {
                subgrp_cntr++;
                selected_subgrp++;
                current_freq_index = 0;                        
                if (subgrp_cntr < currGenTest.test_struct.nuSubGrps)
                {
                    if (selected_subgrp >= 3)
                    {
                        selected_subgrp = 0;
                        external_menu_update_subgroup_list();
                    }
                    selected_grp_remaining_time_sec = menu_subgroup[selected_subgrp].lib_struct.dwellTime;    
                }
                else
                {
                    currGenTest.test_struct.name[0] = 0;
                    sysMode = STOP;      
                    output_low(LED_DRIVER_EN);
                    subgrp_cntr = 0;
                    update_freq = false;   
                    AD5930_shutdown();
                }                       
            }                    
        }
    }
    else if (sysMode == STOP)
    {                   
        selected_grp_remaining_time_sec = 0;
        current_freq_index = 0;
        selected_subgrp = 0; 
        subgrp_cntr = 0;  
        update_freq = false; 
        AD5930_shutdown();     
        output_low(LED_DRIVER_EN);
    }                      
    
}              

#if 0                                        
void test_eeprom( void )  
{    
    uint8_t data_wr[5] = {1,2,3,4,5};     
    uint8_t data_rd[5] = {0};    
    WriteExtEEPROM( extMemory, 0, data_wr, 5);         
    ReadExtEEPROM( extMemory, 0, data_rd, 5);    
    
    if( data_wr[0] == data_rd[0] )   
    {
       data_wr[0] == data_rd[0] + 1;    
    }
}                 
#endif    

#ifdef BUILD_TEST_EEPROM_DATA
void build_test_libraries(void)
{
    lib_eeprom_t libraries = { 0 };
    uint32_t freq;
    uint16_t byte_addr = 0;
                           
    libraries.lib_struct.test_id = 1;
    libraries.lib_struct.dwellTime = 180;
    libraries.lib_struct.numFrequencies = 1;
    freq = 100;        //1 hz
    libraries.lib_struct.freqList[0].freq[0] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[1] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[2] = (uint8_t)freq;
                                                      
    strncpy(libraries.lib_struct.name, (uint8_t*)"1 Hz Lib", 20);
    libraries.lib_struct.overflow = 0;            
    byte_addr = (4 + ((libraries.lib_struct.test_id - 1) * 2)) * 128;                                                                       
    WriteExtEEPROM(extMemory, byte_addr, libraries.bytes, 128);

    libraries.lib_struct.test_id = 2;            
    libraries.lib_struct.dwellTime = 180;            
    libraries.lib_struct.numFrequencies = 1;
    freq = 810;        //8.1hz
    libraries.lib_struct.freqList[0].freq[0] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[1] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[2] = (uint8_t)freq;

    strncpy(libraries.lib_struct.name, (uint8_t*)"8.1 Hz Lib", 20);
    libraries.lib_struct.overflow = 0;
    byte_addr = (4 + ((libraries.lib_struct.test_id - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, libraries.bytes, 128);

    libraries.lib_struct.test_id = 3;
    libraries.lib_struct.dwellTime = 180;
    libraries.lib_struct.numFrequencies = 1;
    freq = 100000;        //1khz
    libraries.lib_struct.freqList[0].freq[0] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[1] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[2] = (uint8_t)freq;
                                                          
    strncpy(libraries.lib_struct.name, (uint8_t*)"1 kHz Lib", 20);
    libraries.lib_struct.overflow = 0;
    byte_addr = (4 + ((libraries.lib_struct.test_id - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, libraries.bytes, 128);

    libraries.lib_struct.test_id = 4;
    libraries.lib_struct.dwellTime = 180;
    libraries.lib_struct.numFrequencies = 1;
    freq = 1000000;        //10khz
    libraries.lib_struct.freqList[0].freq[0] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[1] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[2] = (uint8_t)freq;

    strncpy(libraries.lib_struct.name, (uint8_t*)"10kHz Lib", 20);
    libraries.lib_struct.overflow = 0;
    byte_addr = (4 + ((libraries.lib_struct.test_id - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, libraries.bytes, 128);

}

void build_tests(void)
{                                                         
    test_eeprom_t tests = { 0 };
    uint16_t byte_addr = 0;

    tests.test_struct.nuSubGrps = 1;
    tests.test_struct.overflow = 0;
    tests.test_struct.subGrpList[0] = 1;
    strncpy(tests.test_struct.name, (uint8_t*)"1 Hz Test", 20);
    tests.test_struct.test_num = 1;
    byte_addr = (248 + 4 + ((tests.test_struct.test_num - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, tests.bytes, 128);

    tests.test_struct.nuSubGrps = 1;
    tests.test_struct.overflow = 0;                   
    tests.test_struct.subGrpList[0] = 2;
    strncpy(tests.test_struct.name, (uint8_t*) "8.1 Hz Test", 20);
    tests.test_struct.test_num = 2;
    byte_addr = (248 + 4 + ((tests.test_struct.test_num - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, tests.bytes, 128);

    tests.test_struct.nuSubGrps = 1;
    tests.test_struct.overflow = 0;
    tests.test_struct.subGrpList[0] = 3;
    strncpy(tests.test_struct.name, (uint8_t*)"1 KHz Test", 20);
    tests.test_struct.test_num = 3;
    byte_addr = (248 + 4 + ((tests.test_struct.test_num - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, tests.bytes, 128);  

    tests.test_struct.nuSubGrps = 1;
    tests.test_struct.overflow = 0;                         
    tests.test_struct.subGrpList[0] = 4;                   
    strncpy(tests.test_struct.name, (uint8_t*)"10 KHz Test", 20);
    tests.test_struct.test_num = 4;
    byte_addr = (248 + 4 + ((tests.test_struct.test_num - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, tests.bytes, 128);

    tests.test_struct.nuSubGrps = 4;
    tests.test_struct.overflow = 0;
    tests.test_struct.subGrpList[0] = 1;
    tests.test_struct.subGrpList[1] = 2;
    tests.test_struct.subGrpList[2] = 3;
    tests.test_struct.subGrpList[3] = 4;                                                                                            
    strncpy(tests.test_struct.name, (uint8_t*)"4 Freq Test", 20);                                                             
    tests.test_struct.test_num = 5;
    byte_addr = (248 + 4 + ((tests.test_struct.test_num - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, tests.bytes, 128);
}

void build_test_header(void)
{
    hdr_eeprom_t header = { 0 };
                                                                                                                                               
    header.hdr_struct.eeprom_valid = 1;
    header.hdr_struct.eeprom_ver = 1;              
    header.hdr_struct.num_used_pages = 22;
    header.hdr_struct.num_data_bytes = (18 * 128);
    WriteExtEEPROM(extMemory, 0, header.bytes, HDR_LEN);
}
                
#endif                        
