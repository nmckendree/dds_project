/****************************************************************************  
FileName:     main.c                                                                                       
Dependencies:  
Processor:                 
Hardware:
Complier:       
Company:    
                                
                      
Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        12/2016    
                                         
*****************************************************************************
General description: Main code for the project              
                                                                    
                                                    
****************************************************************************
                            INCLUDES             
****************************************************************************/
#include "project.h"
#include "user_debounce.h"
#include "system_Tick.h"
#include "sw_pwm.h"
#include "led_controller.h"

/****************************************************************************
                            DEFINES            
****************************************************************************/     
#define FW_VERSION                  5

#define TASK_10MS_PERIOD            10
#define TASK_1000MS_PERIOD          1000
#define DWELL_3MIN                  ( 3UL * 60UL * 1000UL)
#define DWELL_10MIN                 ( 10UL * 60UL * 1000UL)

#define OT_WARN_C                   65
#define OT_SHUTOFF_C                80
#define OT_RESTART_C                30
#define OVER_TEMP_CALC_ADC(x)       ( (204*x+10193+50)/100 )            //based on excel linear fit between temp (x) and adc (y). Expanded decimal places for integer math. Added 50 for rounding
#define OT_WARN_ADC                 ( OVER_TEMP_CALC_ADC( OT_WARN_C ))
#define OT_ALARM_ADC                ( OVER_TEMP_CALC_ADC( OT_SHUTOFF_C ))
#define OT_RESTART_ADC              ( OVER_TEMP_CALC_ADC( OT_RESTART_C ))

#define ADC_FULL_SCALE              1023
#define ADC_HALF_SCALE              (ADC_FULL_SCALE >> 1 )
#define ADC_RAW_250MV_OFFSET        51

#define DC_SWITCH_OVERRIDE          0       //change to 1 to default the DC
#define DEF_DUTY_CYCLE              50

//Since the ADC is used to read a switch with 3 positions. I will make a small range
//at the rails (ov and 5v) with a minimum 250mV offset to allow for system noise. 
//The remaining median voltage range (nominal 2.5v) will be valid for the third option. This allows
//for no invalid spaces between selections since this is purely a switch condition (not a sensor measurement)

#define DUTY_CYCLE_SEL_25_PER_MIN   0//0V (Up to +10% for resistor tolerances )
#define DUTY_CYCLE_SEL_25_PER_MAX   (DUTY_CYCLE_SEL_25_PER_MIN + ADC_RAW_250MV_OFFSET )

#define DUTY_CYCLE_SEL_75_PER_MAX   ADC_FULL_SCALE//5V in raw adc (down to -10% for resistor tolerances)
#define DUTY_CYCLE_SEL_75_PER_MIN   (DUTY_CYCLE_SEL_75_PER_MAX - ADC_RAW_250MV_OFFSET)//5V in raw adc (down to -10% for resistor tolerances)

#define DUTY_CYCLE_SEL_50_PER_MIN   (DUTY_CYCLE_SEL_25_PER_MAX + 1)
#define DUTY_CYCLE_SEL_50_PER_MAX   (DUTY_CYCLE_SEL_75_PER_MIN - 1)

#define RUN_TEMP_CONTROLS

#define RAMP_STR_HZ                 809         //8.09 * 100
#define RAMP_END_HZ                 815         //8.15 * 100
#define RAMP_INC_HZ                 1           //0.01 * 100

#define RAMP_EN_ON                  1           //Define the active level (0,1) for ramp enable switch
/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/                                          
 
/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/
static _tDEBOUNCED_INPUT btn_start;
static _tDEBOUNCED_INPUT btn_stop; 
static _tDEBOUNCED_INPUT pwr_good; 
static uint32_t dwell_timer_ms = 0;         //uint32 to give minutes, up to 71582
static uint32_t task_10mS = 0;
static uint32_t dwell_timeout = DWELL_3MIN;
static uint32_t task_1s = 0;
static bool is_unit_running = false;
static uint16_t temp_sensors[2] = {0};
static bool unit_is_in_overtemp_alarm = false;
static bool power_Good_error = false;

static _LED_CONTROLLER_t dwell_led;
static _LED_CONTROLLER_t status_led;
static _LED_CONTROLLER_t temp_warn_led;
static _LED_CONTROLLER_t temp_alarm_led;

static uint8_t duty_cycle_option = 0;
static uint32_t current_freq = RAMP_STR_HZ;
/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/  
void init_hardware( void );
uint16_t read_temp_sensor( adc_channel_t channel );
void check_for_overtemperature( void );
void set_system_state_to_off( void );
void test_PowerGood_Failure( void );
uint8_t process_duty_cycle_input( void );
/*********************************************************************
Function:    main
Input:       void
Output:      void
Dependencies:
Description: The main function
********************************************************************/ 
int main(void)            
{            
    init_hardware();           
    initSysTick();                       

    INTERRUPT_PeripheralInterruptEnable();
    INTERRUPT_GlobalInterruptEnable();
    
    while( 1 )                     
    {     
        if(SysTick_Ticked)
        {   
            SysTick_Ticked = 0;         
            if( timePassed( task_10mS ) >= TASK_10MS_PERIOD )
            {                
                task_10mS = GetSysTick();   
                debounceInput( &btn_start, iSTART_CTRL_GetValue() );
                debounceInput( &btn_stop, iSTOP_CTRL_GetValue() );
                debounceInput( &pwr_good, iPwrGood_GetValue() );
                                   
                if( is_unit_running )
                {
                    if( timePassed( dwell_timer_ms ) >= dwell_timeout )
                    {          
                        /* Ramp the frequency if desired*/
                        if( iRAMP_EN_PORT == RAMP_EN_ON )
                        {
                            current_freq += RAMP_INC_HZ;
                            if( current_freq >= RAMP_END_HZ)    /* At the max freq so shut off */
                            {
                                set_system_state_to_off();                            
                            }
                            else
                            {
                                PWM_Stop();
                                PWM_Frequency( current_freq, duty_cycle_option );                                
                                PWM_Start();
                                dwell_timer_ms = GetSysTick();
                            }    
                                
                        }
                        else
                        {
                            set_system_state_to_off();
                        }
                    }
                    if( ButtonPressed( &btn_stop) )
                    {
                        set_system_state_to_off();
                    }
                }
                else
                {
                    if( ButtonPressed( &btn_start) && !unit_is_in_overtemp_alarm && !power_Good_error)
                    {  
                        dwell_timer_ms = GetSysTick();
                        set_led_state( &status_led, LED_ON, 0);
                        oPWM_SetLow();
                        is_unit_running = true;
                        current_freq = RAMP_STR_HZ;
                        PWM_Stop();
                        PWM_Frequency( current_freq, duty_cycle_option );                        
                        PWM_Start();                        
                    }
                }
                
                led_controller( &dwell_led, 10 );
                led_controller( &status_led, 10 );
                led_controller( &temp_warn_led, 10 );
                led_controller( &temp_alarm_led, 10 );   
                test_PowerGood_Failure( );
            }   
            if( timePassed( task_1s ) >= TASK_1000MS_PERIOD )
            {
                task_1s = GetSysTick();                
                dwell_timeout = ( (iDWELL_OPT_GetValue() == 0) ? DWELL_10MIN : DWELL_3MIN); 
                if( dwell_timeout == DWELL_10MIN )
                {
                    set_led_state( &dwell_led, LED_ON, 0);
                }
                else
                {
                    set_led_state( &dwell_led, LED_OFF, 0);
                }
                       
                temp_sensors[ 0 ] = read_temp_sensor( adc_Temp1 );
                temp_sensors[ 1 ] = read_temp_sensor( adc_Temp2 );           
                check_for_overtemperature( );
                duty_cycle_option = process_duty_cycle_input( );
                 
            }
        }        
    }                           
    return (0);                        
}   

/*********************************************************************
Function:    init_hardware
Input:       void
Output:      void      
Dependencies:  
Description: init hardware components of the system        
                including the GPIO.
********************************************************************/        
void init_hardware( void )       
{    
    SYSTEM_Initialize();            //Microchip MCC generate function - init all hardware
    PWM_Stop();                                  
    //Setup the debounce module for each user input. This will limit false triggers                         
    initDebounce( &btn_start, 0, 5);                                                      
    initDebounce( &btn_stop, 0, 5);                 
    initDebounce( &pwr_good, 1, 2);   
    
    PWM_Frequency( RAMP_STR_HZ, DEF_DUTY_CYCLE );
    
    dwell_led.led_id = LED_DWELL_10MIN;
    dwell_led.led_state = LED_OFF;
    dwell_led.led_active_level = 1;
    
    status_led.led_id = LED_RUN;
    status_led.led_state = LED_OFF;
    status_led.led_active_level = 1;
    
    temp_warn_led.led_id = LED_TEMP_WARN;
    temp_warn_led.led_state = LED_OFF;
    temp_warn_led.led_active_level = 1;
    
    temp_alarm_led.led_id = LED_TEMP_ALARM;
    temp_alarm_led.led_state = LED_OFF;
    temp_alarm_led.led_active_level = 1;
    
}

/*********************************************************************
Function:    read_temp_sensor
Input:       adc_channel_t channel: ADC channel - see adc.h for the enum value
Output:      uint16_t: RAW ADC value      
Dependencies:  
Description: Gets the raw ADC value of the temp sensors
********************************************************************/    
uint16_t read_temp_sensor( adc_channel_t channel )
{
   // ADC_SelectChannel( channel );
   // ADC_StartConversion();
   // while(!ADC_IsConversionDone());
   // return (ADC_GetConversionResult());    
    return ADC_GetConversion( channel );
}

/*********************************************************************
Function:    check_for_overtemperature
Input:       void
Output:      void
Dependencies:  
Description: Logic code for over temperature. 
********************************************************************/    
void check_for_overtemperature( void )
{           
#ifdef RUN_TEMP_CONTROLS
    if( temp_sensors[0] > OT_WARN_ADC || temp_sensors[1] > OT_WARN_ADC)
    {
        set_led_state( &temp_warn_led, LED_BLINK_FAST, CONTINOUS_BLINK); 
        
        if( temp_sensors[0] > OT_ALARM_ADC || temp_sensors[1] > OT_ALARM_ADC )
        {
            set_led_state( &temp_alarm_led, LED_BLINK_FAST, CONTINOUS_BLINK); 
            unit_is_in_overtemp_alarm = true;
            set_system_state_to_off();
        }
    }
    else
    {
        set_led_state( &temp_warn_led, LED_OFF, 0); 
    }

    if( unit_is_in_overtemp_alarm )
    {
        if( temp_sensors[0] < OT_RESTART_ADC && temp_sensors[1] < OT_RESTART_ADC)
        {
            unit_is_in_overtemp_alarm = false;
            set_led_state( &temp_alarm_led, LED_OFF, 0); 
        }
    }
#else
            unit_is_in_overtemp_alarm = false;
            set_led_state( &temp_alarm_led, LED_OFF, 0);    
            set_led_state( &temp_warn_led, LED_OFF, 0); 
#endif
}

/*********************************************************************
Function:    set_system_state_to_off
Input:       void
Output:      void
Dependencies:  
Description: Common routine to return the state to a known off position
********************************************************************/    
void set_system_state_to_off( void )
{
    PWM_Stop();
    oPWM_SetLow();
    set_led_state( &status_led, LED_OFF, 0);
    is_unit_running = false;
    dwell_timeout = 0;  
}

/*********************************************************************
Function:    test_PowerGood_Failure
Input:       void
Output:      void
Dependencies:  
Description: Checks on the power good signal. This is using the debounced
            state of the pin to guard against noise which may falsely trigger. 
********************************************************************/    
void test_PowerGood_Failure( void )
{
    if( pwr_good.changed  )             //Only check on change event
    {
        if( pwr_good.debouncedState == 1)
        {
           power_Good_error = false;
           set_system_state_to_off();
           set_led_state( &status_led, LED_OFF, 0);
        }
        else
        {
            power_Good_error = true;
            set_system_state_to_off();
            set_led_state( &status_led, LED_BLINK_FAST, CONTINOUS_BLINK);
        }
    }
}

/*********************************************************************
Function:    process_duty_cycle_input
Input:       void
Output:      uint8_t: duty cycle value
Dependencies:  
Description: Reads the adc input for the duty cycle switch. process
              the raw adc value and returns the desired duty cycle. 
********************************************************************/  
uint8_t process_duty_cycle_input( void )
{
    uint16_t adc_read = 0;
    
#if DC_SWITCH_OVERRIDE == 1
    return DEF_DUTY_CYCLE;
#else            
    adc_read = ADC_GetConversion( iDutyCyleSW );
    
    if( adc_read < DUTY_CYCLE_SEL_50_PER_MIN)
    {
        return 25;
    }
    else if( adc_read < DUTY_CYCLE_SEL_75_PER_MIN )
    {
        return 50;
    }
    return 75;
#endif
}
