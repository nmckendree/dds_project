#include <stdint.h>
#include <stdbool.h>
#include "led_controller.h"
#include "mcc.h"

#define LED_ON_TIME             50
#define LED_OFF_TIME            200
/*********************************************************************
Function:    set_led_state
Input:       LED_CONTROLS_t new_state: led state/mode
			 uint8_t new_LED_blink_cnt: # of blinks
Output:      void
Dependencies:
Description: sets the led state (on/off/blinking)
********************************************************************/
void set_led_state( _LED_CONTROLLER_t *led, LED_CONTROLS_t new_state, uint8_t new_LED_blink_cnt)
{	
	led->led_state = new_state;	
	led->blink_counter = new_LED_blink_cnt;
}

static void turn_led_on( _LED_CONTROLLER_t *led )
{
    switch( led->led_id )
    {
        case LED_TEMP_WARN:
            oLED_TEMP_WARN_LAT = led->led_active_level;
            break;
        case LED_TEMP_ALARM:
            oLED_OVER_TEMP_LAT = led->led_active_level;
            break; 
        case LED_DWELL_10MIN:
            oLED_Dwell_LAT = led->led_active_level;
            break; 
        case LED_RUN:
            oLED_LAT = led->led_active_level;
            break;
        default:
            break;
    }
}

static void turn_led_off( _LED_CONTROLLER_t *led )
{
    switch( led->led_id )
    {
        case LED_TEMP_WARN:
            oLED_TEMP_WARN_LAT = !led->led_active_level;
            break;
        case LED_TEMP_ALARM:
            oLED_OVER_TEMP_LAT = !led->led_active_level;
            break; 
        case LED_DWELL_10MIN:
            oLED_Dwell_LAT = !led->led_active_level;
            break; 
        case LED_RUN:
            oLED_LAT = !led->led_active_level;
            break;
        default:
            break;
    }   
}

static bool get_led_status( _LED_CONTROLLER_t *led )
{
    switch( led->led_id )
    {
        case LED_TEMP_WARN:
            return (oLED_TEMP_WARN_LAT == led->led_active_level);
        case LED_TEMP_ALARM:
            return (oLED_OVER_TEMP_LAT == led->led_active_level);
        case LED_DWELL_10MIN:
            return (oLED_Dwell_LAT == led->led_active_level);
        case LED_RUN:
            return (oLED_LAT == led->led_active_level);
        default:
            break;
    }     
    return 0;
}
/*********************************************************************
Function:    led_controller
Input:       void
Output:      void
Dependencies:
Description: state machine to control the LED behavior
********************************************************************/
void led_controller( _LED_CONTROLLER_t *led, uint16_t ms_since_last_call)
{
	switch( led->led_state )
	{
		case LED_ON:
			turn_led_on( led );
			break;
		case LED_BLINK_SLOW:
		case LED_BLINK_FAST:
            led->time_last_ran = ( led->time_last_ran >= ms_since_last_call ? led->time_last_ran - ms_since_last_call : 0);
            
			if( led->blink_counter != 0 && led->time_last_ran == 0 )
			{
				if( get_led_status( led ) == 1)
				{
                    led->time_last_ran = LED_OFF_TIME;
					turn_led_off( led );
                    if( led->blink_counter < 255 )
                        led->blink_counter = ((led->blink_counter > 0) ? led->blink_counter - 1 : 0);

					if( led->blink_counter == 0)
						led->led_state = LED_OFF;
				}
				else
				{
                    led->time_last_ran = LED_ON_TIME;
					turn_led_on( led );
				}
			}
			break;
		case LED_OFF:
		default:
			turn_led_off( led );
			led->blink_counter = 0;
			break;
	}
}
