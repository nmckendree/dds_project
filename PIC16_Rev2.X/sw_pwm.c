/****************************************************************************  
FileName:     sw_pwm.c                                                                                       
Dependencies:  
Processor:                 
Hardware:
Complier:       
Company:    
                                
                      
Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        01/15/2017    
                                         
*****************************************************************************
General description: Source code for a software PWM module
 *                           
****************************************************************************
                            INCLUDES             
****************************************************************************/
#include "project.h"
#include "sw_pwm.h"

/****************************************************************************
                            DEFINES            
****************************************************************************/     
#define FOSC				_XTAL_FREQ
#define TMR_PRESCALE		2
#define F_TMR3				(FOSC / 4 / TMR_PRESCALE)
#define P_TMR3				( 1.0 / F_TMR3 )
/*Adjust the timer period value based on an offset from testing with
 my logic analyzer. */
#define SW_PWM_ADJUSTMENT_FACTOR        70
/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/                                          
 
/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/
static uint16_t onTime = 0;         //off time in ticks
static uint16_t offTime = 0;        //off time in ticks
static uint8_t output_dir = 0;      //Determines next output toggle

/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/  
void PWM_ISR( void );

/*********************************************************************
Function:    PWM_Start
Input:       void
Output:      void      
Dependencies:  
Description:  
********************************************************************/     
void PWM_Start( void )
{
    TMR3_SetInterruptHandler(PWM_ISR);
    TMR3_StartTimer();    
}

/*********************************************************************
Function:    PWM_Stop
Input:       void
Output:      void      
Dependencies:  
Description:  
********************************************************************/     
void PWM_Stop( void )
{
    TMR3_StopTimer();
}
    
/*********************************************************************
Function:    PWM_Frequency
Input:       uint32_t freq: Frequency input * 100
Output:      void      
Dependencies:  
Description:  
********************************************************************/     
void PWM_Frequency( uint32_t freq, uint8_t dutyCycle )
{
    if(dutyCycle > 100)
     dutyCycle = 100;
       
	uint16_t on_period = (uint16_t)((1.0 / ((float)freq / 100.0)) / P_TMR3);
	uint16_t off_period = (uint16_t)( (1.0 / ((float)freq / 100.0))  / P_TMR3);

	if (dutyCycle == 50)
	{
		on_period /= 2;
		off_period /= 2;

	}
	else if (dutyCycle == 25)
	{
		on_period = on_period / 4;
		off_period = on_period * 3;
	}
	else if (dutyCycle == 75)
	{		
		off_period = off_period / 4;
		on_period = off_period * 3;
	}
	onTime = 65535 - on_period;
	offTime = 65535 - off_period;
    
    /* Adjust the on and off times with a tested calibration value. Take care not
     to encounter an subtraction overrun. */
    onTime = ( onTime >= SW_PWM_ADJUSTMENT_FACTOR ? onTime - SW_PWM_ADJUSTMENT_FACTOR : 0);
    offTime = ( offTime >= SW_PWM_ADJUSTMENT_FACTOR ? offTime - SW_PWM_ADJUSTMENT_FACTOR : 0);
    
    PWM_Stop();
    // Write to the Timer3 register
    TMR3H = (offTime >> 8);
    TMR3L = offTime;
    
    
}

void PWM_ISR( void )
{

    if( output_dir )
    {
        oPWM_SetLow();
        TMR3H = (offTime >> 8);
        TMR3L = offTime;
    }
    else
    {
        oPWM_SetHigh();
        TMR3H = (onTime >> 8);
        TMR3L = onTime;
    }
    output_dir ^= 1;
}
