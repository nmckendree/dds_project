/****************************************************************************
FileName:     project.h
Dependencies:
Processor:
Hardware:
Complier:     
Company:     
Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


 *****************************************************************************
General description:





usage:

****************************************************************************/

#ifndef PROJECT_H
#define	PROJECT_H

/****************************************************************************
                        Includes
 ****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include "mcc.h"    
#include "interrupt_manager.h"
#include "pin_manager.h"
//#include "pwm1.h"
#include "tmr1.h"  

/****************************************************************************
                        GLOBAL TYPEDEFS
 *****************************************************************************/

/****************************************************************************
                        DEFINES AND ENUMARATED VALUES
 *****************************************************************************/
 
/****************************************************************************
                        MACROS
 *****************************************************************************/

/****************************************************************************
                        GLOBAL FUNCTION PROTOTYPES
 *****************************************************************************/
 
/****************************************************************************
                        GLOBAL VARIABLES
 ******************************************************************************/
 

#endif	/* PROJECT_H */

