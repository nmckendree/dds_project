/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using MPLAB(c) Code Configurator

  @Description:
    This header file provides implementations for pin APIs for all pins selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 4.15
        Device            :  PIC16F18346
        Version           :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.40

    Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

    Microchip licenses to you the right to use, modify, copy and distribute
    Software only when embedded on a Microchip microcontroller or digital signal
    controller that is integrated into your product or third party product
    (pursuant to the sublicense terms in the accompanying license agreement).

    You should refer to the license agreement accompanying this Software for
    additional information regarding your rights and obligations.

    SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
    EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
    MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
    IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
    CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
    OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
    INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
    CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
    SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
    (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

*/


#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set oLED aliases
#define oLED_TRIS               TRISAbits.TRISA2
#define oLED_LAT                LATAbits.LATA2
#define oLED_PORT               PORTAbits.RA2
#define oLED_WPU                WPUAbits.WPUA2
#define oLED_OD                ODCONAbits.ODCA2
#define oLED_ANS                ANSELAbits.ANSA2
#define oLED_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define oLED_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define oLED_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define oLED_GetValue()           PORTAbits.RA2
#define oLED_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define oLED_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define oLED_SetPullup()      do { WPUAbits.WPUA2 = 1; } while(0)
#define oLED_ResetPullup()    do { WPUAbits.WPUA2 = 0; } while(0)
#define oLED_SetPushPull()    do { ODCONAbits.ODCA2 = 1; } while(0)
#define oLED_SetOpenDrain()   do { ODCONAbits.ODCA2 = 0; } while(0)
#define oLED_SetAnalogMode()  do { ANSELAbits.ANSA2 = 1; } while(0)
#define oLED_SetDigitalMode() do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set oLED_OVER_TEMP aliases
#define oLED_OVER_TEMP_TRIS               TRISBbits.TRISB4
#define oLED_OVER_TEMP_LAT                LATBbits.LATB4
#define oLED_OVER_TEMP_PORT               PORTBbits.RB4
#define oLED_OVER_TEMP_WPU                WPUBbits.WPUB4
#define oLED_OVER_TEMP_OD                ODCONBbits.ODCB4
#define oLED_OVER_TEMP_ANS                ANSELBbits.ANSB4
#define oLED_OVER_TEMP_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define oLED_OVER_TEMP_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define oLED_OVER_TEMP_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define oLED_OVER_TEMP_GetValue()           PORTBbits.RB4
#define oLED_OVER_TEMP_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define oLED_OVER_TEMP_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define oLED_OVER_TEMP_SetPullup()      do { WPUBbits.WPUB4 = 1; } while(0)
#define oLED_OVER_TEMP_ResetPullup()    do { WPUBbits.WPUB4 = 0; } while(0)
#define oLED_OVER_TEMP_SetPushPull()    do { ODCONBbits.ODCB4 = 1; } while(0)
#define oLED_OVER_TEMP_SetOpenDrain()   do { ODCONBbits.ODCB4 = 0; } while(0)
#define oLED_OVER_TEMP_SetAnalogMode()  do { ANSELBbits.ANSB4 = 1; } while(0)
#define oLED_OVER_TEMP_SetDigitalMode() do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set oLED_TEMP_WARN aliases
#define oLED_TEMP_WARN_TRIS               TRISBbits.TRISB5
#define oLED_TEMP_WARN_LAT                LATBbits.LATB5
#define oLED_TEMP_WARN_PORT               PORTBbits.RB5
#define oLED_TEMP_WARN_WPU                WPUBbits.WPUB5
#define oLED_TEMP_WARN_OD                ODCONBbits.ODCB5
#define oLED_TEMP_WARN_ANS                ANSELBbits.ANSB5
#define oLED_TEMP_WARN_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define oLED_TEMP_WARN_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define oLED_TEMP_WARN_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define oLED_TEMP_WARN_GetValue()           PORTBbits.RB5
#define oLED_TEMP_WARN_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define oLED_TEMP_WARN_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define oLED_TEMP_WARN_SetPullup()      do { WPUBbits.WPUB5 = 1; } while(0)
#define oLED_TEMP_WARN_ResetPullup()    do { WPUBbits.WPUB5 = 0; } while(0)
#define oLED_TEMP_WARN_SetPushPull()    do { ODCONBbits.ODCB5 = 1; } while(0)
#define oLED_TEMP_WARN_SetOpenDrain()   do { ODCONBbits.ODCB5 = 0; } while(0)
#define oLED_TEMP_WARN_SetAnalogMode()  do { ANSELBbits.ANSB5 = 1; } while(0)
#define oLED_TEMP_WARN_SetDigitalMode() do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set adc_Temp1 aliases
#define adc_Temp1_TRIS               TRISBbits.TRISB6
#define adc_Temp1_LAT                LATBbits.LATB6
#define adc_Temp1_PORT               PORTBbits.RB6
#define adc_Temp1_WPU                WPUBbits.WPUB6
#define adc_Temp1_OD                ODCONBbits.ODCB6
#define adc_Temp1_ANS                ANSELBbits.ANSB6
#define adc_Temp1_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define adc_Temp1_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define adc_Temp1_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define adc_Temp1_GetValue()           PORTBbits.RB6
#define adc_Temp1_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define adc_Temp1_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define adc_Temp1_SetPullup()      do { WPUBbits.WPUB6 = 1; } while(0)
#define adc_Temp1_ResetPullup()    do { WPUBbits.WPUB6 = 0; } while(0)
#define adc_Temp1_SetPushPull()    do { ODCONBbits.ODCB6 = 1; } while(0)
#define adc_Temp1_SetOpenDrain()   do { ODCONBbits.ODCB6 = 0; } while(0)
#define adc_Temp1_SetAnalogMode()  do { ANSELBbits.ANSB6 = 1; } while(0)
#define adc_Temp1_SetDigitalMode() do { ANSELBbits.ANSB6 = 0; } while(0)

// get/set adc_Temp2 aliases
#define adc_Temp2_TRIS               TRISBbits.TRISB7
#define adc_Temp2_LAT                LATBbits.LATB7
#define adc_Temp2_PORT               PORTBbits.RB7
#define adc_Temp2_WPU                WPUBbits.WPUB7
#define adc_Temp2_OD                ODCONBbits.ODCB7
#define adc_Temp2_ANS                ANSELBbits.ANSB7
#define adc_Temp2_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define adc_Temp2_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define adc_Temp2_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define adc_Temp2_GetValue()           PORTBbits.RB7
#define adc_Temp2_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define adc_Temp2_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define adc_Temp2_SetPullup()      do { WPUBbits.WPUB7 = 1; } while(0)
#define adc_Temp2_ResetPullup()    do { WPUBbits.WPUB7 = 0; } while(0)
#define adc_Temp2_SetPushPull()    do { ODCONBbits.ODCB7 = 1; } while(0)
#define adc_Temp2_SetOpenDrain()   do { ODCONBbits.ODCB7 = 0; } while(0)
#define adc_Temp2_SetAnalogMode()  do { ANSELBbits.ANSB7 = 1; } while(0)
#define adc_Temp2_SetDigitalMode() do { ANSELBbits.ANSB7 = 0; } while(0)

// get/set oPWM aliases
#define oPWM_TRIS               TRISCbits.TRISC0
#define oPWM_LAT                LATCbits.LATC0
#define oPWM_PORT               PORTCbits.RC0
#define oPWM_WPU                WPUCbits.WPUC0
#define oPWM_OD                ODCONCbits.ODCC0
#define oPWM_ANS                ANSELCbits.ANSC0
#define oPWM_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define oPWM_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define oPWM_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define oPWM_GetValue()           PORTCbits.RC0
#define oPWM_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define oPWM_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define oPWM_SetPullup()      do { WPUCbits.WPUC0 = 1; } while(0)
#define oPWM_ResetPullup()    do { WPUCbits.WPUC0 = 0; } while(0)
#define oPWM_SetPushPull()    do { ODCONCbits.ODCC0 = 1; } while(0)
#define oPWM_SetOpenDrain()   do { ODCONCbits.ODCC0 = 0; } while(0)
#define oPWM_SetAnalogMode()  do { ANSELCbits.ANSC0 = 1; } while(0)
#define oPWM_SetDigitalMode() do { ANSELCbits.ANSC0 = 0; } while(0)

// get/set iSTART_CTRL aliases
#define iSTART_CTRL_TRIS               TRISCbits.TRISC1
#define iSTART_CTRL_LAT                LATCbits.LATC1
#define iSTART_CTRL_PORT               PORTCbits.RC1
#define iSTART_CTRL_WPU                WPUCbits.WPUC1
#define iSTART_CTRL_OD                ODCONCbits.ODCC1
#define iSTART_CTRL_ANS                ANSELCbits.ANSC1
#define iSTART_CTRL_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define iSTART_CTRL_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define iSTART_CTRL_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define iSTART_CTRL_GetValue()           PORTCbits.RC1
#define iSTART_CTRL_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define iSTART_CTRL_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define iSTART_CTRL_SetPullup()      do { WPUCbits.WPUC1 = 1; } while(0)
#define iSTART_CTRL_ResetPullup()    do { WPUCbits.WPUC1 = 0; } while(0)
#define iSTART_CTRL_SetPushPull()    do { ODCONCbits.ODCC1 = 1; } while(0)
#define iSTART_CTRL_SetOpenDrain()   do { ODCONCbits.ODCC1 = 0; } while(0)
#define iSTART_CTRL_SetAnalogMode()  do { ANSELCbits.ANSC1 = 1; } while(0)
#define iSTART_CTRL_SetDigitalMode() do { ANSELCbits.ANSC1 = 0; } while(0)

// get/set iSTOP_CTRL aliases
#define iSTOP_CTRL_TRIS               TRISCbits.TRISC2
#define iSTOP_CTRL_LAT                LATCbits.LATC2
#define iSTOP_CTRL_PORT               PORTCbits.RC2
#define iSTOP_CTRL_WPU                WPUCbits.WPUC2
#define iSTOP_CTRL_OD                ODCONCbits.ODCC2
#define iSTOP_CTRL_ANS                ANSELCbits.ANSC2
#define iSTOP_CTRL_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define iSTOP_CTRL_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define iSTOP_CTRL_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define iSTOP_CTRL_GetValue()           PORTCbits.RC2
#define iSTOP_CTRL_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define iSTOP_CTRL_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define iSTOP_CTRL_SetPullup()      do { WPUCbits.WPUC2 = 1; } while(0)
#define iSTOP_CTRL_ResetPullup()    do { WPUCbits.WPUC2 = 0; } while(0)
#define iSTOP_CTRL_SetPushPull()    do { ODCONCbits.ODCC2 = 1; } while(0)
#define iSTOP_CTRL_SetOpenDrain()   do { ODCONCbits.ODCC2 = 0; } while(0)
#define iSTOP_CTRL_SetAnalogMode()  do { ANSELCbits.ANSC2 = 1; } while(0)
#define iSTOP_CTRL_SetDigitalMode() do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set iDWELL_OPT aliases
#define iDWELL_OPT_TRIS               TRISCbits.TRISC3
#define iDWELL_OPT_LAT                LATCbits.LATC3
#define iDWELL_OPT_PORT               PORTCbits.RC3
#define iDWELL_OPT_WPU                WPUCbits.WPUC3
#define iDWELL_OPT_OD                ODCONCbits.ODCC3
#define iDWELL_OPT_ANS                ANSELCbits.ANSC3
#define iDWELL_OPT_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define iDWELL_OPT_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define iDWELL_OPT_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define iDWELL_OPT_GetValue()           PORTCbits.RC3
#define iDWELL_OPT_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define iDWELL_OPT_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define iDWELL_OPT_SetPullup()      do { WPUCbits.WPUC3 = 1; } while(0)
#define iDWELL_OPT_ResetPullup()    do { WPUCbits.WPUC3 = 0; } while(0)
#define iDWELL_OPT_SetPushPull()    do { ODCONCbits.ODCC3 = 1; } while(0)
#define iDWELL_OPT_SetOpenDrain()   do { ODCONCbits.ODCC3 = 0; } while(0)
#define iDWELL_OPT_SetAnalogMode()  do { ANSELCbits.ANSC3 = 1; } while(0)
#define iDWELL_OPT_SetDigitalMode() do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set iRAMP_EN aliases
#define iRAMP_EN_TRIS               TRISCbits.TRISC4
#define iRAMP_EN_LAT                LATCbits.LATC4
#define iRAMP_EN_PORT               PORTCbits.RC4
#define iRAMP_EN_WPU                WPUCbits.WPUC4
#define iRAMP_EN_OD                ODCONCbits.ODCC4
#define iRAMP_EN_ANS                ANSELCbits.ANSC4
#define iRAMP_EN_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define iRAMP_EN_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define iRAMP_EN_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define iRAMP_EN_GetValue()           PORTCbits.RC4
#define iRAMP_EN_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define iRAMP_EN_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define iRAMP_EN_SetPullup()      do { WPUCbits.WPUC4 = 1; } while(0)
#define iRAMP_EN_ResetPullup()    do { WPUCbits.WPUC4 = 0; } while(0)
#define iRAMP_EN_SetPushPull()    do { ODCONCbits.ODCC4 = 1; } while(0)
#define iRAMP_EN_SetOpenDrain()   do { ODCONCbits.ODCC4 = 0; } while(0)
#define iRAMP_EN_SetAnalogMode()  do { ANSELCbits.ANSC4 = 1; } while(0)
#define iRAMP_EN_SetDigitalMode() do { ANSELCbits.ANSC4 = 0; } while(0)

// get/set iDutyCyleSW aliases
#define iDutyCyleSW_TRIS               TRISCbits.TRISC5
#define iDutyCyleSW_LAT                LATCbits.LATC5
#define iDutyCyleSW_PORT               PORTCbits.RC5
#define iDutyCyleSW_WPU                WPUCbits.WPUC5
#define iDutyCyleSW_OD                ODCONCbits.ODCC5
#define iDutyCyleSW_ANS                ANSELCbits.ANSC5
#define iDutyCyleSW_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define iDutyCyleSW_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define iDutyCyleSW_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define iDutyCyleSW_GetValue()           PORTCbits.RC5
#define iDutyCyleSW_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define iDutyCyleSW_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define iDutyCyleSW_SetPullup()      do { WPUCbits.WPUC5 = 1; } while(0)
#define iDutyCyleSW_ResetPullup()    do { WPUCbits.WPUC5 = 0; } while(0)
#define iDutyCyleSW_SetPushPull()    do { ODCONCbits.ODCC5 = 1; } while(0)
#define iDutyCyleSW_SetOpenDrain()   do { ODCONCbits.ODCC5 = 0; } while(0)
#define iDutyCyleSW_SetAnalogMode()  do { ANSELCbits.ANSC5 = 1; } while(0)
#define iDutyCyleSW_SetDigitalMode() do { ANSELCbits.ANSC5 = 0; } while(0)

// get/set iPwrGood aliases
#define iPwrGood_TRIS               TRISCbits.TRISC6
#define iPwrGood_LAT                LATCbits.LATC6
#define iPwrGood_PORT               PORTCbits.RC6
#define iPwrGood_WPU                WPUCbits.WPUC6
#define iPwrGood_OD                ODCONCbits.ODCC6
#define iPwrGood_ANS                ANSELCbits.ANSC6
#define iPwrGood_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define iPwrGood_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define iPwrGood_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define iPwrGood_GetValue()           PORTCbits.RC6
#define iPwrGood_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define iPwrGood_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define iPwrGood_SetPullup()      do { WPUCbits.WPUC6 = 1; } while(0)
#define iPwrGood_ResetPullup()    do { WPUCbits.WPUC6 = 0; } while(0)
#define iPwrGood_SetPushPull()    do { ODCONCbits.ODCC6 = 1; } while(0)
#define iPwrGood_SetOpenDrain()   do { ODCONCbits.ODCC6 = 0; } while(0)
#define iPwrGood_SetAnalogMode()  do { ANSELCbits.ANSC6 = 1; } while(0)
#define iPwrGood_SetDigitalMode() do { ANSELCbits.ANSC6 = 0; } while(0)

// get/set oLED_Dwell aliases
#define oLED_Dwell_TRIS               TRISCbits.TRISC7
#define oLED_Dwell_LAT                LATCbits.LATC7
#define oLED_Dwell_PORT               PORTCbits.RC7
#define oLED_Dwell_WPU                WPUCbits.WPUC7
#define oLED_Dwell_OD                ODCONCbits.ODCC7
#define oLED_Dwell_ANS                ANSELCbits.ANSC7
#define oLED_Dwell_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define oLED_Dwell_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define oLED_Dwell_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define oLED_Dwell_GetValue()           PORTCbits.RC7
#define oLED_Dwell_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define oLED_Dwell_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define oLED_Dwell_SetPullup()      do { WPUCbits.WPUC7 = 1; } while(0)
#define oLED_Dwell_ResetPullup()    do { WPUCbits.WPUC7 = 0; } while(0)
#define oLED_Dwell_SetPushPull()    do { ODCONCbits.ODCC7 = 1; } while(0)
#define oLED_Dwell_SetOpenDrain()   do { ODCONCbits.ODCC7 = 0; } while(0)
#define oLED_Dwell_SetAnalogMode()  do { ANSELCbits.ANSC7 = 1; } while(0)
#define oLED_Dwell_SetDigitalMode() do { ANSELCbits.ANSC7 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/