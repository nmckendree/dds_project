/* 
 * File:   led_controller.h
 * Author: Nick
 *
 * Created on January 23, 2017, 9:10 PM
 */

#ifndef LED_CONTROLLER_H
#define	LED_CONTROLLER_H

#ifdef	__cplusplus
extern "C" {
#endif


typedef enum{ LED_OFF, LED_ON, LED_BLINK_SLOW, LED_BLINK_FAST} LED_CONTROLS_t;
typedef enum{ LED_TEMP_WARN, LED_TEMP_ALARM, LED_DWELL_10MIN, LED_RUN} LED_ID_t;

typedef struct{
    LED_ID_t led_id;
    LED_CONTROLS_t led_state;
    uint8_t blink_counter;     
    uint8_t led_active_level;
    uint16_t time_last_ran;
}_LED_CONTROLLER_t;

#define CONTINOUS_BLINK            255


void set_led_state( _LED_CONTROLLER_t *led, LED_CONTROLS_t new_state, uint8_t new_LED_blink_cnt);
void led_controller( _LED_CONTROLLER_t *led, uint16_t ms_since_last_call);

#ifdef	__cplusplus
}
#endif

#endif	/* LED_CONTROLLER_H */

