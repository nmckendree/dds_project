/****************************************************************************  
FileName:     main.c                                                                                       
Dependencies: CCS libraries
Processor:    PIC18            
Hardware:
Complier: CCS     
Company:    
                                
                      
Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        10/2016    
                                         
*****************************************************************************
General description: Main code for the project              
                                                                    
                                                    
****************************************************************************
                            INCLUDES             
****************************************************************************/
#include "project.h"                                       
#include "system_Tick.h"    
#include "user_debounce.h" 
#include "Serial_Buffer.h"
#include "AD5930.h"                                                   
#include "hal_eeprom.h"
#include "eeprom.h"        
#include "pc_interface.h"                       
#include "lcd_8bit.h"       
#include "Menu_HAL.h"
#include "Menu.h"  
#include <string.h>
#include <stdio.h>  
/****************************************************************************
                            CCS SPECIFIC USE STATEMENTS            
****************************************************************************/       
                 
/****************************************************************************
                            DEFINES            
****************************************************************************/                                   
#define EEPROM_1_ADDR                   0x50    
#define TASK_10MS_PERIOD                (10 * SYSTICK_TICKS_PER_MS)
#define TASK_50MS_PERIOD                (50 * SYSTICK_TICKS_PER_MS)
#define TASK_100MS_PERIOD               (100 * SYSTICK_TICKS_PER_MS)  
#define TASK_500MS_PERIOD               (500 * SYSTICK_TICKS_PER_MS)    
#define TASK_1000MS_PERIOD              (1000 * SYSTICK_TICKS_PER_MS)                      
#define BUILD_TEST_EEPROM_DATA               
#define NUM_OF_TEST_FREQ                8
#define RUN_TEST_MODE
#define TEST_MODE_DWELL_TIME_S          180     //value in sec. 
//#define DISABLE_UART                    
/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/                                          
typedef enum { mSTOP, mPAUSE, mPLAY }_eModes;       

/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/
static _tDEBOUNCED_INPUT key_up;  
static _tDEBOUNCED_INPUT key_down;
static _tDEBOUNCED_INPUT key_left;
static _tDEBOUNCED_INPUT key_right;
static _tDEBOUNCED_INPUT key_sel; 
static _tDEBOUNCED_INPUT key_pause;
static _tDEBOUNCED_INPUT key_stop;      
static _tDEBOUNCED_INPUT key_power;      

static uint16_t subgrp_cntr = 0;     
uint16_t selected_grp_remaining_time_sec = 0;
uint16_t current_freq_index = 0;   
_tEepromDevice extMemory; 
_eModes sysMode = mSTOP;        
static uint8_t update_freq = 0; 
static uint32_t task_10mS = 0;
//static uint32_t task_50mS = 0;
static uint32_t task_100mS = 0;
static uint32_t task_1000mS = 0;
static uint8_t test_mode = 0;
#ifdef RUN_TEST_MODE
static uint32_t task_500mS = 0;
//Values are x100 to preserve decimal
static uint32_t test_mode_freq_list[ NUM_OF_TEST_FREQ] = 
{
    100,
    580,
    6870,
    78100,
    108500,
    303300,
    531300,
    1000000
};
static uint8_t test_mode_index = 0;
#endif
/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/  
void task10mS(void);
//void task50ms(void);
void task100ms(void);                       
void task1s(void);                               
void init_hardware( void );  
void build_eeprom_data(void);
void test_speed_up_cntdwn(void);
void test_eeprom_1(void);
int main(void);
void pull_data_for_display(uint16_t index, void* data);
void pull_current_status_for_display(uint16_t* pRemainingTime, uint16_t *pFreqIndex, uint8_t *_sysMode);
void menu_changed_data(uint16_t grp_cnt, uint16_t subgrp_cnt, uint16_t dwellTime);
uint8_t* get_emu_eeprom_buffer(uint8_t dev_addr);
uint32_t get_frequency_result( void ) ;
static _eKEY_SELECTION get_user_input( void )  ;
void process_system_mode( void );
void run_test_mode( void );
void test_eeprom( void );      
#ifdef BUILD_TEST_EEPROM_DATA
void build_test_libraries(void);
void build_tests(void);                       
void build_test_header(void);   
#endif     
#pragma warning disable 520         //unused function warning
#pragma warning disable 752         //Conversion to shorter data type warning - useless. Falsely triggers

/*********************************************************************
Function:    main
Input:       void
Output:      void
Dependencies:
Description: The main function
********************************************************************/ 
int main(void)            
{          
    uint8_t i = 0;
   
    init_hardware();           
    initSysTick();                       
    AD5930_write_def_control();  
    INTERRUPT_Initialize();
    INTERRUPT_PeripheralInterruptEnable();
    INTERRUPT_GlobalInterruptEnable();
    
#ifndef DISABLE_UART    
    //SerialInit();
#endif    
    initDispl();
    
    menu_Init();
    InitEEPROM( &extMemory, EEPROM_1_ADDR, (128UL * 500UL) );    
    
    WriteString((char*)"RPGreen Elec"); 
    SetPostion(LINE2);                   
    WriteString((char*)"LED Controller");  
    SetPostion(LINE3);
    WriteString((char*)"Version 1.0");      
#if defined BUILD_TEST_EEPROM_DATA && !defined RUN_TEST_MODE 
    SetPostion(LINE4);                              
    WriteString((char*)"Creating EEPROM Data");  
    EraseEEPROM( &extMemory );   
    build_test_libraries();
    build_tests();                       
    build_test_header();   
#endif            
      
    for(i = 0; i < 100; i++)
    {   //__delay_ms has low max time. based on sys osc. 64mhz osc clock. 
        __delay_ms(10); //startup delay to see splash screen  
        task10mS();         //junk read to setup debounce routine
    }
#if !defined RUN_TEST_MODE    
    scan_eeprom( &extMemory );                  
#endif
    get_user_input();       //junk read to setup debounce routine
    while( 1 )                     
    {     
#ifdef RUN_TEST_MODE
        if( timePassed( task_10mS ) >= TASK_10MS_PERIOD )
        {
            task10mS();  
            task_10mS = GetSysTick();   
        }   
        if( timePassed( task_500mS ) >= TASK_500MS_PERIOD )
        {                
            task_500mS = GetSysTick();   
            run_test_mode();
        }       
        if( timePassed( task_1000mS ) >= TASK_1000MS_PERIOD )
        {                   
              
            selected_grp_remaining_time_sec = ( selected_grp_remaining_time_sec > 0 ? selected_grp_remaining_time_sec - 1 : 0);
            task_1000mS = GetSysTick();   
        }       
        
//#ifndef DISABLE_UART
        pc_cmd_handler(); 
        if( serialMessageRdy )
        {
            if( pc_cmd_parse() == PC_MSG_HANDLED)
                serialMessageRdy = 0;
        }
//#endif      //DISABLE_UART        
#else
        
        //Execute a soft scheduler to run things at certain time intervals. 
        //Note: This is a soft scheduler because it is not an interrupt or OS task
        //and is subject to current execution. 
        if(SysTick_Ticked)
        {   
            SysTick_Ticked = 0;         
            if( timePassed( task_10mS ) >= TASK_10MS_PERIOD )
            {
                task10mS();  
                task_10mS = GetSysTick();   
            }                   
//            if( timePassed( task_50mS ) >= TASK_50MS_PERIOD )
//            {
//                task50mS();  
//                task_50mS = GetSysTick();   
//            }                  
            if( timePassed( task_100mS ) >= TASK_100MS_PERIOD )
            {                
                task_100mS = GetSysTick();   
                menu((_eKEY_SELECTION)get_user_input(), 100);
            }                                     
            if( timePassed( task_1000mS ) >= TASK_1000MS_PERIOD )
            {                   
                task1s();  
                task_1000mS = GetSysTick();   
            }        
        }
        /*Outside of the tasks we can process anything that may need immediate
          attention or go to sleep (if needed)  */     
#ifndef DISABLE_UART
        pc_cmd_handler(); 
        if( serialMessageRdy )
        {
            if( pc_cmd_parse() == PC_MSG_HANDLED)
                serialMessageRdy = 0;
        }
#endif      //DISABLE_UART


        if( update_freq )   
        {
            AD5930_write_freq( get_frequency_result() );   
            update_freq = 0;
        } 
#endif      //RUN_TEST_MODE             
    }                           
    return (0);                        
}                          
                                    
/*********************************************************************
Function:    init_hardware
Input:       void
Output:      void      
Dependencies:  
Description: init hardware components of the system        
                including the GPIO.
********************************************************************/        
void init_hardware( void )       
{    
    SYSTEM_Initialize();            //Microchip MCC generate function - init all hardware
                                      
    //Setup the debounce module for each user input. This will limit false triggers                         
    initDebounce( &key_up, 1);                                                      
    initDebounce( &key_down, 1);                 
    initDebounce( &key_left, 1);
    initDebounce( &key_right, 1);
    initDebounce( &key_sel, 1);
    initDebounce( &key_pause, 1);
    initDebounce( &key_stop, 1);      
    initDebounce( &key_power, 1);      

}

/*********************************************************************
Function:    task10mS
Input:       void
Output:      void                
Dependencies:                                            
Description: Task that runs every 10mS. This is a good location
            to read raw key inputs as it will need multiple hits for
            debouncing until it is valid.    
********************************************************************/             
void task10mS(void)                          
{                                          
    debounceInput( &key_up, BTN_UP_GetValue());       
    debounceInput( &key_down, BTN_DOWN_GetValue());
    debounceInput( &key_left, BTN_LEFT_GetValue());
    debounceInput( &key_right, BTN_RIGHT_GetValue());
    debounceInput( &key_sel, BTN_SELECT_GetValue());
    debounceInput( &key_pause, BTN_PAUSE_GetValue());
    debounceInput( &key_stop, BTN_STOP_GetValue());  
    debounceInput( &key_power, BTN_POWER_GetValue());    
    
  //  if( !serial_IsTXBusy(SERIAL_PC_TX))
  //      serial_StartTX(SERIAL_PC_TX);    
                                     
}                     

/*********************************************************************
Function:    task1s
Input:       void
Output:      void                                                                                        
Dependencies:
Description: Task that runs every 1S. Will remove this if not needed. 
********************************************************************/   
void task1s(void)                 
{           
    process_system_mode();   
}
        
/*********************************************************************
Function:     pull_data_for_display
Input:        uint16_t index: Page number to read from EEPROM                                 
              void* data:Pointer to an array to store the data
Return:       void    
Description:  This function is called from the menu module ("callback")
                that pulls data from the eeprom to be displayed. 
Visibility: Public
*********************************************************************/
void pull_data_for_display(uint16_t index, void* data)
{
    ReadExtEEPROM(extMemory, (index * 128), (uint8_t*)data, 128);  
}                                     

/*********************************************************************
Function:   get_frequency_result    
Input:      uint32_t *i_freq: Pointer to return the current frequency
Return:     void                                                        
Description: This function retrieves the current frequency (or next frequency)
            from eeprom. 
Visibility: Private     
*********************************************************************/
uint32_t get_frequency_result( void ) 
{  
    uint32_t ifreq;
    uint32_t t, t1;
    freq_t freq = { 0 }; 
#if 0                   

    
    uint16_t overflow_page = current_freq_index / 33;
    uint16_t overflow_offset = 28;   

    if (current_freq_index >= 33)
    {                                            
        overflow_offset = 4;
        overflow_offset += (current_freq_index - (33 * overflow_page)); 
    }                                                             
    overflow_offset = (((subgrp_lookup[selected_subgrp] + overflow_page) * 128) + overflow_offset) - 1;       
    ReadExtEEPROM(extMemory, overflow_offset, freq.freq, 3);
#endif                                                   
    freq = menu_subgroup[selected_subgrp].lib_struct.freqList[current_freq_index];

    //The data is retrieved as 3 bytes, lSB first. Adjust it to return as 32bit value   
    //CCS wouldn't work with oring ifreq at each line? Had to break it into sep. variables
    ifreq = freq.freq[0];   
    t = ((uint32_t)freq.freq[1] << 8);    
    t1 = (uint32_t)(freq.freq[2]) << 16;           
    
    ifreq |= (t | t1);    
                                        
    return ifreq;
}
     
/*********************************************************************
Function:    pull_current_status_for_display
Input:       uint16_t* pRemainingTime: Retruns the current systems remaining dwell time.                                        
            uint16_t *pFreqIndex: Returns the current system frequency index 
Return:     void
Description: "callback" function from menu module that gets the current
            frequency index of the current subtest and the remaining dwell time
            so it can be displayed on the display. 
Visibility: public                                                                     
*********************************************************************/
void pull_current_status_for_display(uint16_t* pRemainingTime, uint16_t *pFreqIndex, uint8_t *_sysMode)
{                                       
    *pRemainingTime = selected_grp_remaining_time_sec;        
    *pFreqIndex = current_freq_index;   
    *_sysMode = (uint8_t)sysMode;    

}

/*********************************************************************
Function:   menu_changed_data
Input:      uint16_t grp_cnt: New group ID selected   
            uint16_t subgrp_cnt: New subgroup ID selected                 
            uint16_t dwellTime: New dwelltime selected                         
Return:     void                                         
Description: The "callback" function is called from the menu module
            when a user has selected a new option in the menu for a test. 
            This will update the list maintained in main as the system's value. 
Visibility: public
*********************************************************************/        
void menu_changed_data(uint16_t _grp_cnt, uint16_t _subgrp_cnt, uint16_t dwellTime)
{                 
    //grp = _grp_cnt;              
    subgrp_cntr = _subgrp_cnt;                            
                      
    selected_grp_remaining_time_sec = dwellTime;
    current_freq_index = 0;                        
    sysMode = mPLAY;    
    update_freq = 1;     
        
                                    
}                                    
  
/*********************************************************************
Function:   get_user_input
Input:      void                        
Return:     _eKEY_SELECTION: key action                                         
Description: Returns key action based on debounced input 
Visibility: private
*********************************************************************/    
static _eKEY_SELECTION get_user_input( void )  
{    
    if( ButtonPressed( &key_stop))   
    {
        sysMode = mSTOP;                
    }  
    if( ButtonPressed( &key_pause))
    {    
        if( sysMode == mPLAY)
            sysMode = mPAUSE;  
        else
            sysMode = mPLAY;      
    }      

    if( ButtonPressed( &key_up ) )
    {
        return eKEY_UP;                 
    }                    
    else if( ButtonPressed( &key_down ) )
    {                              
        return eKEY_DOWN;    
    } 
    else if( ButtonPressed( &key_left ) )
    {                
        return eKEY_LEFT;    
    }                     
    else if( ButtonPressed( &key_right ) )
    {
        return eKEY_RIGHT;    
    }             
    else if( ButtonPressed( &key_sel ) )
    {                    
        return eKEY_SELECT;    
    }                         
    return eKEY_NONE; 
                        
}           

void process_system_mode( void )   
{
    if (sysMode == mPLAY)
    {           
        LED_DRIVER_EN_SetHigh();
        if (selected_grp_remaining_time_sec != 0) 
        {    
            selected_grp_remaining_time_sec--;
        }
        else
        {             
        
            update_freq = 1;    
            if (++current_freq_index < menu_subgroup[selected_subgrp].lib_struct.numFrequencies)
            {      
                selected_grp_remaining_time_sec = menu_subgroup[selected_subgrp].lib_struct.dwellTime;
            }
            else
            {
                subgrp_cntr++;
                selected_subgrp++;
                current_freq_index = 0;                        
                if (subgrp_cntr < currGenTest.test_struct.nuSubGrps)
                {
                    if (selected_subgrp >= 3)
                    {
                        selected_subgrp = 0;
                        external_menu_update_subgroup_list();
                    }
                    selected_grp_remaining_time_sec = menu_subgroup[selected_subgrp].lib_struct.dwellTime;    
                }
                else
                {
                    currGenTest.test_struct.name[0] = 0;
                    sysMode = mSTOP;      
                    LED_DRIVER_EN_SetLow();
                    subgrp_cntr = 0;
                    update_freq = 0;   
                    AD5930_shutdown();
                }                       
            }                    
        }
    }
    else if (sysMode == mSTOP)
    {                   
        selected_grp_remaining_time_sec = 0;
        current_freq_index = 0;
        selected_subgrp = 0; 
        subgrp_cntr = 0;  
        update_freq = 0; 
        AD5930_shutdown();     
        LED_DRIVER_EN_SetLow();
    }                      
    
}              

#if 0                                        
void test_eeprom( void )  
{    
    uint8_t data_wr[5] = {1,2,3,4,5};     
    uint8_t data_rd[5] = {0};    
    WriteExtEEPROM( extMemory, 0, data_wr, 5);         
    ReadExtEEPROM( extMemory, 0, data_rd, 5);    
    
    if( data_wr[0] == data_rd[0] )   
    {
       data_wr[0] == data_rd[0] + 1;    
    }
}                 
#endif    

#ifdef BUILD_TEST_EEPROM_DATA
void build_test_libraries(void)
{
    lib_eeprom_t libraries = { 0 };
    uint32_t freq;
    uint16_t byte_addr = 0;
                           
    libraries.lib_struct.test_id = 1;
    libraries.lib_struct.dwellTime = 180;
    libraries.lib_struct.numFrequencies = 1;
    freq = 100;        //1 hz
    libraries.lib_struct.freqList[0].freq[0] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[1] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[2] = (uint8_t)freq;
                                                      
    strncpy(libraries.lib_struct.name, "1 Hz Lib", 20);
    libraries.lib_struct.overflow = 0;            
    byte_addr = (4 + ((libraries.lib_struct.test_id - 1) * 2)) * 128;                                                                       
    WriteExtEEPROM(extMemory, byte_addr, libraries.bytes, 128);

    libraries.lib_struct.test_id = 2;            
    libraries.lib_struct.dwellTime = 180;            
    libraries.lib_struct.numFrequencies = 1;
    freq = 810;        //8.1hz
    libraries.lib_struct.freqList[0].freq[0] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[1] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[2] = (uint8_t)freq;

    strncpy(libraries.lib_struct.name, "8.1 Hz Lib", 20);
    libraries.lib_struct.overflow = 0;
    byte_addr = (4 + ((libraries.lib_struct.test_id - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, libraries.bytes, 128);

    libraries.lib_struct.test_id = 3;
    libraries.lib_struct.dwellTime = 180;
    libraries.lib_struct.numFrequencies = 1;
    freq = 100000;        //1khz
    libraries.lib_struct.freqList[0].freq[0] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[1] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[2] = (uint8_t)freq;
                                                          
    strncpy(libraries.lib_struct.name, "1 kHz Lib", 20);
    libraries.lib_struct.overflow = 0;
    byte_addr = (4 + ((libraries.lib_struct.test_id - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, libraries.bytes, 128);

    libraries.lib_struct.test_id = 4;
    libraries.lib_struct.dwellTime = 180;
    libraries.lib_struct.numFrequencies = 1;
    freq = 1000000;        //10khz
    libraries.lib_struct.freqList[0].freq[0] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[1] = (uint8_t)freq;
    freq = freq >> 8;
    libraries.lib_struct.freqList[0].freq[2] = (uint8_t)freq;

    strncpy(libraries.lib_struct.name, "10kHz Lib", 20);
    libraries.lib_struct.overflow = 0;
    byte_addr = (4 + ((libraries.lib_struct.test_id - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, libraries.bytes, 128);

}

void build_tests(void)
{                                                         
    test_eeprom_t tests = { 0 };
    uint16_t byte_addr = 0;

    tests.test_struct.nuSubGrps = 1;
    tests.test_struct.overflow = 0;
    tests.test_struct.subGrpList[0] = 1;
    strncpy(tests.test_struct.name, (uint8_t*)"1 Hz Test", 20);
    tests.test_struct.test_num = 1;
    byte_addr = (248 + 4 + ((tests.test_struct.test_num - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, tests.bytes, 128);

    tests.test_struct.nuSubGrps = 1;
    tests.test_struct.overflow = 0;                   
    tests.test_struct.subGrpList[0] = 2;
    strncpy(tests.test_struct.name, (uint8_t*) "8.1 Hz Test", 20);
    tests.test_struct.test_num = 2;
    byte_addr = (248 + 4 + ((tests.test_struct.test_num - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, tests.bytes, 128);

    tests.test_struct.nuSubGrps = 1;
    tests.test_struct.overflow = 0;
    tests.test_struct.subGrpList[0] = 3;
    strncpy(tests.test_struct.name, (uint8_t*)"1 KHz Test", 20);
    tests.test_struct.test_num = 3;
    byte_addr = (248 + 4 + ((tests.test_struct.test_num - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, tests.bytes, 128);  

    tests.test_struct.nuSubGrps = 1;
    tests.test_struct.overflow = 0;                         
    tests.test_struct.subGrpList[0] = 4;                   
    strncpy(tests.test_struct.name, (uint8_t*)"10 KHz Test", 20);
    tests.test_struct.test_num = 4;
    byte_addr = (248 + 4 + ((tests.test_struct.test_num - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, tests.bytes, 128);

    tests.test_struct.nuSubGrps = 4;
    tests.test_struct.overflow = 0;
    tests.test_struct.subGrpList[0] = 1;
    tests.test_struct.subGrpList[1] = 2;
    tests.test_struct.subGrpList[2] = 3;
    tests.test_struct.subGrpList[3] = 4;                                                                                            
    strncpy(tests.test_struct.name, (uint8_t*)"4 Freq Test", 20);                                                             
    tests.test_struct.test_num = 5;
    byte_addr = (248 + 4 + ((tests.test_struct.test_num - 1) * 2)) * 128;  
    WriteExtEEPROM(extMemory, byte_addr, tests.bytes, 128);
}

void build_test_header(void)
{
    hdr_eeprom_t header = { 0 };
                                                                                                                                               
    header.hdr_struct.eeprom_valid = 1;
    header.hdr_struct.eeprom_ver = 1;              
    header.hdr_struct.num_used_pages = 22;
    header.hdr_struct.num_data_bytes = (18 * 128);
    WriteExtEEPROM(extMemory, 0, header.bytes, HDR_LEN);
}
                
#endif                        

void run_test_mode( void )
{
    char disp_buff[21];
    uint16_t temp = 0;  
    uint16_t temp2 = 0;  
    
    switch( get_user_input() )
    {
        case eKEY_RIGHT:
            test_mode_index = ( (test_mode_index < (NUM_OF_TEST_FREQ - 1)) ? (test_mode_index + 1) : 0);
            sysMode = mSTOP;
            break;
        case eKEY_LEFT:
            test_mode_index = ( (test_mode_index > 0) ? (test_mode_index - 1) : (NUM_OF_TEST_FREQ - 1));
            sysMode = mSTOP;
            break;            
        case eKEY_SELECT:
            selected_grp_remaining_time_sec = TEST_MODE_DWELL_TIME_S;
            sysMode = mPLAY;
            AD5930_write_freq( test_mode_freq_list[ test_mode_index ] );    
            break;
    }
    temp = test_mode_freq_list[ test_mode_index ] / 100;     
    temp2 = test_mode_freq_list[ test_mode_index ]  % 100;                          
    while (temp2 > 9)
    {
        temp2 /= 10;                            
    }                                                 
    sprintf(disp_buff, "#:%6u   F:%4u.%1u", selected_grp_remaining_time_sec, temp, temp2); 
    ClrDisplay();
    WriteString( disp_buff);
    if (sysMode == mPLAY)
    {           
        LED_DRIVER_EN_SetHigh();
//        if (selected_grp_remaining_time_sec != 0) 
//        {    
//            selected_grp_remaining_time_sec--;
//        }
        if(selected_grp_remaining_time_sec == 0)
        {             
            test_mode_index = ( test_mode_index < NUM_OF_TEST_FREQ ? test_mode_index + 1 : 0);
            selected_grp_remaining_time_sec = TEST_MODE_DWELL_TIME_S;
            AD5930_write_freq( test_mode_freq_list[ test_mode_index ] );    
        }
    }
    else if (sysMode == mSTOP)
    {                   
        selected_grp_remaining_time_sec = 0;
        AD5930_shutdown();     
        LED_DRIVER_EN_SetLow();
    }                          
}