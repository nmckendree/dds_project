/****************************************************************************  
FileName:     serial_data_link_layer.c                                                                                         
Dependencies:  
Processor:    PIC18            
Hardware:
Complier:       
Company:                 
                                
                      
Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        12/2016    
                                         
*****************************************************************************
                                                         
                                                    
****************************************************************************
                            INCLUDES             
****************************************************************************/
#include "project.h"
#include "serial_data_link_layer.h"       
                 
/****************************************************************************
                            DEFINES            
****************************************************************************/   

/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/                                               

/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/
static link_layer_t tx_link_layer;
static link_layer_t rx_link_layer;
    
/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/  


/*********************************************************************
Function:    encode_byte 
Input:       uint8_t *data_in: Buffer for raw serial data
             uint8_t *data_out: Buffer for corrected data
Output:      uint8_t: 1: advance pointers, 0: advance data_in ptr only
Dependencies:
Description: encodes a frame with special characters. This 
             needs to be called in a loop where data_out is incremented
             externally only when 1 is returned. Length of buffers need
             to be handled externally.  
********************************************************************/  
uint8_t encode_byte(uint8_t *data_in, uint8_t *data_out)
{
	uint8_t return_val = true;

	if (*data_in == END_OF_DATA && tx_link_layer.prev_byte != DATA_ESCAPE_CHAR)
	{	
		*data_out = DATA_ESCAPE_CHAR;
	
		return_val = false;
	}
	else if (*data_in == START_OF_DATA && tx_link_layer.prev_byte != DATA_ESCAPE_CHAR)
	{	
		*data_out = DATA_ESCAPE_CHAR;
	
		return_val = false;
	}
	else if (*data_in == DATA_ESCAPE_CHAR && tx_link_layer.prev_byte != DATA_ESCAPE_CHAR)
	{	
		*data_out = DATA_ESCAPE_CHAR;
	
		return_val = false;
	}
	else
	{
		*data_out = *data_in;
		return_val = true;
	}
	tx_link_layer.prev_byte = *data_out;
	tx_link_layer.bytes_processed++;
	
	return return_val;
}

/*********************************************************************
Function:    send_start 
Input:       void
Output:      uint8_t: special character
Dependencies:
Description: Returns the start byte of packets  
********************************************************************/  
uint8_t send_start(void)
{
	return START_OF_DATA;
}

/*********************************************************************
Function:    send_end 
Input:       void
Output:      uint8_t: special character
Dependencies:
Description: Returns the end byte of packets  
********************************************************************/  
uint8_t send_end(void)
{
	return END_OF_DATA;
}

/*********************************************************************
Function:    reset_link_layer 
Input:       void
Output:      void
Dependencies:
Description: Clears internal variables. Must be called when setting up 
             a new byte-per-byte action 
********************************************************************/  
void reset_link_layer(void)
{
	tx_link_layer.bytes_processed = 0;
	tx_link_layer.prev_byte = 0;

	rx_link_layer.bytes_processed = 0;
	rx_link_layer.prev_byte = 0;
}

/*********************************************************************
Function:    encode_array 
Input:       uint8_t *data_in: Ptr to input data buffer
             uint8_t *data_out: Ptr to output data buffer 
             uint16_t max_len_in: Length of data in input buffer
             uint16_t max_len_out: Max size of the output buffer
Output:      uint16_t: Number of bytes processed
Dependencies:
Description: Encodes an array with special characters and 
             necessary escape characters 
             Will return 0 if failed due to array length (output)
********************************************************************/  
uint16_t encode_array(uint8_t *data_in, uint8_t *data_out, uint16_t max_len_in, uint16_t max_len_out)
{
	uint16_t i = 0;
	uint16_t total_cnt = 1;
	uint16_t j = 0xffff;

	*data_out = START_OF_DATA;
	data_out++;

	while (i < max_len_in)
	{
		if (total_cnt >= max_len_out)
			return 0;

		if ((*data_in == END_OF_DATA || *data_in == START_OF_DATA || *data_in == DATA_ESCAPE_CHAR) && i != j)
		{
			*data_out = DATA_ESCAPE_CHAR;
			data_out++;
			total_cnt++;
			j = i;
		}
		else
		{
			*data_out = *data_in;
			data_out++;
			data_in++;
			i++;
			total_cnt++;
			j = 0xffff;
		}

	}
	*data_out = END_OF_DATA;

	return total_cnt + 1;
}

/*********************************************************************
Function:    decode_array 
Input:       uint8_t *data_in: Ptr to input data buffer
             uint8_t *data_out: Ptr to output data buffer 
             uint16_t max_len_in: Length of data in input buffer
             uint16_t max_len_out: Max size of the output buffer
Output:      uint16_t: Number of bytes processed
Dependencies:
Description: Decodes an array with special characters and 
             necessary escape characters 
             Will return 0 if failed due to array length (output)
********************************************************************/  
uint16_t decode_array(uint8_t *data_in, uint8_t *data_out, uint16_t max_len_in, uint16_t max_len_out)
{
	uint16_t i = 0;
	uint16_t total_cnt = 0;

	while (i < max_len_in)
	{
		if (total_cnt >= max_len_out)
			return 0;

		if ((*data_in == DATA_ESCAPE_CHAR))
		{
			*data_out = *(data_in + 1);
			data_in += 2;
			i+=2;
			data_out++;
			total_cnt++;
		}
		else
		{
			if (*data_in == START_OF_DATA)
			{
				data_in++;
				i++;
			}
			else if (*data_in == END_OF_DATA)
			{
				break;
			}
			else
			{
				*data_out = *data_in;
				data_out++;
				data_in++;
				i++;
				total_cnt++;
			}
		}

	}

	return total_cnt;
}

/*********************************************************************
Function:    decode_byte 
Input:       uint8_t *data_in: Buffer for raw serial data
             uint8_t *data_out: Buffer for corrected data
Output:      uint8_t: 1: advance pointers, 0: advance data_in ptr only
Dependencies:
Description: Decodes a received frame with special characters. This 
             needs to be called in a loop where data_out is incremented
             externally only when 1 is returned. Length of buffers need
             to be handled externally.  
********************************************************************/  
uint8_t decode_byte(uint8_t *data_in, uint8_t *data_out)
{
	uint8_t return_val = 0;


	if(*data_in == DATA_ESCAPE_CHAR && rx_link_layer.prev_byte != DATA_ESCAPE_CHAR)
	{
		return_val = 0;
	}
	else if (*data_in == END_OF_DATA && rx_link_layer.prev_byte != DATA_ESCAPE_CHAR)
	{
		return_val = END_OF_NEW_MSG;
	}
	else if (*data_in == START_OF_DATA && rx_link_layer.prev_byte != DATA_ESCAPE_CHAR)
	{
		reset_link_layer();
		return_val = BEGINNING_OF_NEW_MSG;
	}
	else
	{
		return_val = 1;
	}

	if (return_val == 1)
	{
		*data_out = *data_in;
	}
	rx_link_layer.prev_byte = *data_in;
	rx_link_layer.bytes_processed++;

	return return_val;
}

