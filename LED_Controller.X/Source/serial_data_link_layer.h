/****************************************************************************
 FileName:     serial_data_link_layer.h
 Dependencies:
 Processor:     
 Hardware:      
 Complier:      
 Company:       
          

 Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 

*****************************************************************************
General description:                          


Dependencies:


usage:

****************************************************************************
Public Functions:

****************************************************************************/
                       
#ifndef __SERIAL_DATA_LINK_LAYER_H
#define __SERIAL_DATA_LINK_LAYER_H


/****************************************************************************
                        Includes
****************************************************************************/
#include <stdint.h>

/****************************************************************************
                        GLOBAL TYPEDEFS
 *****************************************************************************/
typedef struct {
	uint8_t prev_byte;
	uint8_t bytes_processed;
}link_layer_t;

/****************************************************************************
                        DEFINES AND ENUMARATED VALUES
*****************************************************************************/
#define START_OF_DATA			0x02
#define END_OF_DATA				0x17
#define DATA_ESCAPE_CHAR		0x10
#define BEGINNING_OF_NEW_MSG	0x02
#define END_OF_NEW_MSG			0x03

/****************************************************************************
                        MACROS
*****************************************************************************/

/****************************************************************************
                        GLOBAL FUNCTION PROTOTYPES
*****************************************************************************/                                    
uint8_t encode_byte(uint8_t *data_in, uint8_t *data_out);
uint8_t send_start(void);
uint8_t send_end(void);
void reset_link_layer(void);
uint16_t encode_array(uint8_t *data_in, uint8_t *data_out, uint16_t max_len_in, uint16_t max_len_out);
uint16_t decode_array(uint8_t *data_in, uint8_t *data_out, uint16_t max_len_in, uint16_t max_len_out);
uint8_t decode_byte(uint8_t *data_in, uint8_t *data_out);

/****************************************************************************
                        GLOBAL VARIABLES
******************************************************************************/

#endif    /* __SERIAL_DATA_LINK_LAYER_H */
