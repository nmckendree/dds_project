//-----------------------------------------------------
//File: lcd_8bit.h
//Auth: ELECTRONIC ASSEMBLY JM
//DATE: 08-25-2013
//-----------------------------------------------------
#ifndef LCDSPI_H
#define LCDSPI_H

//--- Standard definitions for LCD ---
#define LCD_HOME_L1     0x80
#define LINE1           0 
#define LINE2           LINE1+0x20
#define LINE3           LINE1+0x40
#define LINE4           LINE1+0x60

#define DISPLAY_MSK     0x04
#define CURSOR_MSK    0x02
#define BLINK_MSK    0x01


#define TOPVIEW        0x05
#define BOTTOMVIEW    0x06

#define ROMA        0x00
#define ROMB        0x04
#define ROMC        0x0C



void initDispl(void);                                     


void WriteChar        (char character);
void WriteString    (char * string);  
void SetPostion        (uint8_t pos);
void DisplayOnOff    (uint8_t data);
void DefineCharacter(uint8_t postion, uint8_t *data);
void ClrDisplay        (void);
void SetContrast    (uint8_t contr);
void SetView        (uint8_t view);
void SetROM            (uint8_t rom_data);
#define GETCURSORADDR()    CheckBusy()
                 
#define STOP_CHAR           0x01   
#define PLAY_CHAR           0x02   
#define PAUSE_CHAR          0x03
 #define mSEND_STOP_CHAR()    ( WriteChar( STOP_CHAR) )
 #define mSEND_PLAY_CHAR()    ( WriteChar( PLAY_CHAR) )
 #define mSEND_PAUSE_CHAR()    ( WriteChar( PAUSE_CHAR) )
//Normally you don't need these functions outside this module
static void WriteIns        (uint8_t ins);
static void WriteData        (uint8_t data);
void display_generate_cust_chars( void );      
uint8_t CheckBusy        (void);

#endif
