/****************************************************************************  
FileName:     hal_eeprom.c
Dependencies: CCS libraries
Processor:    PIC18            
Hardware:
Complier: CCS 
Company:    


Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        10/2016    
                                    
*****************************************************************************
General description: "Low level functions" for the eeprom to interface with the I2C  
                                                             
                                                    
****************************************************************************
                            INCLUDES
****************************************************************************/       
#include <stdint.h>
#include "mcc.h"
#include "hal_eeprom.h"                      

/****************************************************************************
                            DEFINES            
****************************************************************************/                       

/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/   

/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/

/****************************************************************************
                            FUNCTION PROTOTYPES                                                       
****************************************************************************/     
uint8_t hal_i2c_send_pkt(uint8_t rcvr/*, uint16_t mem_addr*/,uint8_t* packet, uint8_t numBytes);
uint8_t hal_i2c_get_pkt(uint8_t i2cAddr, uint16_t mem_addr, uint8_t* packet, uint8_t  numBytes);
                                                                                                 
            
/*********************************************************************
Function:   hal_i2c_send_pkt   
Input:      uint8_t i2cAddr: device address
            uint32_t mem_addr: address of memory location             
            uint8_t* packet: pointer to array of data to wrie 
            uint32_t  numBytes: number of bytes to write                              
Return:     uint8_t: flag if ack(s) were received.        
Description:  wrapper function for the CCS i2c write functions 
Visibility: public      
Author:        NAM
*********************************************************************/                            
uint8_t hal_i2c_send_pkt(uint8_t rcvr, /*uint16_t mem_addr,*/uint8_t* packet, uint8_t numBytes)     
{                                          
    //uint16_t i = 0;     
    //uint8_t ack = 0;  
    I2C2_MESSAGE_STATUS status;
    uint16_t timeOut = 0;
   // uint16_t counter = 0;    
//    
    while(status != I2C2_MESSAGE_FAIL)
    {
        I2C2_MasterWrite(packet,(uint8_t)numBytes,rcvr,&status);
        // wait for the message to be sent or status has changed.
        while(status == I2C2_MESSAGE_PENDING);
        if (status == I2C2_MESSAGE_COMPLETE)
            return 1;
        // if status is  I2C2_MESSAGE_ADDRESS_NO_ACK,
          //               or I2C2_DATA_NO_ACK,
          // The device may be busy and needs more time for the last
          // write so we can retry writing the data, this is why we
          // use a while loop here

          // check for max retry and skip this byte
        if (timeOut == 200)
            return 0;
        else
            timeOut++;        
    }
    return 0;
}             

/*********************************************************************
Function:   hal_i2c_get_pkt   
Input:      uint8_t i2cAddr: device address
            uint32_t mem_addr: address of memory location             
            uint8_t* packet: pointer to array of data to read into                             
            uint32_t  numBytes: number of bytes to read                              
Return:     uint8_t: flag if ack(s) were received.        
Description:  wrapper function for the CCS i2c read functions 
Visibility: public      
Author:        NAM
*********************************************************************/      
uint8_t hal_i2c_get_pkt(uint8_t i2cAddr, uint16_t mem_addr, uint8_t* packet, uint8_t  numBytes)  
{
    //uint16_t i = 0;
    //uint8_t ack = 0;     
    I2C2_MESSAGE_STATUS status;
    I2C2_TRANSACTION_REQUEST_BLOCK readTRB[2];
    uint8_t     writeBuffer[3];
    uint16_t    timeOut;    

    // this initial value is important
    status = I2C2_MESSAGE_PENDING;

    // build the write buffer first
    // starting address of the EEPROM memory
    writeBuffer[0] = (mem_addr >> 8);                        // high address
    writeBuffer[1] = (uint8_t)(mem_addr);                    // low low address
                
    // we need to create the TRBs for a random read sequence to the EEPROM
    // Build TRB for sending address
    I2C2_MasterWriteTRBBuild(   &readTRB[0],
                                    writeBuffer,
                                    2,
                                    i2cAddr);
    // Build TRB for receiving data
    I2C2_MasterReadTRBBuild(    &readTRB[1],
                                    packet,
                                    (uint8_t)numBytes,
                                    i2cAddr);

    timeOut = 0;
    while(status != I2C2_MESSAGE_FAIL)
    {
        // now send the transactions
        I2C2_MasterTRBInsert(2, readTRB, &status);

        // wait for the message to be sent or status has changed.
        while(status == I2C2_MESSAGE_PENDING);

        if (status == I2C2_MESSAGE_COMPLETE)
            return 1;

        // if status is  I2C2_MESSAGE_ADDRESS_NO_ACK,
        //               or I2C2_DATA_NO_ACK,
        // The device may be busy and needs more time for the last
        // write so we can retry writing the data, this is why we
        // use a while loop here

        // check for max retry and skip this byte
        if (timeOut == 200)
            return 0;
        else
            timeOut++;

    }
   
   return 0;
}                      

