/****************************************************************************  
FileName:     AD5930.c
Dependencies:
Processor:
Hardware:
Complier: CCS 
Company:    


Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                         
                                    
*****************************************************************************
General description:  Basic device driver for the DDS IC


****************************************************************************
                            INCLUDES
****************************************************************************/
#include "project.h"
#include <stdint.h>   
#include "AD5930.h"

/****************************************************************************
                            CCS SPECIFIC USE STATEMENTS            
****************************************************************************/ 

/****************************************************************************
                            DEFINES            
****************************************************************************/  
#define AD5930_CTRL_ADDR                0UL
#define AD5930_NUM_INCREMENTS_ADDR      1UL
#define AD5930_DELTA_FREQ_LSB_ADDR      2UL
#define AD5930_DELTA_FREQ_MSB_ADDR      3UL
#define AD5930_INC_INTERVAL_ADDR        4UL
#define AD5930_BURST_INTERVAL_ADDR      8UL
#define AD5930_FSTART_LSB_ADDR          12UL
#define AD5930_FSTART_MSB_ADDR          13UL               
         
#define REG_ADDR_SHIFT                  12UL               
/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/    
//The AD5930 is accessed with 16 bits of data. I created this union to 
//handle both control register writes and/or data writes.
typedef union{  
  uint16_t word;
  uint8_t register_bytes[2];
  struct{      
    uint16_t data_bits;                  
  }register_bits;
  struct{
    uint8_t Reserved2:1;       
    uint8_t Reserved1:1;    
    uint8_t SyncOut_En:1;    
    uint8_t SyncSel:1;
    uint8_t Mode:1;   
    uint8_t IntExtIncr:1;
    uint8_t IntExtBurst:1;    
    uint8_t CWBurst:1;
    uint8_t MSB_Dig_Out:1;  
    uint8_t Sin_nTri_Out:1;                            
    uint8_t DAC_En:1;
    uint8_t B24:1;  
    uint8_t addr_bits:4;
  }control_bits;              
}AD5930_Reg_t;  

/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/
   
/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/
                       
                          
/*********************************************************************
Function:    AD5930_write_def_control 
Input:       void    
Output:      void
Dependencies:
Description: Sets up the DDS IC with a default configuration       
********************************************************************/    
void AD5930_write_def_control( void )
{
    AD5930_Reg_t reg = {0};   
                                        
    freq_Standby_SetLow();               
      
    reg.control_bits.addr_bits = AD5930_CTRL_ADDR;       //msb
    reg.control_bits.B24 = 1;                                   
    reg.control_bits.DAC_En = 1;                                   
    reg.control_bits.Sin_nTri_Out = 1;                                   
    reg.control_bits.MSB_Dig_Out = 1;                                   
    reg.control_bits.CWBurst = 1;                                   
    reg.control_bits.IntExtBurst = 1;                                   
    reg.control_bits.IntExtIncr = 1;                                   
    reg.control_bits.Mode = 1;                                   
    reg.control_bits.SyncSel = 1;                                   
    reg.control_bits.SyncOut_En = 1;                                   
    reg.control_bits.Reserved1 = 1;     //should always be 1                                  
    reg.control_bits.Reserved2 = 1;     //should always be 1   
                             
    freq_FSYNC_SetLow();
    SPI1_Exchange8bit(reg.register_bytes[1]); 
    SPI1_Exchange8bit(reg.register_bytes[0]);    
    freq_FSYNC_SetHigh();     

}                         
                      
/*********************************************************************
Function:    AD5930_write_freq                                  
Input:        uint32_t freq: 32bit frequency, x100
Output:      void 
Dependencies: floating point library                                                         
Description:  Take a 32bit frequency (x100 for precision)
            and calcuate the correct Fstart value to write to the chip.      
            NOTE: On FSTART writes after the very 1st one, I had to 
            reinit the control register which resets the internal state 
            machine. If this is not done, the following low to high freq_control 
            toggle will sweep to next freq, but those other registers
            are not configured for this application. 
********************************************************************/             
void AD5930_write_freq( uint32_t freq )  
{                         
    AD5930_Reg_t reg = {0};                    
    uint32_t data = 0;    
                                         
    AD5930_write_def_control();       
    //assume input freq is x100 for extra accuracy
    //use line created in excel to generate the data word     
    //drop floating point if needed   
#if defined _100KHZ_REF_CLK    
    data = ((1.6775*(float)freq) - 0.1818);  
#elif defined _500KHZ_REF_CLK          
    data = (( 0.3353 *(float)freq) - 0.1818);                          
#else
    data = ((1.6775*(float)freq) - 0.1818);  
#endif                                     
    freq_Control_SetLow();     
    reg.word = AD5930_FSTART_LSB_ADDR << REG_ADDR_SHIFT;               
    reg.word |= (data & 0xFFF);                             //Get LSbits
    //Write to SPI - writes are 8bit so send the MSB first
    //FSYNC is the chip select and is active low.
    freq_FSYNC_SetLow();          
    SPI1_Exchange8bit(reg.register_bytes[1]);           
    SPI1_Exchange8bit(reg.register_bytes[0]);    
    freq_FSYNC_SetHigh();   
    reg.word = AD5930_FSTART_MSB_ADDR << REG_ADDR_SHIFT;    
    reg.word |= ((data >> 12) & 0xFFF);                     //shift down to send MSbits 
    freq_FSYNC_SetLow(); 
    SPI1_Exchange8bit(reg.register_bytes[1]);    
    SPI1_Exchange8bit(reg.register_bytes[0]);       
    freq_FSYNC_SetHigh();;                           
    freq_Control_SetHigh() ;   
}   
  
/*********************************************************************
Function:    AD5930_shutdown                                  
Input:       void
Output:      void 
Dependencies: 
Description: Disables the DAC setting whihc places the IC in low power
               mode. Finally the standby line is set high which
               places the IC in the lowest power mode. 
********************************************************************/     
void AD5930_shutdown( void )   
{                
    AD5930_Reg_t reg = {0}; 
    reg.control_bits.addr_bits = AD5930_CTRL_ADDR;       //msb
    reg.control_bits.B24 = 1;                                   
    reg.control_bits.DAC_En = 0;                                   
    reg.control_bits.Sin_nTri_Out = 1;                                   
    reg.control_bits.MSB_Dig_Out = 0;                                   
    reg.control_bits.CWBurst = 1;                                   
    reg.control_bits.IntExtBurst = 1;                                   
    reg.control_bits.IntExtIncr = 1;                                   
    reg.control_bits.Mode = 1;                                   
    reg.control_bits.SyncSel = 1;                                   
    reg.control_bits.SyncOut_En = 1;                                   
    reg.control_bits.Reserved1 = 1;     //should always be 1                                  
    reg.control_bits.Reserved2 = 1;     //should always be 1   
                             
    freq_FSYNC_SetLow();
    SPI1_Exchange8bit(reg.register_bytes[1]); 
    SPI1_Exchange8bit(reg.register_bytes[0]);    
    freq_FSYNC_SetHigh();    
    
    freq_Standby_SetHigh();     
}

                                                                        
