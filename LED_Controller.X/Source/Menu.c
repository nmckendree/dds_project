/******************************************************************************************

******************************************************************************************
FileName:        Menu.c
Dependencies:    See INCLUDES section below
Processor:       PIC18

Tested With:     CCS

Description:
This module contains functions to navigate the menu and change the menu data
The menu module is setup so that all writes are down to a RAM buffer which
means only 1 write to the display when it is ready.

Author(s)                       Date            Version     Comment
******************************************************************************************
Nick Mckendree                  10/2016
******************************************************************************************/

/*****************************************************************************************
PRIVATE INCLUDE FILES
******************************************************************************************/
//#include <inttypes.h>
#include <stdint.h>  
#include "string.h"
#include "stdio.h"     
#include "Menu.h"      
#include "Menu_HAL.h"
#include "eeprom.h"
#include "lcd_8bit.h"

/*****************************************************************************************
PRIVATE GLOBAL DEFINES
******************************************************************************************/
#define MENU_TIMEOUT_MS                     15000   //If this elapses, return to the status menu 
/*****************************************************************************************
PRIVATE GLOBAL DataTypes
******************************************************************************************/

/*****************************************************************************************
PRIVATE GLOBAL VARIABLES
******************************************************************************************/
//Should be able to make this a const but CCS has issue with pointers to ROM space. 
char main_menu_txt[4][21] = {
    { "Main" },
    { "General Library" },
    { "Custom Library" },
    { "Settings" }
};
int8_t menu_buffer[NUM_LINES][NUM_COLS] = { 0 };    //Buffer that stores everything on the display
MENU_SCR_NUM_ENUM curr_menu = 0;                    // What menu are we in    
static uint8_t line;                                // Cursor line number (1-4)
static uint8_t column;                              // Cursor column number (1-20)
static uint32_t menu_timeout = MENU_TIMEOUT_MS;
uint8_t menu_has_changed = 0;                       //flag indicating the display needs to be redrawn
test_eeprom_t currGenTest = { 0 };                  //Store the current gen test
lib_eeprom_t menu_subgroup[3] = { 0 };              //Store the 3 subgroups that are on the menu                           
uint16_t selected_subgrp = 0;                        //INdex that keps the selected subgroup                                           
uint16_t menu_page = 0;                             //What page are we on? use to index the general tests                                      
static uint16_t menu_sel_page = 0;                         //Used to index what subgroup we are on                                                    
uint8_t call_new_menu = 1;
/*****************************************************************************************
PRIVATE FUNCTION PROTOTYPES
******************************************************************************************/
static void menu_clear(void);
static void menu_puts(int8_t *s);
static void menu_putchar(int8_t x);
static void menu_move_cursor(uint8_t new_column, uint8_t new_line, uint8_t show);
static void menu_set_xy(uint8_t new_column, uint8_t new_line);
static void menu_draw_cursor(void);
static void menu_UpdateSystemDisplay(_eKEY_SELECTION *key);
static void menu_NewMenu(void);
static void menu_settings(_eKEY_SELECTION *key);
static void menu_cust_lib(_eKEY_SELECTION *key);
static void menu_gen_lib(_eKEY_SELECTION *key);
static void menu_main(_eKEY_SELECTION *key);
static void menu_ServiceDisplay(_eKEY_SELECTION *key);
void menu(_eKEY_SELECTION key, uint32_t time_since_last_called_ms);
void menu_Init(void);
void external_menu_update_subgroup_list(void );
/*****************************************************************************************
CALLBACK FUNCTION PROTOTYPES
******************************************************************************************/
extern void pull_data_for_display(uint16_t index, void* data);
extern void pull_current_status_for_display(uint16_t* pRemainingTime, uint16_t *pFreqIndex, uint8_t *_sysMode);
extern void menu_changed_data(uint16_t grp_cnt, uint16_t subgrp_cnt, uint16_t dwellTime);
// -- BEGIN CODE -- 

/**********************************************************************
Function:   menu_Init
Inputs:     void
Outputs:    void
Description:Inits the menu variables
**********************************************************************/
void menu_Init(void)
{
    menu_sel_page = 0;
    line = 0;     //cursor line number
    column = 0;     //cursor column number
    menu_timeout = MENU_TIMEOUT_MS;
    // clear screen and display model/version info
    menu_clear();
    curr_menu = NORM_OP_DISPLAY;
    menu_NewMenu();
}

/**********************************************************************
Function:   menu_main
Inputs:     _eKEY_SELECTION key: Current key
uint32_t time_since_last_called_ms: How long since this function was last called
Outputs:    void
Description:Global interface to the menu module.
**********************************************************************/
void menu(_eKEY_SELECTION key, uint32_t time_since_last_called_ms)
{
    menu_ServiceDisplay(&key);

    if (curr_menu != NORM_OP_DISPLAY)
    {
        menu_timeout = (menu_timeout > time_since_last_called_ms) ? (menu_timeout - time_since_last_called_ms) : 0;
        if (menu_timeout == 0)
        {
            curr_menu = NORM_OP_DISPLAY;
        }
    }
    if (call_new_menu == 1)
    {
        call_new_menu = 0;
        menu_NewMenu();
    }

    if (menu_has_changed == 1)
    {
        menu_HAL_write_display_buffer((uint8_t*)&menu_buffer[0][0], NUM_COLS * NUM_LINES);
        menu_has_changed = 0;
        menu_timeout = MENU_TIMEOUT_MS;
    }
}

/**********************************************************************
Function:   menu_ServiceDisplay
Inputs:     _eKEY_SELECTION key: Current key
Outputs:    void
Description:Menu Directory
**********************************************************************/
static void menu_ServiceDisplay(_eKEY_SELECTION *key)
{
    switch (curr_menu)
    {
    case  MAIN:
        menu_main(key);
        break;
    case GEN_LIBRARY:
        menu_gen_lib(key);
        break;
    case CUST_LIBRARY:
        menu_cust_lib(key);
        break;
    case SETTINGS:
        menu_gen_lib(key);
        break;
    case GRP_MENU:
        break;
    case SUBGRP_MENU:
        break;
    case NORM_OP_DISPLAY:
        menu_UpdateSystemDisplay(key);
        break;
    default:
        break;
    }
    return;
}

/**********************************************************************
Function:   menu_main
Inputs:     _eKEY_SELECTION key: Current key
Outputs:    void
Description:Provides navigation (cursor movement and selection) for
the top level menu (MAIN).
**********************************************************************/
static void menu_main(_eKEY_SELECTION *key)
{
    switch (*key)
    {
    case eKEY_SELECT:
        curr_menu = (uint8_t)line;
        call_new_menu = 1;  //menu_NewMenu();
        break;
    case eKEY_UP:
        menu_putchar(' ');  //erase current cursor
        if (line > 1)
            line--;
        menu_move_cursor(0, line, 1);
        break;
    case eKEY_DOWN:
        menu_putchar(' ');  //erase current cursor
        if (line < (NUM_LINES - 1))
            line++;
        menu_move_cursor(0, line, 1);
        break;
    case eKEY_LEFT:
        if (curr_menu != MAIN)
            curr_menu = MAIN;
        else
            curr_menu = NORM_OP_DISPLAY;
        call_new_menu = 1;  //menu_NewMenu();
        break;
    default:
        // do nothing
        break;
    }
    *key = eKEY_NONE;        //consume key  
}

/**********************************************************************
Function:   menu_gen_lib
Inputs:     _eKEY_SELECTION key: Current key
Outputs:    void
Description:Menu for the general library sub menu
**********************************************************************/
static void menu_gen_lib(_eKEY_SELECTION *key)
{
    switch (*key)
    {
    case eKEY_SELECT:
        //selected_subgrp = ((line - 1) + (menu_sel_page * 3));
        selected_subgrp = (line - 1);
        menu_changed_data(menu_page, ((line - 1) + (menu_sel_page * 3)), menu_subgroup[selected_subgrp].lib_struct.dwellTime);
        curr_menu = NORM_OP_DISPLAY;
        call_new_menu = 1;  //menu_NewMenu();
        break;
    case eKEY_UP:
        menu_putchar(' ');  //erase current cursor
        if (line > 1)
            line--;
        else
            if (menu_sel_page > 0)
            {
                menu_sel_page--;
                call_new_menu = 1;  //menu_NewMenu();
            }
        menu_move_cursor(0, line, 1);
        break;
    case eKEY_DOWN:
        menu_putchar(' ');  //erase current cursor
        if (line < (NUM_LINES - 1))
            line++;
        else
            if (menu_sel_page < (currGenTest.test_struct.nuSubGrps / 3))
            {
                menu_sel_page++;
                call_new_menu = 1;  //menu_NewMenu();
            }
        menu_move_cursor(0, line, 1);
        break;
    case eKEY_LEFT:
        if (menu_page != 0)
        {
            menu_page--;
        }
        else
        {
            curr_menu = MAIN;
            menu_page = 0;
        }

        menu_sel_page = 0;
        call_new_menu = 1;  //menu_NewMenu();
        break;
    case eKEY_RIGHT:
        if (++menu_page < found_tests)
        {
            menu_sel_page = 0;
            call_new_menu = 1;  //menu_NewMenu();
        }
        else
            menu_page--;
        break;
    default:
        // do nothing
        break;
    }
    *key = eKEY_NONE;        //consume key  
}

/**********************************************************************
Function:   menu_cust_lib
Inputs:     _eKEY_SELECTION key: Current key
Outputs:    void
Description:Menu for the custom library submenu
**********************************************************************/
static void menu_cust_lib(_eKEY_SELECTION *key)
{
    switch (*key)
    {
    case eKEY_SELECT:
        break;
    case eKEY_UP:
        menu_putchar(' ');  //erase current cursor
        if (line > 1)
            line--;
        menu_move_cursor(0, line, 1);
        break;
    case eKEY_DOWN:
        menu_putchar(' ');  //erase current cursor
        if (line < (NUM_LINES - 1))
            line++;
        menu_move_cursor(0, line, 1);
        break;
    case eKEY_LEFT:
        if (curr_menu != MAIN)
            curr_menu = MAIN;
        call_new_menu = 1;  //menu_NewMenu();
        break;
    default:
        break;
    }
    *key = eKEY_NONE;        //consume key  
}

/**********************************************************************
Function:   menu_settings
Inputs:     _eKEY_SELECTION key: Current key
Outputs:    void
Description:Menu for the settings submenu
**********************************************************************/
static void menu_settings(_eKEY_SELECTION *key)
{
    switch (*key)
    {
    case eKEY_SELECT:
        break;
    case eKEY_UP:
        menu_putchar(' ');  //erase current cursor
        if (line > 1)
            line--;
        menu_move_cursor(0, line, 1);
        break;
    case eKEY_DOWN:
        menu_putchar(' ');  //erase current cursor
        if (line < (NUM_LINES - 1))
            line++;
        menu_move_cursor(0, line, 1);
        break;
    case eKEY_LEFT:
        if (curr_menu != MAIN)
            curr_menu = MAIN;
        call_new_menu = 1;  //menu_NewMenu();
        break;
    default:
        break;
    }
    *key = eKEY_NONE;        //consume key  
}

/**********************************************************************
Function:   menu_NewMenu
Inputs:     void
Outputs:    void
Description: Initially draws the new menu. Called when there is a menu
transition.
**********************************************************************/
static void menu_NewMenu(void)
{
    uint16_t temp = 0;
    uint8_t subGrp_Index = 0;
    // clear screen
    menu_clear();

    //add the updates/data to the screen
    switch (curr_menu)
    {
    case MAIN:    //main menu page
                  //Text for main    
        menu_set_xy(1, 0);
        menu_puts((int8_t*)(&main_menu_txt[0][0]));
        menu_set_xy(1, 1);
        menu_puts((int8_t*)(&main_menu_txt[1][0]));
        menu_set_xy(1, 2);
        menu_puts((int8_t*)(&main_menu_txt[2][0]));
        menu_set_xy(1, 3);
        menu_puts((int8_t*)(&main_menu_txt[3][0]));
        menu_move_cursor(0, 1, 1);
        break;
    case SETTINGS:
        menu_set_xy(1, 0);
        menu_puts((int8_t*)"Settings");
        break;
    case GEN_LIBRARY: 
        menu_set_xy(1, 0);

        //  if (currGenTest.test_struct.test_num != menu_page)
        {
            pull_data_for_display((Test_lookup[menu_page]), &currGenTest);
        }                                                  

        menu_puts((int8_t*)currGenTest.test_struct.name);
                  
        if (!(subGrp_Index < currGenTest.test_struct.nuSubGrps))
        {
            menu_sel_page--;
            subGrp_Index = menu_sel_page * 3;
        }                   
        temp = (menu_sel_page * 3);
        if( temp < currGenTest.test_struct.nuSubGrps)  
        {                                  
            pull_data_for_display(subgrp_lookup[currGenTest.test_struct.subGrpList[temp]-1], &menu_subgroup[0]);
            menu_set_xy(1, 1);  
            menu_puts((int8_t*)menu_subgroup[0].lib_struct.name);  
        }                                
        temp = (menu_sel_page * 3) + 1;    
        if( temp < currGenTest.test_struct.nuSubGrps)   
        {
            pull_data_for_display(subgrp_lookup[currGenTest.test_struct.subGrpList[temp]-1], &menu_subgroup[1]);
            menu_set_xy(1, 2);                 
            menu_puts((int8_t*)menu_subgroup[1].lib_struct.name); 
        }                                                        
        temp = (menu_sel_page * 3) + 2;   
        if( temp < currGenTest.test_struct.nuSubGrps)   
        {
            pull_data_for_display(subgrp_lookup[currGenTest.test_struct.subGrpList[temp]-1], &menu_subgroup[2]);
            menu_set_xy(1, 3);     
            menu_puts((int8_t*)menu_subgroup[2].lib_struct.name); 
        }

        menu_move_cursor(0, 1, 1);
        break;

    case CUST_LIBRARY:
        menu_set_xy(1, 0);
        menu_puts((int8_t*)"This is cust lib");
        break;
    case NORM_OP_DISPLAY:
        menu_UpdateSystemDisplay((void*)0);
        break;
    default:
        break;
    }

}

void external_menu_update_subgroup_list( void )
{
    menu_sel_page++;
    //temp = currGenTest.test_struct.subGrpList[(menu_sel_page * 3)];
    pull_data_for_display(subgrp_lookup[(currGenTest.test_struct.subGrpList[(menu_sel_page * 3)]) - 1], &menu_subgroup[0]);
    //temp = currGenTest.test_struct.subGrpList[(menu_sel_page * 3) + 1];
    pull_data_for_display(subgrp_lookup[(currGenTest.test_struct.subGrpList[(menu_sel_page * 3) + 1]) - 1], &menu_subgroup[1]);
    //temp = currGenTest.test_struct.subGrpList[(menu_sel_page * 3) + 2];
    pull_data_for_display(subgrp_lookup[(currGenTest.test_struct.subGrpList[(menu_sel_page * 3) + 2]) - 1], &menu_subgroup[2]);
}

/**********************************************************************
Function:   menu_UpdateSystemDisplay
Inputs:     _eKEY_SELECTION key: Current key press
Outputs:    void
Description:Updates the normal status screeen
**********************************************************************/
static void menu_UpdateSystemDisplay(_eKEY_SELECTION *key)
{
    //add the updates/data to the screen
    uint16_t RemainingTime = 0;
    uint16_t FreqIndex = 0;
    uint32_t *pFrequency = 0;
    uint16_t temp = 0;  
    uint16_t temp2 = 0;  
    uint8_t systemMode = 0;
    char opMode[NUM_LINES][NUM_COLS] = { 0 };
    char *p;

    pull_current_status_for_display(&RemainingTime, &FreqIndex, &systemMode);
                                
    if (currGenTest.test_struct.name[0] == 0)
    {                         
        strncpy(&opMode[0][0], "No Group Selected    ", NUM_COLS);
        strncpy(&opMode[1][0], "No subgroup Selected ", NUM_COLS);
        sprintf(&opMode[2][0], "%6u       %6u", 0, 0);
        sprintf(&opMode[3][0], "%6u       %6u", 0, 0);
    }                                   
    else                 
    {
        strncpy(&opMode[0][0], currGenTest.test_struct.name, NUM_COLS);
        strncpy(&opMode[1][0], menu_subgroup[selected_subgrp].lib_struct.name, NUM_COLS);
                                                                    
        pFrequency = (uint32_t*)&menu_subgroup[selected_subgrp].lib_struct.freqList[FreqIndex];
        sprintf(&opMode[2][0], "D:%6u   T:%6u", menu_subgroup[selected_subgrp].lib_struct.numFrequencies/*menu_subgroup[selected_subgrp].lib_struct.dwellTime*/, RemainingTime);   
        temp = *pFrequency / 100;     
        temp2 = *pFrequency % 100;                          
        while (temp2 > 9)
        {
            temp2 /= 10;                            
        }
        if (temp2 != 0)                                                           
            sprintf(&opMode[3][0], "#:%6u   F:%4u.%1u", FreqIndex+1, temp, temp2);
        else
            sprintf(&opMode[3][0], "#:%6u   F:%6u", FreqIndex+1, temp);            
        p =  opMode;                          
                           
        for (temp = 0; temp < 80; temp++)
        {
            if (*p == 0x00)
                *p = 0x20;
            p++;                     
        }
    }
                            
    switch (systemMode)
    {
        case 0:
            opMode[0][19] = STOP_CHAR;    //stop symbol       
            break;
        case 1:                     
            opMode[0][19] = PAUSE_CHAR;    //pause symbol      
            break;
        case 2:             
            opMode[0][19] = PLAY_CHAR;    //play symbol  
            break;
    }

    menu_set_xy(0, 0);
    menu_puts((char*)(&opMode[0][0]));
    menu_set_xy(0, 1);
    menu_puts((char*)(&opMode[1][0]));
    menu_set_xy(0, 2);
    menu_puts((char*)(&opMode[2][0]));
    menu_set_xy(0, 3);                                                                                                                                                                                  
    menu_puts((char*)(&opMode[3][0]));

    if (key != (void*)0 && *key != eKEY_NONE)
    {
        curr_menu = MAIN;
        call_new_menu = 1;     //menu_newMenu
    }
}

/**********************************************************************
Function:   menu_draw_cursor
Inputs:     void
Outputs:    void
Description:Adds the cursor character to the display buffer at the
current position.
**********************************************************************/
static void menu_draw_cursor(void)
{
    menu_buffer[line][column] = '>';
    menu_has_changed = 1;
}

/**********************************************************************
Function:   menu_set_xy
Inputs:     uint8_t new_column: new column moving to
uint8_t new_line: new display line moving to
Outputs:    void
Description:Moves the cursor within the display buffer.
**********************************************************************/
static void menu_set_xy(uint8_t new_column, uint8_t new_line)
{
    if (new_column >= NUM_COLS || new_line >= NUM_LINES)
        return;

    line = new_line;
    column = new_column;
}

/**********************************************************************
Function:   menu_move_cursor
Inputs:     uint8_t new_column: new column moving to
uint8_t new_line: new display line moving to
uint8_t show: flag to determine if cursor should be shown
Outputs:    void
Description:Moves the cursor within the display buffer. Will show the
cursor character is enabled.
**********************************************************************/
static void menu_move_cursor(uint8_t new_column, uint8_t new_line, uint8_t show)
{
    if (new_column >= NUM_COLS || new_line >= NUM_LINES)
        return;

    menu_buffer[new_line][new_column] = ' ';
    line = new_line;
    column = new_column;
    if (show)
        menu_draw_cursor();
    menu_has_changed = 1;
}

/**********************************************************************
Function:   menu_putchar
Inputs:     int8_t x: character
Outputs:    void
Description:Writes a character to the display buffer
**********************************************************************/
static void menu_putchar(int8_t x)
{
    menu_buffer[line][column] = x;
    column++;
    menu_has_changed = 1;
}

/**********************************************************************
Function:   menu_puts
Inputs:     int8_t *s: Pointer to a string, null terminated.
Outputs:    void
Description:Writes a null terminated string to the screen buffer. Will
only write the max positions (NUM_COLS).
**********************************************************************/
static void menu_puts(int8_t *s)
{
    uint8_t i = 0;

    while (*s != 0 && i < NUM_COLS)
    {
        menu_putchar(*s);
        s++;
        i++;
    }
    menu_has_changed = 1;
}

/**********************************************************************
Function:   menu_clear
Inputs:     void
Outputs:    void
Description:Clears the screen (writes spaces)
**********************************************************************/
static void menu_clear(void)
{
    uint8_t y = 0;
    uint8_t x = 0;

    for (y = 0; y < NUM_LINES; y++)
    {
        for (x = 0; x < NUM_COLS; x++)
            menu_buffer[y][x] = ' ';
    }
    menu_has_changed = 1;
}
