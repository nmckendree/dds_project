/****************************************************************************
 FileName:     Serial_Buffer.h
 Dependencies:
 Processor:     
 Hardware:      
 Complier:      
 Company:       
          

 Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 

*****************************************************************************
General description:


Dependencies:


usage:

****************************************************************************
Public Functions:

****************************************************************************/
                       
#ifndef __SERIAL_BUFFER_H
#define __SERIAL_BUFFER_H


/****************************************************************************
                        Includes
****************************************************************************/

/****************************************************************************
                        GLOBAL TYPEDEFS
 *****************************************************************************/

/****************************************************************************
                        DEFINES AND ENUMARATED VALUES
*****************************************************************************/
typedef enum
{
   //DEBUG SERIAL ports
   SERIAL_PC_RX = 0,
   SERIAL_PC_TX,
   // always must be last entry
   SPORT_ENUM_MAX
} SPORT_ENUM;

/****************************************************************************
                        MACROS
*****************************************************************************/

/****************************************************************************
                        GLOBAL FUNCTION PROTOTYPES
*****************************************************************************/
void SerialInit(void);                                                                                   
uint8_t serial_Add2Buffer( SPORT_ENUM port, char *data, uint8_t byteCnt);
uint8_t serial_GetFromBuffer( SPORT_ENUM port, char *data, uint8_t byteCnt);
void serial_BuffReset( SPORT_ENUM port );
uint8_t serial_IsTXEmpty( SPORT_ENUM port );
uint8_t serial_IsTXBusy( SPORT_ENUM port );
#if defined USE_UART_TX_INTS
uint8_t serial_StartTX( SPORT_ENUM port );
#endif
/****************************************************************************
                        GLOBAL VARIABLES
******************************************************************************/

#endif    /* UART_H */

