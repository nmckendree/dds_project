/******************************************************************************************

******************************************************************************************
FileName:        Menu.h
Dependencies:    See INCLUDES section below
Processor:       PIC18

Tested With:     CCS

Description:
Header file for the Menu module

To Use:                                                            


Functions:

Author(s)                        Date            Version        Comment
******************************************************************************************
Nick Mckendree                      10/2016

******************************************************************************************/
#ifndef __MENU_H
#define __MENU_H

/*****************************************************************************************
GLOBAL INCLUDES
******************************************************************************************/
#include "eeprom.h"
/*****************************************************************************************
GLOBAL PUBLIC DEFINES
******************************************************************************************/
#define LINE_LEN 21

/*****************************************************************************************
GLOBAL DataTypes
******************************************************************************************/
typedef struct
{
    char  line1[LINE_LEN];
    char  line2[LINE_LEN];
    char  line3[LINE_LEN];
    char  line4[LINE_LEN];
} menu_scn_STRCT;

typedef enum
{
    MAIN = 0,            //main menu
    GEN_LIBRARY,
    CUST_LIBRARY,
    SETTINGS,
    GRP_MENU,
    SUBGRP_MENU,
    NORM_OP_DISPLAY,
    MENU_MAX
} MENU_SCR_NUM_ENUM;

typedef enum { eKEY_NONE, eKEY_UP, eKEY_DOWN, eKEY_RIGHT, eKEY_LEFT, eKEY_SELECT }_eKEY_SELECTION;
/*****************************************************************************************
EXTERNAL PUBLIC GLOBAL VARIABLES
*****************************************************************************************/
extern test_eeprom_t currGenTest;
extern lib_eeprom_t menu_subgroup[3];
extern uint16_t selected_subgrp;
/*****************************************************************************************
EXTERNAL PUBLIC FUNCTION DEFINITIONS
******************************************************************************************/
void menu(_eKEY_SELECTION key, uint32_t time_since_last_called_ms);
void menu_Init(void);
void external_menu_update_subgroup_list(void);
#endif                                                            
