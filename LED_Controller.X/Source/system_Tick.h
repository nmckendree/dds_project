/****************************************************************************
FileName:     SysTick.h
Dependencies:
Processor:
Hardware:
Complier:     
Company:     
Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


 *****************************************************************************
General description:





usage:

 ****************************************************************************
Public Functions:

 ****************************************************************************/
#ifndef __SYSTICK_H
#define    __SYSTICK_H

#ifdef    __cplusplus
extern "C" {
#endif

/****************************************************************************
                        Includes
 ****************************************************************************/
#include <stdint.h>

/****************************************************************************
                        GLOBAL TYPEDEFS
 *****************************************************************************/

/****************************************************************************
                        DEFINES AND ENUMARATED VALUES
 *****************************************************************************/
#define TIME_MAX                UINT16_MAX //4294967294ULL

#define SYSTICK_TICKS_PER_MS    1

#define TICKS_PER_MS            SYSTICK_TICKS_PER_MS
#define TICKS_PER_SEC           (1000 * TICKS_PER_MS)   
#define TICKS_PER_MIN           (TICKS_PER_SEC * 60)
#define TICKS_PER_HOUR          (TICKS_PER_MIN * 60)
/****************************************************************************
                        MACROS
 *****************************************************************************/

/****************************************************************************
                        GLOBAL FUNCTION PROTOTYPES
 *****************************************************************************/
extern uint16_t GetSysTick(void);
extern void initSysTick(void);
extern uint16_t timePassed(uint16_t timeOrig);
extern void SysWait(uint16_t millisec);
void systemTimerCallback(void);
/****************************************************************************
                        GLOBAL VARIABLES
 ******************************************************************************/
extern volatile uint8_t SysTick_Ticked;

#ifdef    __cplusplus
}
#endif
                          
#endif    /* SYSTICK_H */


