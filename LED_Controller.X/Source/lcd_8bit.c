/****************************************************************************  
FileName:     lcd_8bit.c                                                                                         
Dependencies: CCS libraries
Processor:    PIC18            
Hardware:
Complier: CCS     
Company:                 
                                
                      
Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        10/2016    
                                         
*****************************************************************************
General description: Display driver code            
 Based on code from ELECTRONIC ASSEMBLY JM (Display Manu.)                                                                    
                                                    
****************************************************************************
                            INCLUDES             
****************************************************************************/
#include "project.h"
#include "lcd_8bit.h"       
                 
/****************************************************************************
                            DEFINES            
****************************************************************************/   
#define Wait(x)                 __delay_ms(x) 
#define WAITST                  1

/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/                                               

/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/
const uint8_t stop[] =  { 0x00, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x00, 0x00};
const uint8_t play[] =  { 0x00, 0x08, 0x0c, 0x0e, 0x0c, 0x08, 0x00, 0x00};   
const uint8_t pause[] = { 0x00, 0x1b, 0x1b, 0x1b, 0x1b, 0x1b, 0x00, 0x00};    
const uint8_t time[] =  { 0x1f, 0x11, 0x0a, 0x04, 0x0a, 0x11, 0x1f, 0x00}; 
    
/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/                                       
    
/*********************************************************************
Function:    initDispl
Input:       void
Output:      void
Dependencies:
Description: The init function
********************************************************************/  
void initDispl(void)
{   
    LCD_ENABLE_PIN_SetHigh();
    LCD_ENABLE_PIN_SetHigh();
    LCD_ENABLE_PIN_SetHigh();
                                         
    LCD_RW_PIN_SetHigh();      //ddRW = 1;
    LCD_RS_PIN_SetHigh();      //ddRS = 1;

    LCD_DATA7_SetHigh(); 
    LCD_DATA6_SetHigh();
    LCD_DATA5_SetHigh();
    LCD_DATA4_SetHigh();
    LCD_DATA3_SetHigh();
    LCD_DATA2_SetHigh();
    LCD_DATA1_SetHigh();
    LCD_DATA0_SetHigh();
    
    //Reset of LCD-Module is recommended,
    LCD_RESET_SetLow();     
    __delay_ms( 1 );
    LCD_RESET_SetHigh();                                
    __delay_ms( 5 );
              
    //init Display  
    WriteIns(0x3A);    //8-Bit data length extension Bit RE=1; REV=0
    WriteIns(0x3A);    //8-Bit data length extension Bit RE=1; REV=0
    WriteIns(0x09);    //4 line display
    WriteIns(TOPVIEW);    //Bottom view             
    WriteIns(0x1E);    //Bias setting BS1=1
    WriteIns(0x39);    //8-Bit data length extension Bit RE=0; IS=1
    WriteIns(0x1B);    //BS0=1 -> Bias=1/6
    WriteIns(0x6E); //Devider on and set value
    WriteIns(0x57); //Booster on and set contrast (BB1=C5, DB0=C4)
    WriteIns(0x72); //Set contrast (DB3-DB0=C3-C0)
    WriteIns(0x38); //8-Bit data length extension Bit RE=0; IS=0  
    display_generate_cust_chars();              
    ClrDisplay();                  
    DisplayOnOff(DISPLAY_MSK & ~CURSOR_MSK & ~BLINK_MSK);
    SetContrast( 60 );
}               
                                                   
/*********************************************************************
Function:    display_generate_cust_chars
Input:       void
Output:      void
Dependencies:
Description: App function to write all necessary custom characters
********************************************************************/                                                     
void display_generate_cust_chars( void )      
{                                                                                                                           
   DefineCharacter( 1, (uint8_t*)stop);   
   DefineCharacter( 2, (uint8_t*)play);                                 
   DefineCharacter( 3, (uint8_t*)pause);   
   DefineCharacter( 4, (uint8_t*)time);      
}
   
/*********************************************************************
Function:    WriteChar
Input:       char character: character to display
Output:      void
Dependencies:
Description: sends a single character to display      
********************************************************************/  
void WriteChar (char character)                                 
{
    WriteData((uint8_t)character);
}

/*********************************************************************
Function:    WriteString
Input:       char * string: pointer to string array
Output:      void
Dependencies:
Description: sends a string to display, must be 0 terminated 
********************************************************************/  
void WriteString( char * string)
{
    do
    {
        WriteData((uint8_t)*string++);
    }                                       
    while(*string);
}
 
/*********************************************************************
Function:    SetPostion
Input:       uint8_t pos: position, combo of line and column
Output:      void
Dependencies:
Description: set postion   
********************************************************************/  
void SetPostion(uint8_t pos)
{
    WriteIns(LCD_HOME_L1+pos);
}
  
/*********************************************************************
Function:    DisplayOnOff
Input:       char data: control
Output:      void
Dependencies:
Description: use definitions of header file to set display   
********************************************************************/  
void DisplayOnOff(uint8_t data)
{
    WriteIns(0x08+data);
}
    
/*********************************************************************
Function:    DefineCharacter
Input:       uint8_t postion: location in GRAM to store it
             uint8_t *data: pointer to array holding char data
Output:      void
Dependencies:
Description: stores an own defined character      
             Display can store 8, addresses 0 - 7
********************************************************************/  
void DefineCharacter(uint8_t postion, uint8_t *data)
{
    uint8_t i=0;
    WriteIns(0x40+8*postion);

    for (i=0; i<8; i++)
    {
        WriteData(data[i]);
    }
    SetPostion(LINE1);
}
 
/*********************************************************************
Function:    ClrDisplay
Input:       void
Output:      void
Dependencies:
Description: Clears entire Display content and set home pos   
********************************************************************/  
void ClrDisplay(void)
{                    
    WriteIns(0x01);
    SetPostion(LINE1);
}

/*********************************************************************
Function:    SetContrast
Input:       uint8_t contr: contrast setting
Output:      void  
Dependencies:
Description: Sets contrast 0..63  
********************************************************************/  
void SetContrast(uint8_t contr)
{
    WriteIns(0x39);
    WriteIns(0x54 | (contr >> 4));
    WriteIns(0x70 | (contr & 0x0F));
    WriteIns(0x38);     
}      

/*********************************************************************
Function:    SetView
Input:       uint8_t view: viewing angle, see header
Output:      void
Dependencies:
Description: Rotates the display 
            view bottom view(0x06), top view (0x05)   
********************************************************************/  
void SetView(uint8_t view)
{
    WriteIns(0x3A);
    WriteIns(view);
    WriteIns(0x38);
}

/*********************************************************************
Function:    SetROM
Input:       uint8_t rom_data: Which rom page to use
Output:      void
Dependencies:                                             
Description: Changes the Rom code, aka character maps
            (ROMA=0x00, ROMB=0x04, ROMC=0x0C)  
********************************************************************/  
void SetROM (uint8_t rom_data)
{
    WriteIns(0x3A);
    WriteIns(0x72);        
    WriteData(rom_data);
    WriteIns(0x38);
}

/*********************************************************************
Function:    WriteIns
Input:       uint8_t ins: instruction byte to send
Output:      void
Dependencies:
Description: sends instruction to display  
********************************************************************/  
static void WriteIns(uint8_t ins)
{               
    CheckBusy();
    LCD_RS_PIN_SetLow();       //RS = 0;
    LCD_RW_PIN_SetLow();       //RW = 0;      
       
    LCD_DATA0_LAT = (ins & 0x01);
    ins >>= 1;  
    LCD_DATA1_LAT = (ins & 0x01);
    ins >>= 1;  
    LCD_DATA2_LAT = (ins & 0x01);
    ins >>= 1;  
    LCD_DATA3_LAT = (ins & 0x01);
    ins >>= 1;  
    LCD_DATA4_LAT = (ins & 0x01);
    ins >>= 1;  
    LCD_DATA5_LAT = (ins & 0x01);
    ins >>= 1;  
    LCD_DATA6_LAT = (ins & 0x01);
    ins >>= 1;  
    LCD_DATA7_LAT = (ins & 0x01);

    LCD_ENABLE_PIN_SetHigh();       //EN = 1; 
    Wait(WAITST);                        
    LCD_ENABLE_PIN_SetLow();       //EN = 0;       
}          

/*********************************************************************
Function:    WriteData
Input:       uint8_t data: value to send
Output:      void
Dependencies:
Description: sends data to display     
********************************************************************/  
static void WriteData(uint8_t data)
{
    CheckBusy();                      
    LCD_RS_PIN_SetHigh();       //RS = 0;
    LCD_RW_PIN_SetLow();
    //DATA = ins;                                   
    LCD_DATA0_LAT = (data & 0x01);
    data >>= 1;  
    LCD_DATA1_LAT = (data & 0x01);
    data >>= 1;  
    LCD_DATA2_LAT = (data & 0x01);
    data >>= 1;  
    LCD_DATA3_LAT = (data & 0x01);
    data >>= 1;  
    LCD_DATA4_LAT = (data & 0x01);
    data >>= 1;  
    LCD_DATA5_LAT = (data & 0x01);
    data >>= 1;  
    LCD_DATA6_LAT = (data & 0x01);
    data >>= 1;  
    LCD_DATA7_LAT = (data & 0x01);
                                        
    LCD_ENABLE_PIN_SetHigh();       //EN = 1; 
    Wait(WAITST);
    LCD_ENABLE_PIN_SetLow();       //EN = 0;        
}
  
/*********************************************************************
Function:    CheckBusy
Input:       void
Output:      uint8_t: 1 if ready/idle
Dependencies:
Description: checks if display is idle 
********************************************************************/  
uint8_t CheckBusy(void)
{
#if 0       //enable for read capability
    uint8_t readData = 1;  
    prc2 = 1;
    ddDATA = 0x00; // Data is input
    RS = 0;
    RW = 1;
    do
    {
        EN = 1;
        Wait(WAITST);
        EN = 0;
        readData = DATA; //read data directly falling edge
            Wait(WAITST);
    }while(readData&0x80); //check for busyflag
                       
    prc2 = 1;  //remove protection of port P0 to change port direction
    ddDATA = 0xFF;
                
    return readData;
#else
    __delay_us( 50 );
                
    return 1;    
#endif

}





