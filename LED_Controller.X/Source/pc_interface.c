/****************************************************************************  
FileName:     pc_interface.c
Dependencies:
Processor:
Hardware:
Complier: CCS        
Company:    


Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        10/2016
                                    
*****************************************************************************
General description:  Application level processing of incoming data from
                    the PC / serial interface.

                            
****************************************************************************
                            INCLUDES
****************************************************************************/
#include "project.h"  
#include <stdint.h>   
#include "pc_interface.h"
#include "serial_buffer.h"     
#include "serial_data_link_layer.h"

/****************************************************************************
                            DEFINES            
****************************************************************************/  

#define MSG_CRC_OVERHEAD			2
#define MSG_OVERHEAD				( 3 + MSG_CRC_OVERHEAD )
#define PAYLOAD_LEN					(128 + MSG_CRC_OVERHEAD)
#define MAX_MSG_LEN					( 128 + MSG_OVERHEAD)      //payload + overhead



/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/    
typedef enum{ 
    COMM_SET_MODE,
    COMM_GET_MODE,
    COMM_GET_STATUS,
    COMM_SHOW_STATUS,
    COMM_DL_TEST,
    COMM_ACK_TEST_DL,
    COMM_DL_GRP,
    COMM_ACK_GRP_DL,        
    COMM_GET_NUM_TESTS,
    COMM_SHOW_NUM_TESTS,
    COMM_GET_NUM_GRPS,            
    COMM_SHOW_NUM_GRPS,
    COMM_MAX_NUM_COMMANDS            
}APP_LAYER_COMM_CMDS_t;

typedef union {
	uint8_t buffer[MAX_MSG_LEN];
	__pack struct {
		uint8_t comm_id;
		uint16_t data_len;
		uint8_t payload[PAYLOAD_LEN];
	};
}pc_app_layer_data_t;
                          
/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/
static uint16_t buffer_index = 0;   
uint8_t serialMessageRdy = 0;  
pc_app_layer_data_t appData = {0};

/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/ 

/*********************************************************************
Function:     pc_cmd_handler
Input:        void
Return:       void
Description:Callback function that is installed into the UxRX ISR. Parses
            the commands from terminal in real time.
Visibility: Public
Author:        NAM
Notes:
**********************************************************************/
void pc_cmd_handler(void)       
{                                
    uint8_t c = 0;   
    uint8_t r_code = 0;
    
    if(serial_GetFromBuffer(SERIAL_PC_RX,&c, 1) > 0)
    {
        r_code = decode_byte(&c, &appData.buffer[buffer_index]);
        
        switch (r_code)
        {
            case BEGINNING_OF_NEW_MSG:   
                buffer_index = 0;
                serialMessageRdy = 0;
                break;
            case END_OF_NEW_MSG:
                reset_link_layer();
                buffer_index = 0;
                serialMessageRdy = 1;
                break;
            case 0:
                break;
            case 1:
                buffer_index++;
                break;
            default:
                break;
        }
    }
} 

/*********************************************************************
Function:     cmd_parse
Input:        void
Return:       int8_t: return code, -1 = error
Description: function to parse the available command from the terminal interface
Visibility: Public
Author:        NAM
Notes:
**********************************************************************/
_tPC_MSG_RETURN_CODES pc_cmd_parse(void)    
{
    _tPC_MSG_RETURN_CODES returnCode = PC_MSG_NULL;
    
    if( serialMessageRdy == 0)
        return PC_MSG_NULL;
    
    serialMessageRdy = 0;
    
    switch( appData.comm_id)
    {
        case COMM_SET_MODE:        
            break;
        case COMM_GET_MODE:        
            break;
        case COMM_GET_STATUS:        
            break;
        case COMM_SHOW_STATUS:        
            break;
        case COMM_DL_TEST:        
            break;
        case COMM_ACK_TEST_DL:        
            break;
        case COMM_DL_GRP:        
            break;
        case COMM_ACK_GRP_DL:                
            break;
        case COMM_GET_NUM_TESTS:        
            break;
        case COMM_SHOW_NUM_TESTS:        
            break;
        case COMM_GET_NUM_GRPS:                    
            break;
        case COMM_SHOW_NUM_GRPS:        
            break;
        default:
            return PC_MSG_ERROR;
            break;
    }
    return returnCode;
}

