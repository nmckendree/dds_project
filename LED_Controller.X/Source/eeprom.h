/******************************************************************************************
 
******************************************************************************************
FileName:        eeprom.h
Dependencies:    See INCLUDES section below
Processor:        

Tested With:      
 
Description: 


Author(s)                        Date            Version        Comment
******************************************************************************************
 
******************************************************************************************/

/*****************************************************************************************
GLOBAL INCLUDES
******************************************************************************************/
#ifndef __EEPROM_H
#define __EEPROM_H

#include <stdint.h>
/*****************************************************************************************
GLOBAL PUBLIC DEFINES
******************************************************************************************/
#define HDR_LEN                15
#define HDR_PAGE            0
/*****************************************************************************************
GLOBAL DataTypes
******************************************************************************************/
typedef struct {                   
    uint8_t SlaveAddr;
    uint32_t Max_Data_Len;
    uint32_t PageSz;
    uint32_t DataSz;
    uint8_t Data_Valid;
}_tEepromDevice /*__attribute__((packed))*/;

typedef struct {                  //Frequencies stored as 3 bytes. This is my 3 byte datatype                                             
    uint8_t freq[3];
}freq_t /*__attribute__((packed))*/;
   
/******************************
These structures represent how the data is stored to eeprom. I am using unions in order
to retrieve/access as bytes but be able to logically work with them in a readable way (structs)
Hopefully there will be no issue with CCS. 
I used the packed attribute on these structs so the compiler does not pad them and
add extra bytes which will mess up reading/writing to/from the eeprom
*******************************/    
typedef union {
    uint8_t bytes[128];
    struct {          
        uint8_t overflow;
        uint16_t test_id;
        char name[20];           
        uint16_t dwellTime;
        uint16_t numFrequencies;
        freq_t freqList[33];
        uint8_t freeByte0;
        uint8_t freeByte1;
    }lib_struct /*__attribute__((packed))*/;
    struct {   
        uint8_t overflow;
        uint16_t test_id;
        freq_t freqList[41];
    }lib_struct_overflow /*__attribute__((packed))*/;
}lib_eeprom_t;

typedef union {
    uint8_t bytes[128];
    struct {
        uint8_t overflow;
        uint16_t test_num;
        char name[20];
        uint16_t nuSubGrps;
        uint16_t subGrpList[51];
        uint8_t freeByte0;
    }test_struct /*__attribute__((packed))*/;
    struct {
        uint8_t overflow;
        uint16_t test_num;
        uint16_t subGrpList[62];
    }test_struct_overflow /*__attribute__((packed))*/;

}test_eeprom_t;

typedef union {
    uint8_t bytes[HDR_LEN];
    struct {
        uint8_t reserved;
        uint16_t headerChksm;
        uint16_t dataChksm;
        uint8_t eeprom_ver;
        uint8_t eeprom_valid;
        uint8_t eeprom_type;
        uint16_t num_used_pages;
        uint16_t num_data_bytes;
    }hdr_struct /*__attribute__((packed))*/;
}hdr_eeprom_t;


/*****************************************************************************************
EXTERNAL PUBLIC FUNCTION DEFINITIONS
******************************************************************************************/
extern uint8_t ReadExtEEPROM(_tEepromDevice _eeprom, uint32_t MemAddr, uint8_t *pData, uint16_t NumBytes);
extern uint8_t WriteExtEEPROM(_tEepromDevice _eeprom, uint32_t MemAddr, uint8_t *pData, uint16_t NumBytes);
extern uint8_t EraseEEPROM(_tEepromDevice *_eeprom);
extern void InitEEPROM(_tEepromDevice *_eeprom, uint8_t SlaveAddr, uint16_t Max_Data_Len);
extern uint8_t ChecksumEEPROM(_tEepromDevice _eeprom, uint32_t StartAddr, uint16_t NumBytes, uint8_t *success);
extern uint8_t ChecksumPKT(uint8_t *pData, uint16_t NumBytes);
extern uint16_t Stored_Length(_tEepromDevice _eeprom, uint16_t *stored_len);
extern uint8_t Data_is_Valid(_tEepromDevice* _eeprom);
extern uint8_t Update_Checksum(_tEepromDevice* _eeprom);
extern void scan_eeprom(_tEepromDevice *_eeprom);
// for debug
//uint8_t Read_Whole_EEPROM(_tEepromDevice eeprom, uint8_t *pData, uint16_t NumBytes);


/*****************************************************************************************
EXTERNAL PUBLIC VARIABLE ACCESS
******************************************************************************************/
extern uint16_t subgrp_lookup[250];
extern uint16_t Test_lookup[250];
extern uint16_t found_subgrps;
extern uint16_t found_tests;

#endif

/*****************************************************************************************
NOTES                                  
*****************************************************************************************/

