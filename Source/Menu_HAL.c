/******************************************************************************************
FileName:        menu_hal.c
Dependencies:    See INCLUDES section below
Processor:       PIC18

Tested With:     CCS

Description:
This module contains functions that interfaces the menu system to a lower
level display driver. 

Author(s)                       Date            Version     Comment
******************************************************************************************
Nick Mckendree                  10/2016
******************************************************************************************/
                                       
/*****************************************************************************************
PRIVATE INCLUDE FILES
******************************************************************************************/
#include <stdint.h>
#include "Menu_HAL.h"  
#include "lcd_8bit.h"   
                
/*****************************************************************************************
PRIVATE GLOBAL DEFINES
******************************************************************************************/
          
/*****************************************************************************************
PRIVATE GLOBAL DataTypes
******************************************************************************************/

/*****************************************************************************************
PRIVATE GLOBAL VARIABLES
******************************************************************************************/

/*****************************************************************************************
PRIVATE FUNCTION PROTOTYPES
******************************************************************************************/
void menu_hal_set_xy(uint8_t x, uint8_t y);  
void menu_HAL_write_display_buffer(uint8_t* buffer, uint8_t len);                         

                  
/**********************************************************************
Function:   menu_HAL_write_display_buffer
Inputs:     uint8_t* buffer: pointer to the array holding the data to write                          
            uint8_t len: Length of the buffer to write
Outputs:    void
Description:Writes the menu buffer to the display, one byte at a time, one
               row at a time. 
**********************************************************************/                    
void menu_HAL_write_display_buffer(uint8_t* buffer, uint8_t len)    
{                
    int i = 0;
    int row = 0;
    int col = 0;
     
    menu_hal_set_xy(0, 0);            

    for (row = 0; row < NUM_LINES; row++)
    {
        menu_hal_set_xy(0, row);
        for (col = 0; col < NUM_COLS; col++)
        {
            WriteChar((char)buffer[i]);
            i++;
        }                      
    }                     
}

/**********************************************************************
Function:   menu_hal_set_xy
Inputs:     uint8_t x: col coordinate                      
            uint8_t y: row coordinate
Outputs:    void         
Description:Wrapper function to set the position at a X,Y coordinate 
            on the display
**********************************************************************/          
void menu_hal_set_xy(uint8_t x, uint8_t y)
{                                        
    uint8_t pos = 0;
    switch( y )
    {
        case 0:   
            pos = x;
            break;                               
        case 1:
            pos = x + 0x20;
            break;                        
        case 2:
            pos = x + 0x40;
            break;       
        case 3:
            pos = x + 0x60;
            break;    
    }
    SetPostion(pos);   
}

