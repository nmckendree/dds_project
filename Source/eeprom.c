/******************************************************************************************
 
******************************************************************************************
FileName:        eeprom.c
Dependencies:    See INCLUDES section below
Processor:       

Tested With:     
MPLAB IDE  
Description:
Description:
This is a low level I2C EEPROM interface for the selected 2K bit devices by Microchip.
These functions will handle page write boundaries.
Device: Microchip 24aa025e48/24aa02e48/24aa025
Dependencies:     Device with I2C communication port
 

To USE:

Create a local variable of the type _tEepromDevice such as _tEepromDevice ConfigEEPROM
This will be the configuration structure of the EEPROM since we are supporting 2 types

Call InitEEPROM to set the structure to 0 and if the autoconfig function is defined, the
EUI field and the max data size fields will be filled in.


NOTES!

Author(s)                        Date            Version        Comment
******************************************************************************************
Nick Mckendree                      20121120                1.0
*****************************************************************************************/
/*****************************************************************************************
PRIVATE INCLUDE FILES
******************************************************************************************/
#include <stdint.h>
#include "eeprom.h"                
#include "hal_eeprom.h"
                  

/*****************************************************************************************
PRIVATE GLOBAL DEFINES
******************************************************************************************/
#define MAX_PAGE_SIZE                128    //BYTES
#define EEPROM_ERASED_STATE            0xFF
#define MAX_BYTES_WR                128
#define    MAX_POLL_ATTEMPTS            355     
#define CHECKSUM_ADDR                4

/*
The 24AA1025 memory chip will not acknoledge a write command
while the device is undergoing an internal page write. Use
this feature to poll when the device is ready */
#define _mPollEepromRdy(Address)    hal_i2c_send_pkt(Address, 0, (void*)0, 0)
/*****************************************************************************************
PRIVATE GLOBAL DATATYPES
******************************************************************************************/

/*****************************************************************************************
PRIVATE GLOBAL VARIABLES  
******************************************************************************************/
uint16_t subgrp_lookup[250];                //Lookup table for subgroup test
uint16_t Test_lookup[250];                  //lookup table for group test                               
uint16_t found_subgrps = 0;
uint16_t found_tests = 0;               
/*****************************************************************************************
PRIVATE FUNCTION PROTOTYPES
******************************************************************************************/
 

/*****************************************************************************************
FUNCTION LIST
******************************************************************************************/
                         
// -- BEGIN CODE -- 
/*********************************************************************
Function:     void InitEEPROM(_tEepromDevice* eeprom, uint8_t SlaveAddr, uint8_t Max_Data_Len, uint8_t i2c_mod_no)
Input:        _tEepromDevice* eeprom: pointer to configuration register for chip
uint8_t SlaveAddr: Address of eepromto configure
uint8_t Max_Data_Len: maximum data space to be used (excluding reserved space)
Return:       void
Description: This funciton clears the associated structure and then
populates it based on passed in address and then calls
the autodetectmemorytype function if defined.
Visibility: Public              
Author:        NAM
Notes:
Dependencies: I2C has to be initialized/opened
*********************************************************************/
void InitEEPROM(_tEepromDevice* eeprom, uint8_t SlaveAddr, uint32_t Max_Data_Len)
{
    eeprom->PageSz = 128;
    eeprom->SlaveAddr = SlaveAddr;
    eeprom->Max_Data_Len = Max_Data_Len;
    eeprom->DataSz =128;

    Data_is_Valid(eeprom);
}


/*********************************************************************
Function:     uint8_t Data_is_Valid(_tEepromDevice* eeprom)
Input:        _tEepromDevice* eeprom: pointer to configuration register for chip
Return:       uint8_t success - using uint8_t now, returning byte for future expansion
Description: This funciton tests the content of the eeprom for valid data
two techniques used:  checksum, & value of saved size.
Visibility: Public
Author:         
Notes:
Dependencies: eeprom has to be initialized
*********************************************************************/
uint8_t Data_is_Valid(_tEepromDevice* eeprom)
{
    //uint8_t data[2];                        // for reading the stored checksum and length
    //uint8_t checksum = 0;
    uint8_t success = 1;
    hdr_eeprom_t header;

    success = hal_i2c_get_pkt(eeprom->SlaveAddr, CHECKSUM_ADDR, header.bytes, HDR_LEN);    // 1 addr byte, 2 data bytes to retrieve

    //if (success )
    //{
        //checksum = ChecksumEEPROM(*eeprom, 0, data[1], &success);
    //}

///    if (data[0] == checksum && success && data[1] <= (eeprom->DataSz ))
        eeprom->Data_Valid = 1;
//    else
//        eeprom->Data_Valid = 0;

    return((uint8_t)eeprom->Data_Valid);
}



/*********************************************************************
Function:     uint8_t Update_Checksum(_tEepromDevice* eeprom)
Input:        _tEepromDevice* eeprom: pointer to configuration register for chip
Return:       uint8_t success - using uint8_t now, returning byte for future expansion
Description: This funciton updates the content of the eeprom with checksum & size
Should be called after an eeprom write.
Visibility: Public
Author:         
Notes:
Dependencies: eeprom has to be initialized
*********************************************************************/
uint8_t Update_Checksum(_tEepromDevice* eeprom)
{
#if 0
    uint8_t data_out[3];                        // for reading the stored checksum and length
    uint8_t success = 1;
  
    uint16_t PollAttempts;
    uint8_t num_tries;

 
    if (success)
    {
        // using address of 0 because the read ext eeprom function adds the reserved size to it
        data_out[0] = ChecksumEEPROM(*eeprom, 0, eeprom->Max_Data_Len, &success);
        data_out[1] = eeprom->Max_Data_Len;

        if (success)
        {
            num_tries = 3;
            do    // enters first, tests after
            {
                //Poll until EEPROM responds, ready for next write
                PollAttempts = MAX_POLL_ATTEMPTS; //have to reset poll attempts since we are in a loop!!!
                while (!_mPollEepromRdy(eeprom->SlaveAddr) && PollAttempts--);

                success = hal_i2c_send_pkt(eeprom->SlaveAddr, CHECKSUM_ADDR, data_out, 3);
            } while (!success && num_tries--);

            //Poll until EEPROM responds, ready for next read or write
            PollAttempts = MAX_POLL_ATTEMPTS; //have to reset poll attempts since we are in a loop!!!
            while (!_mPollEepromRdy(eeprom->SlaveAddr) && PollAttempts--);
        }
    }

    if (success)
        eeprom->Data_Valid = 1;
    else
        eeprom->Data_Valid = 0;

 

    return((uint8_t)success);
#endif
    return 1;
}



/*********************************************************************
Function:     uint8_t ReadExtEEPROM(_tEepromDevice eeprom, uint16_t MemAddr, uint8_t *pData, uint16_t NumBytes)
Input:        _tEepromDevice eeprom: eeprom struct containing device specific details
uint8_t MemAddr: Memory address, current devices are 8 bit addressing, left 16 bit for future
uint8_t *pData: buffer of data
uint16_t NumBytes: Number of bytes to write
Return:       uint8_t: success value i2c comms, made it a byte to return error codes in future
Description: Retrieves a block of data from the EEPROMS. Reading does not need
to be checked for page boundaries!
Visibility: Public
Author:        NAM
Notes:         
Dependencies: I2C has to be initialized/opened
*********************************************************************/
uint8_t ReadExtEEPROM(_tEepromDevice eeprom, uint32_t MemAddr, uint8_t* pData, uint32_t NumBytes)
{
    uint8_t SuccessFlag = 1;
    //uint32_t Length = 0;

    //if (eeprom.DataSz != 0)
    //    Length = eeprom.DataSz;    //datasz stored as bytes

    //if (NumBytes + MemAddr > Length)
    //    NumBytes = Length - MemAddr;

    SuccessFlag = hal_i2c_get_pkt(eeprom.SlaveAddr, MemAddr, pData, NumBytes);

    return (uint8_t)SuccessFlag;
}
                                
/*********************************************************************
Function:     uint8_t WriteExtEEPROM(_tEepromDevice eeprom, uint16_t MemAddr, uint8_t *pData, uint16_t NumBytes)
Input:        _tEepromDevice eeprom: eeprom struct containing device specific details
uint8_t MemAddr: Memory address, current devices are 8 bit addressing, left 16 bit for future
uint8_t *pData: buffer of data
uint16_t NumBytes: Number of bytes to write
Return:       uint8_t: success value i2c comms, made it a byte to return error codes in future
Description:Formats the packet with address byte and writes data to the eeprom. This function
will also check for page bounadaries and perform multiple writes until all data
is written
Visibility: public
Author:        NAM
Notes:        Writes depend on page boundaries, pass this in!!!
Have to copy data to local array each write due to memory address
having to be in write buffer
Dependencies: none
*********************************************************************/
uint8_t WriteExtEEPROM(_tEepromDevice eeprom, uint32_t MemAddr, uint8_t *pData, uint32_t NumBytes)
{                                                                                            
    uint8_t success = 1;
    uint32_t index = 0;        //index/loop counter
    uint32_t Length = 0;
    uint16_t PollAttempts = 0;
    uint8_t num_tries;
           
    //if (eeprom.DataSz != 0)
    //    Length = eeprom.DataSz;    //datasz stored as bytes

    if( NumBytes > eeprom.PageSz) 
        NumBytes = eeprom.PageSz;                             
           
    //if (success)                     
    {
        Length = NumBytes;
        //    if (NumBytes + MemAddr > Length)
    //        NumBytes = Length - MemAddr;

        while (index < NumBytes && success)
        {
            num_tries = 3;
            do    // enters first, tests after
            {
                //Poll until EEPROM responds, ready for next write
                PollAttempts = MAX_POLL_ATTEMPTS; //have to reset poll attempts since we are in a loop!!!
                while (!_mPollEepromRdy(eeprom.SlaveAddr) && PollAttempts--);

                success = hal_i2c_send_pkt(eeprom.SlaveAddr, MemAddr, pData, Length );
            } while (!success && num_tries--);
            index += Length;
            MemAddr += Length;
            if ((NumBytes - index) >= eeprom.PageSz)
                Length = eeprom.PageSz;
            else
                Length = NumBytes - index;
        }

        //Poll until EEPROM responds, ready for next read or write
        PollAttempts = MAX_POLL_ATTEMPTS; //have to reset poll attempts since we are in a loop!!!
        while (!_mPollEepromRdy(eeprom.SlaveAddr) && PollAttempts--);
    }
 
    return (uint8_t)success;
}

/*********************************************************************
Function:     uint8_t EraseEEPROM(_tEepromDevice *eeprom)
Input:        _tEepromDevice eeprom: eeprom struct containing device specific details
Return:       uint8_t: success value i2c comms, made it a byte to return error codes in future
Description:Writes 0xFF (ERASED) to each memory location in the device.
Visibility: public
Author:        NAM
Notes:        Writes depend on page boundaries, pass this in!!!
Dependencies: I2C has to be initialized/opened
*********************************************************************/
uint8_t EraseEEPROM(_tEepromDevice *eeprom)
{
    uint8_t Data[128];
    uint8_t SuccessFlag = 1;
    uint32_t i = 0;        
    //uint32_t Length = 0;
    //uint8_t start_addr = 0; 
    uint16_t PollAttempts;      
           
   while (i < 128)
    {
        Data[i] = EEPROM_ERASED_STATE;  
        i++;
    } 
           
  //  start_addr = 0;
               
  //  if (eeprom->DataSz != 0)
  //      Length = eeprom->DataSz;    //datasz stored as bytes
  //  else
   //     Length = 128;    
            
    i = 0;
    while (i < eeprom->Max_Data_Len)               
    {                               
        SuccessFlag |= WriteExtEEPROM(*eeprom, i, Data, 128);  
        i += 128;
    } 

//        Data[0] = CHECKSUM_ADDR;
//        // using address of 0 because the read ext eeprom function adds the reserved size to it
//        Data[1] = EEPROM_ERASED_STATE;
//        Data[2] = EEPROM_ERASED_STATE;

//        SuccessFlag = hal_i2c_send_pkt(eeprom->SlaveAddr, Data, 3);
    //Poll until EEPROM responds, ready for next write
    PollAttempts = MAX_POLL_ATTEMPTS; //have to reset poll attempts since we are in a loop!!!
    if (SuccessFlag)
        while (!_mPollEepromRdy(eeprom->SlaveAddr) && PollAttempts--);

    eeprom->Data_Valid = 0;

    return (uint8_t)SuccessFlag;
}
 
 
/*********************************************************************
Function:     ChecksumEEPROM(_tEepromDevice eeprom, uint8_t StartAddr, uint16_t NumBytes, uint8_t *success)
Input:        _tEepromDevice eeprom: config settings
uint16_t StartAddr: Start adress to read
uint16_t NumBytes: Number of bytes to read
Return:       uint8_t: uint8_t length checksum value
Description:Calculates a basic sum checksum (8-bit) on a block of
data passed in.
Visibility: public
Author:        NAM
*********************************************************************/
uint8_t ChecksumEEPROM(_tEepromDevice eeprom, uint32_t StartAddr, uint32_t NumBytes, uint8_t *success)
{
#if 0
    uint8_t Checksum = 0;
    uint8_t Data[CHIP_STORAGE_BYTES];
    uint32_t index = 0;
 

    *success = 1;

    if ((NumBytes + StartAddr) > eeprom.DataSz)
        NumBytes = (eeprom.DataSz) - StartAddr;

 

    if (*success)
    {
        if (ReadExtEEPROM(eeprom, StartAddr, Data, NumBytes))
        {
            while (index < NumBytes)
            {
                Checksum += Data[index++];
            }
            Checksum = ~Checksum;
            Checksum += 1;
        }
        else
            *success = 0;
    }

 

    return Checksum;
#endif
    return 0;
}


/*********************************************************************
Function:     uint8_t ChecksumPKT(uint8_t *pData, uint16_t NumBytes)
Input:        uint8_t pData: pointer to data to calculate checksum on
uint16_t NumBytes: Number of bytes to calculate
Return:       uint8_t: uint8_t Checksum: checksum value
Description:Calculates a basic sum checksum (8-bit) on a block of
data passed in.
Visibility: public
Author:        NAM
*********************************************************************/
uint8_t ChecksumPKT(uint8_t *pData, uint32_t NumBytes)
{
    uint8_t Checksum = 0;
    uint32_t index = 0;

    while (index < NumBytes)
    {
        Checksum += pData[index++];
    }
    Checksum = ~Checksum;
    Checksum += 1;

    return Checksum;
}


/*********************************************************************
Function:     uint8_t Stored_Length(_tEepromDevice eeprom, uint8_t *stored_len)
Input:        _tEepromDevice eeprom: eeprom struct containing device specific details
uint8_t *stored_len: pointer to array of size 1, to retrieve stored length
Return:       uint8_t: success value i2c comms, made it a byte to return error codes in future
Description: This funciton returns the stored max data length value from the eeprom
Visibility: Public
Author:         
Notes:        useful for software updates that may increase used eeprom space
Dependencies: none
*********************************************************************/
uint32_t Stored_Length(_tEepromDevice eeprom, uint32_t *stored_len)
{

    return (uint8_t)1;
}
#define MAX_BYTES           (500 * 128)         
void scan_eeprom(_tEepromDevice *eeprom)
{
    uint16_t i = 4 * 128;  
    uint8_t data[3];        
    uint16_t prevID = 0;            
    uint16_t newID = 0; 
    uint16_t page_cntr = 4;
    i = page_cntr * 128;
                  
    //for (i = 4; i < 500; i++)
    while( i < MAX_BYTES )
    {                                  
        ReadExtEEPROM(*eeprom, i, data, 3);        
        //if (data[0] == 0)                
        {                            
            newID = data[2];
            newID = newID << 8;
            newID |= data[1];   
            
            if (prevID != newID && newID != 65535)
            {                               
                prevID = newID;
                                                
                if (page_cntr < 252)
                {
                    subgrp_lookup[found_subgrps] = page_cntr;
                    found_subgrps++;
                }
                else                            
                {                               
                    Test_lookup[found_tests] = page_cntr;
                    found_tests++;
                }
            }
        } 
        i = i + (256);   
        page_cntr += 2;
            
    }
    i = 0;  
    //printf("Found test: %u Found SG: %u\r\n", found_tests, found_subgrps);
}
