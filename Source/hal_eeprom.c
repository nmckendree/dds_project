/****************************************************************************  
FileName:     hal_eeprom.c
Dependencies: CCS libraries
Processor:    PIC18            
Hardware:
Complier: CCS 
Company:    


Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        10/2016    
                                    
*****************************************************************************
General description: "Low level functions" for the eeprom to interface with the I2C  
                                                             
                                                    
****************************************************************************
                            INCLUDES
****************************************************************************/       
#include <stdint.h>
#include "hal_eeprom.h"                      

/****************************************************************************
                            DEFINES            
****************************************************************************/                       

/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/   

/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/

/****************************************************************************
                            FUNCTION PROTOTYPES                                                       
****************************************************************************/     
uint8_t hal_i2c_send_pkt(uint8_t rcvr, uint16_t mem_addr,uint8_t* packet, uint32_t numBytes);
uint8_t hal_i2c_get_pkt(uint8_t i2cAddr, uint16_t mem_addr, uint8_t* packet, uint32_t  numBytes);
                                                                                                 
            
/*********************************************************************
Function:   hal_i2c_send_pkt   
Input:      uint8_t i2cAddr: device address
            uint32_t mem_addr: address of memory location             
            uint8_t* packet: pointer to array of data to wrie 
            uint32_t  numBytes: number of bytes to write                              
Return:     uint8_t: flag if ack(s) were received.        
Description:  wrapper function for the CCS i2c write functions 
Visibility: public      
Author:        NAM
*********************************************************************/                            
uint8_t hal_i2c_send_pkt(uint8_t rcvr, uint16_t mem_addr,uint8_t* packet, uint32_t numBytes)     
{                                          
    uint16_t i = 0;     
    uint8_t ack = 0;     
                               
    i2c_start();    // Start condition                           
    //ack |= i2c_write((rcvr|(BYTE)(mem_addr>>7))&0xfe);// Device address                        
    ack |= (i2c_write((rcvr << 1) & 0xfe) ^ 1);// Device address               
    ack |= (i2c_write((uint8_t)(mem_addr >> 8)) ^ 1);     
    ack |= (i2c_write((uint8_t)mem_addr) ^ 1);     
    if( ack)                                
    {
        for (i = 0; i < numBytes; i++)
            ack |= (i2c_write(packet[i]) ^ 1);    
    }  
    i2c_stop();     // Stop condition                   
    return ack;
}                

/*********************************************************************
Function:   hal_i2c_get_pkt   
Input:      uint8_t i2cAddr: device address
            uint32_t mem_addr: address of memory location             
            uint8_t* packet: pointer to array of data to read into                             
            uint32_t  numBytes: number of bytes to read                              
Return:     uint8_t: flag if ack(s) were received.        
Description:  wrapper function for the CCS i2c read functions 
Visibility: public      
Author:        NAM
*********************************************************************/      
uint8_t hal_i2c_get_pkt(uint8_t i2cAddr, uint16_t mem_addr, uint8_t* packet, uint32_t  numBytes)  
{
    uint16_t i = 0;
    uint8_t ack = 0;     
                 
   i2c_start();                  // Start condition    
   //ack |= i2c_write((i2cAddr|(BYTE)(mem_addr>>7))&0xfe);
   ack |= (i2c_write((i2cAddr << 1) & 0xfe) ^ 1);// Device address   
   ack |= (i2c_write((uint8_t)(mem_addr >> 8)) ^ 1);     
   ack |= (i2c_write((uint8_t)mem_addr) ^ 1);     
   if( ack)              
   {
       i2c_start();                   
       //ack |= i2c_write((i2cAddr|(BYTE)(mem_addr>>7))|1);  
       ack |= (i2c_write((i2cAddr << 1) | 0x01) ^ 1);// Device address   
       
       if( ack )                    
       {
           for (i = 0; i < numBytes; i++)
           {
            if( i == (numBytes - 1) )
                packet[i] = i2c_read(0);
            else    
                packet[i] = i2c_read(1);    
           }
       }
   }
   i2c_stop();
   
   return ack;
}                      

