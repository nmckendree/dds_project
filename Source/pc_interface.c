/****************************************************************************  
FileName:     pc_interface.c
Dependencies:
Processor:
Hardware:
Complier: CCS        
Company:    


Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        10/2016
                                    
*****************************************************************************
General description:  Application level processing of incoming data from
                    the PC / serial interface.

                            
****************************************************************************
                            INCLUDES
****************************************************************************/
#include <main.h>  
#include <stdint.h>    
#include "pc_interface.h"
#include "serial_buffer.h"        
/****************************************************************************
                            CCS SPECIFIC USE STATEMENTS            
****************************************************************************/ 

/****************************************************************************
                            DEFINES            
****************************************************************************/  
#define SIZEOF_BUFF                  (160)
/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/    

                          
/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/
static uint8_t msg[SIZEOF_BUFF];            //leave room to append a space and null on line return
static uint8_t *msg_ptr;
static uint16_t buffer_cnt = 0;   
uint8_t serialMessageRdy = 0;  
/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/ 

/*********************************************************************
Function:     pc_cmd_handler
Input:        void
Return:       void
Description:Callback function that is installed into the UxRX ISR. Parses
            the commands from terminal in real time.
Visibility: Public
Author:        NAM
Notes:
**********************************************************************/
void pc_cmd_handler(void)       
{                                
    char c = 0;           

    if(serial_GetFromBuffer(SERIAL_PC_RX,&c, 1) > 0)
    {
        switch (c)
        {
            case '\r':                 
    //            if(cli_buffer_cnt == 0) //display menu/prompt if only enter pressed
    //           {                 
    //                ClearTerminal();   
    //                cmd_display();   
    //            }
    //            else
                {                
                    // terminate the msg and reset the msg ptr. then send it to the handler for processing.
                    *msg_ptr = ' ';
                    //cli_buffer_cnt++;
                    msg_ptr++;
                    *msg_ptr = '\0';
                    //cli_buffer_cnt++;
                    serialMessageRdy = true;
                    msg_ptr = msg;
                   // echoCommand('\r');
                   // echoCommand('\n');
                    buffer_cnt = 0;
                }
                break;
                               
            case '\b':          // backspace                         
       //         if (msg_ptr > msg)
       //         {
       //             cli_buffer_cnt--;
       //             msg_ptr--;
       //             echoCommand('\b');
       //         } 
                break;
            case '\n':
                break;
            default:    // normal character entered. add it to the buffer
                if( buffer_cnt < SIZEOF_BUFF)
                {
                    *msg_ptr++ = c;
                    buffer_cnt++;
      //            echoCommand(c);
                }
                    
                break;
        }
    }
} 

/*********************************************************************
Function:     cmd_parse
Input:        void
Return:       int8_t: return code, -1 = error
Description: function to parse the available command from the terminal interface
Visibility: Public
Author:        NAM
Notes:
**********************************************************************/
_tPC_MSG_RETURN_CODES pc_cmd_parse(void)    
{
    uint8_t argc, i = 0;  
    //char *argv[MAX_PARAMS];
    _tPC_MSG_RETURN_CODES returnCode = PC_MSG_NULL;

    // parse the command line statement and break it up into space-delimited
    // strings. the array of strings will be saved in the argv array.     
    if(msg[0] != 0)
    {                                 
#if 0    
        argv[i] = strtok(msg, " /:");
        do
        {
            argv[++i] = strtok(NULL, " /:");
        } while ((i < MAX_PARAMS) && (argv[i] != NULL));

        // save off the number of arguments for the particular command.
        argc = i;

        // parse the command table for valid command. used argv[0] which is the
        // actual command name typed in at the prompt
        
        for (i = 0; i < NUM_COMMANDS; i++)
        {
            if (!strcmp(argv[0], commands[i].cmdName))
            {
                if (commands[i].func != NULL)
                {
                    returnCode = commands[i].func(argc, argv);
                }
                if( returnCode != PC_MSG_IN_PROGRESS)
                    serialMessageRdy = false;
                return returnCode;
            }                    
                                   
        }  
#endif

        serialMessageRdy = false;
        // command not recognized. print message and re-generate prompt.
        //serial_Add2Buffer(SERIAL_PC_TX, (char*)cmd_unrecog, strlen(cmd_unrecog));

        //cmd_display();
        //serial_Add2Buffer(SERIAL_PC_TX, (char*)cmd_prompt, strlen(cmd_prompt));
    }                
    else
        serialMessageRdy = false; //inputr error?
    return PC_MSG_ERROR;
}

