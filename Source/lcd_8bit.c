/****************************************************************************  
FileName:     lcd_8bit.c                                                                                         
Dependencies: CCS libraries
Processor:    PIC18            
Hardware:
Complier: CCS     
Company:                 
                                
                      
Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        10/2016    
                                         
*****************************************************************************
General description: Display driver code            
 Based on code from ELECTRONIC ASSEMBLY JM (Display Manu.)                                                                    
                                                    
****************************************************************************
                            INCLUDES             
****************************************************************************/
#include "lcd_8bit.h"       
                 
/****************************************************************************
                            DEFINES            
****************************************************************************/   
#define Wait(x)                 delay_ms(x) 
#define WAITST                  1

/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/                                               

/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/
 
/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/                                       
    
/*********************************************************************
Function:    initDispl
Input:       void
Output:      void
Dependencies:
Description: The init function
********************************************************************/  
void initDispl(void)
{   
    output_high(LCD_ENABLE_PIN);      //ddEN  = 1; 
    output_high(LCD_ENABLE_PIN);      //ddEN  = 1; 
    output_high(LCD_ENABLE_PIN);      //ddEN  = 1; 
                                         
    output_high( LCD_RW_PIN );      //ddRW = 1;
    output_high( LCD_RS_PIN );      //ddRS = 1;

    output_high(LCD_DATA7); 
    output_high(LCD_DATA6);
    output_high(LCD_DATA5);
    output_high(LCD_DATA4);
    output_high(LCD_DATA3);
    output_high(LCD_DATA2);
    output_high(LCD_DATA1);
    output_high(LCD_DATA0);         
    
    //Reset of LCD-Module is recommended,
    output_low(LCD_RESET);     
    delay_ms( 1 );
    output_high(LCD_RESET);                                
    delay_ms( 5 );
              
    //init Display  
    WriteIns(0x3A);    //8-Bit data length extension Bit RE=1; REV=0
    WriteIns(0x3A);    //8-Bit data length extension Bit RE=1; REV=0
    WriteIns(0x09);    //4 line display
    WriteIns(0x05);    //Bottom view             
    WriteIns(0x1E);    //Bias setting BS1=1
    WriteIns(0x39);    //8-Bit data length extension Bit RE=0; IS=1
    WriteIns(0x1B);    //BS0=1 -> Bias=1/6
    WriteIns(0x6E); //Devider on and set value
    WriteIns(0x57); //Booster on and set contrast (BB1=C5, DB0=C4)
    WriteIns(0x72); //Set contrast (DB3-DB0=C3-C0)
    WriteIns(0x38); //8-Bit data length extension Bit RE=0; IS=0  
    display_generate_cust_chars();              
    ClrDisplay();                  
    DisplayOnOff(DISPLAY_MSK & ~CURSOR_MSK & ~BLINK_MSK);
}               
                                                   
/*********************************************************************
Function:    display_generate_cust_chars
Input:       void
Output:      void
Dependencies:
Description: App function to write all necessary custom characters
********************************************************************/                                                     
void display_generate_cust_chars( void )      
{                                                       
    uint8_t stop[] =  { 0x00, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x00, 0x00};
    uint8_t play[] =  { 0x00, 0x08, 0x0c, 0x0e, 0x0c, 0x08, 0x00, 0x00};   
    uint8_t pause[] = { 0x00, 0x1b, 0x1b, 0x1b, 0x1b, 0x1b, 0x00, 0x00};    
    uint8_t time[] =  { 0x1f, 0x11, 0x0a, 0x04, 0x0a, 0x11, 0x1f, 0x00}; 
                                                                     
   DefineCharacter( 1, stop);   
   DefineCharacter( 2, play);                                 
   DefineCharacter( 3, pause);   
   DefineCharacter( 4, time);   
   
}
   
/*********************************************************************
Function:    WriteChar
Input:       char character: character to display
Output:      void
Dependencies:
Description: sends a single character to display      
********************************************************************/  
void WriteChar (char character)                                 
{
    WriteData(character);
}

/*********************************************************************
Function:    WriteString
Input:       char * string: pointer to string array
Output:      void
Dependencies:
Description: sends a string to display, must be 0 terminated 
********************************************************************/  
void WriteString( char * string)
{
    do
    {
        WriteData(*string++);
    }                                       
    while(*string);
}
 
/*********************************************************************
Function:    SetPostion
Input:       char pos: position, combo of line and column
Output:      void
Dependencies:
Description: set postion   
********************************************************************/  
void SetPostion(char pos)
{
    WriteIns(LCD_HOME_L1+pos);
}
  
/*********************************************************************
Function:    DisplayOnOff
Input:       char data: control
Output:      void
Dependencies:
Description: use definitions of header file to set display   
********************************************************************/  
void DisplayOnOff(char data)
{
    WriteIns(0x08+data);
}
    
/*********************************************************************
Function:    DefineCharacter
Input:       unsigned char postion: location in GRAM to store it
             unsigned char *data: pointer to array holding char data
Output:      void
Dependencies:
Description: stores an own defined character      
             Display can store 8, addresses 0 - 7
********************************************************************/  
void DefineCharacter(unsigned char postion, unsigned char *data)
{
    unsigned char i=0;
    WriteIns(0x40+8*postion);

    for (i=0; i<8; i++)
    {
        WriteData(data[i]);
    }
    SetPostion(LINE1);
}
 
/*********************************************************************
Function:    ClrDisplay
Input:       void
Output:      void
Dependencies:
Description: Clears entire Display content and set home pos   
********************************************************************/  
void ClrDisplay(void)
{                    
    WriteIns(0x01);
    SetPostion(LINE1);
}

/*********************************************************************
Function:    SetContrast
Input:       unsigned char contr: contrast setting
Output:      void  
Dependencies:
Description: Sets contrast 0..63  
********************************************************************/  
void SetContrast(unsigned char contr)
{
    WriteIns(0x39);
    WriteIns(0x54 | (contr >> 4));
    WriteIns(0x70 | (contr & 0x0F));
    WriteIns(0x38);     
}      

/*********************************************************************
Function:    SetView
Input:       unsigned char view: viewing angle, see header
Output:      void
Dependencies:
Description: Rotates the display 
            view bottom view(0x06), top view (0x05)   
********************************************************************/  
void SetView(unsigned char view)
{
    WriteIns(0x3A);
    WriteIns(view);
    WriteIns(0x38);
}

/*********************************************************************
Function:    SetROM
Input:       unsigned char rom_data: Which rom page to use
Output:      void
Dependencies:                                             
Description: Changes the Rom code, aka character maps
            (ROMA=0x00, ROMB=0x04, ROMC=0x0C)  
********************************************************************/  
void SetROM (unsigned char rom_data)
{
    WriteIns(0x3A);
    WriteIns(0x72);        
    WriteData(rom_data);
    WriteIns(0x38);
}

/*********************************************************************
Function:    WriteIns
Input:       uint8_t ins: instruction byte to send
Output:      void
Dependencies:
Description: sends instruction to display  
********************************************************************/  
static void WriteIns(uint8_t ins)
{               
    CheckBusy();
    output_low( LCD_RS_PIN );       //RS = 0;
    output_low( LCD_RW_PIN );       //RW = 0;      
                                  
    output_bit(LCD_DATA0, (ins & 0x01));
    ins >>= 1;  
    output_bit(LCD_DATA1, (ins & 0x01));
    ins >>= 1;
    output_bit(LCD_DATA2, (ins & 0x01));                
    ins >>= 1;
    output_bit(LCD_DATA3, (ins & 0x01));    
    ins >>= 1;           
    output_bit(LCD_DATA4, (ins & 0x01));
    ins >>= 1;  
    output_bit(LCD_DATA5, (ins & 0x01));
    ins >>= 1;                
    output_bit(LCD_DATA6, (ins & 0x01));                
    ins >>= 1;                 
    output_bit(LCD_DATA7, (ins & 0x01));      

    output_high( LCD_ENABLE_PIN );       //EN = 1; 
    Wait(WAITST);                        
    output_low( LCD_ENABLE_PIN );       //EN = 0;        
}          

/*********************************************************************
Function:    WriteData
Input:       uint8_t data: value to send
Output:      void
Dependencies:
Description: sends data to display     
********************************************************************/  
static void WriteData(uint8_t data)
{
    CheckBusy();                      
    output_high( LCD_RS_PIN );       //RS = `;
    output_low( LCD_RW_PIN );       //RW = 0;      
    //DATA = ins;                                   
    output_bit(LCD_DATA0, (data & 0x01));
    data >>= 1;  
    output_bit(LCD_DATA1, (data & 0x01));
    data >>= 1;
    output_bit(LCD_DATA2, (data & 0x01));                
    data >>= 1;
    output_bit(LCD_DATA3, (data & 0x01));    
    data >>= 1;           
    output_bit(LCD_DATA4, (data & 0x01));
    data >>= 1;  
    output_bit(LCD_DATA5, (data & 0x01));
    data >>= 1;                
    output_bit(LCD_DATA6, (data & 0x01));                
    data >>= 1;
    output_bit(LCD_DATA7, (data & 0x01));   
                                        
    output_high( LCD_ENABLE_PIN );       //EN = 1; 
    Wait(WAITST);
    output_low( LCD_ENABLE_PIN );       //EN = 0;        
}
  
/*********************************************************************
Function:    CheckBusy
Input:       void
Output:      unsigned char: 1 if ready/idle
Dependencies:
Description: checks if display is idle 
********************************************************************/  
unsigned char CheckBusy(void)
{
    unsigned char readData = 1;   
#if 0       //enable for read capability
    prc2 = 1;
    ddDATA = 0x00; // Data is input
    RS = 0;
    RW = 1;
    do
    {
        EN = 1;
        Wait(WAITST);
        EN = 0;
        readData = DATA; //read data directly falling edge
            Wait(WAITST);
    }while(readData&0x80); //check for busyflag
                       
    prc2 = 1;  //remove protection of port P0 to change port direction
    ddDATA = 0xFF;
#endif
    delay_us( 25 );
                
    return readData;
}





