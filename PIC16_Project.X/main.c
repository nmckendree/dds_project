     /****************************************************************************  
FileName:     main.c                                                                                       
Dependencies:  
Processor:                 
Hardware:
Complier:       
Company:    
                                
                      
Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        12/2016    
                                         
*****************************************************************************
General description: Main code for the project              
                                                                    
                                                    
****************************************************************************
                            INCLUDES             
****************************************************************************/
#include "project.h"
#include "user_debounce.h"
#include "system_Tick.h"

/****************************************************************************
                            DEFINES            
****************************************************************************/     
#define FW_VERSION                  1

#define TASK_10MS_PERIOD            10
#define TASK_1000MS_PERIOD          1000
#define DWELL_3MIN                  ( 3UL * 60UL * 1000UL)
#define DWELL_10MIN                 ( 10UL * 60UL * 1000UL)
/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/                                          
 
/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/
static _tDEBOUNCED_INPUT btn_start;
static _tDEBOUNCED_INPUT btn_stop; 
static uint32_t dwell_timer_ms = 0;         //uint32 to give minutes, up to 71582
static uint32_t task_10mS = 0;
static uint32_t dwell_timeout = DWELL_3MIN;
static uint32_t task_1s = 0;
static bool is_unit_running = false;
/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/  
void init_hardware( void );

/*********************************************************************
Function:    main
Input:       void
Output:      void
Dependencies:
Description: The main function
********************************************************************/ 
int main(void)            
{            
    init_hardware();           
    initSysTick();                       

    INTERRUPT_PeripheralInterruptEnable();
    INTERRUPT_GlobalInterruptEnable();
    
    while( 1 )                     
    {     
        if(SysTick_Ticked)
        {   
            SysTick_Ticked = 0;         
            if( timePassed( task_10mS ) >= TASK_10MS_PERIOD )
            {                
                task_10mS = GetSysTick();   
                debounceInput( &btn_start, iSTART_CTRL_GetValue() );
                debounceInput( &btn_stop, iSTOP_CTRL_GetValue() );
                
                if( is_unit_running )
                {
                    if( timePassed( dwell_timer_ms ) >= dwell_timeout || ButtonPressed( &btn_stop) )
                    {
                        PWM1_Stop();
                        oLED_SetLow();
                        is_unit_running = false;
                    }
                }
                else
                {
                    if( ButtonPressed( &btn_start))
                    {
                        dwell_timer_ms = GetSysTick();
                        oLED_SetHigh();
                        is_unit_running = true;
                        PWM1_Stop();
                        PWM1_Start();
                    }
                }
            }   
            if( timePassed( task_1s ) >= TASK_1000MS_PERIOD )
            {
                task_1s = GetSysTick();                
                dwell_timeout = ( (iDWELL_OPT_GetValue() == 0) ? DWELL_10MIN : DWELL_3MIN);                
            }
        }        
    }                           
    return (0);                        
}   

/*********************************************************************
Function:    init_hardware
Input:       void
Output:      void      
Dependencies:  
Description: init hardware components of the system        
                including the GPIO.
********************************************************************/        
void init_hardware( void )       
{    
    SYSTEM_Initialize();            //Microchip MCC generate function - init all hardware
    PWM1_Stop();                                  
    //Setup the debounce module for each user input. This will limit false triggers                         
    initDebounce( &btn_start, 0);                                                      
    initDebounce( &btn_stop, 0);                 

}