/****************************************************************************
FileName:     system_Tick.c
Dependencies:
Processor:
Hardware:
Complier:
Company:                        
                                                                                           

 Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


*****************************************************************************
General description:



                           

usage:                   

****************************************************************************
INCLUDES
 ****************************************************************************/
#include <stdint.h>
#include "mcc.h"
#include "system_Tick.h"
/****************************************************************************
                            DEFINES
 ****************************************************************************/

                    
/****************************************************************************
                            GLOBAL VARIABLES
 ****************************************************************************/
static volatile uint32_t SysTick = 0;
volatile uint8_t SysTick_Ticked = 0;
/****************************************************************************
                            FUNCTION PROTOTYPES
 ****************************************************************************/
uint32_t timePassed(uint32_t timeOrig);
uint32_t GetSysTick(void);        
void systemTimerCallback(void);
/*********************************************************************
Function:       sysTickHandler
Input:          void                     
Output:         void
Dependencies:
Description:    ISR, TIMER1
 ********************************************************************/       
 //#INT_TIMER2                                  
void systemTimerCallback(void) 
{     
    SysTick_Ticked = 1;
    SysTick++;                                                
//    clear_interrupt(int_timer2);           
}  
                             
                  
/*********************************************************************
Function:       GetSysTick
Input:          void
Output:         uint32_t: Current system time, use TICKS_PER_MS,TICKS_PER_S 
                    for timing. (.5ms or 1ms could be the source depending on setitngs)
Dependencies:
Description:    returns the current system tick
 ********************************************************************/
uint32_t GetSysTick(void)
{
    return SysTick;
}                  

/*********************************************************************
Function:       initSysTick
Input:          void
Output:         void
Dependencies:           
Description:    Utilize timer 1 as the systick source
 ********************************************************************/
void initSysTick(void)
{
    TMR1_Initialize();
    TMR1_StopTimer();
    TMR1_SetInterruptHandler(systemTimerCallback);
    TMR1_StartTimer();
}
                   
/*********************************************************************
Function:       initSysTick
Input:          uint32_t millisec: desired millisec delay 
Output:         void
Dependencies:
Description:    Delay built on the sys tick
 ********************************************************************/
void SysWait(uint32_t millisec)
{
#ifdef USE_WDT
    mDIS_WDT();
#endif
    uint32_t CurrTime = SysTick;
    millisec *= TICKS_PER_MS;

    while((int32_t)(SysTick - CurrTime) < millisec);
#ifdef USE_WDT    
    mEN_WDT();
#endif                  
}

/*********************************************************************
Function:       timePassed
Input:          uint32_t timeOrig: Original "start" time to compare against current time (in mS)
Output:         uint32_t: Elapsed time in mS
Dependencies:
Description:    
 ********************************************************************/
uint32_t timePassed(uint32_t timeOrig)
{
    uint32_t now = GetSysTick();
    
    if( now >= timeOrig)
    {
        return (now - timeOrig);
    }   
    //rollover has occurred
    return( now + (1 + TIME_MAX - timeOrig));
}
      
