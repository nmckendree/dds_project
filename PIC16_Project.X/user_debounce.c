/****************************************************************************
FileName:     userInput.c
Dependencies:
Processor:
Hardware:
Complier: CCS 
Company:      


Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                         

*****************************************************************************
General description: 
     




usage:
 
 initDebounce call for each pin needed to debounce
 Call debounceInput between 1 to 5 mS
 ButtonPressed will return if the button is currently pressed or not
 
 For the count, I am using a windows count value that is offset from 0 so 
 I do not need to use a signed value.
 
 Areas of improvement
 -  make port compatible instead of pin by pin
 -  Add a time value so not dependent on calling frequency   
****************************************************************************
                            INCLUDES
****************************************************************************/
#include <stdint.h>
#include "user_debounce.h"              
/****************************************************************************
                            DEFINES            
 ****************************************************************************/
#define DEBOUNCE_COUNT              5      //number of consecutive counts before "debounced"
#define WINDOWED_COUNT_LOW          5
#define WINDOWED_COUNT_HIGH         ( WINDOWED_COUNT_LOW + DEBOUNCE_COUNT)
/****************************************************************************
                            GLOBAL VARIABLES
 ****************************************************************************/


/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/

/*********************************************************************
Function:    initDebounce
Input:      _tDEBOUNCED_INPUT *pin: pin data structure
            uint8_t initalState: Value of unpressed state, is switch normally pulled up or down?
Output:     void
Dependencies:
Description:    Gets the veh id from the 3 address switches. Have to
                XOR the pins to get the inverted value.
********************************************************************/ 
void initDebounce( _tDEBOUNCED_INPUT *pin, uint8_t initalState)
{
    pin->changed = 0;
    pin->counter = WINDOWED_COUNT_LOW;
    pin->debouncedState = !initalState;
    pin->prevDebouncedState = 0;
    pin->activeState = initalState;
}

/*********************************************************************
Function:    debounceInput
Input:      _tDEBOUNCED_INPUT *pin: pin data structure
            uint8_t rawInput: New input from a button/switch
Output:     void
Dependencies:
Description:    Updates the count for the pin 
********************************************************************/
void debounceInput( _tDEBOUNCED_INPUT *pin, uint8_t rawInput)
{
   pin->prevDebouncedState = pin->debouncedState;
   if(rawInput)
   {
       pin->counter++;
   }
   else
   {
       pin->counter--;
   }
   
   if( pin->counter > WINDOWED_COUNT_HIGH)
   {
       pin->counter = WINDOWED_COUNT_HIGH;
       pin->debouncedState = 1;       
   }
   else if( pin->counter < WINDOWED_COUNT_LOW)
   {
       pin->counter = WINDOWED_COUNT_LOW;
       pin->debouncedState = 0;
   }
   
   if( pin->prevDebouncedState != pin->debouncedState)
       pin->changed = 1;
   else
       pin->changed = 0;
   
       
}

/*********************************************************************
Function:    ButtonPressed
Input:      _tDEBOUNCED_INPUT *pin: Pin data structure
Output:     uint8_t: 1 = button pressed, 0 = not pressed
Dependencies:
Description:  Returns if the button is pressed
********************************************************************/
uint8_t ButtonPressed( _tDEBOUNCED_INPUT *pin )
{
    return( (pin->debouncedState == pin->activeState));
}
                                                     
