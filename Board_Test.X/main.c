/* 
 * File:   main.c
 * Author: Nick
 *
 * Created on November 25, 2016, 9:17 PM
 */



/****************************************************************************  
FileName:     main.c                                                                                       
Dependencies: CCS libraries
Processor:    PIC18            
Hardware:
Complier: CCS     
Company:    
                                
                      
Author                Date            Comment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Nick McKendree        10/2016    
                                         
*****************************************************************************
General description: Main code for the project              
                                                                    
                                                    
****************************************************************************
                            INCLUDES             
****************************************************************************/
#include "project.h"                                       

/****************************************************************************
                            CCS SPECIFIC USE STATEMENTS            
****************************************************************************/       
                 
/****************************************************************************
                            DEFINES            
****************************************************************************/                                   
                 
/****************************************************************************
                            PRIVATE TYPEDEFS            
****************************************************************************/                                          

/****************************************************************************
                            GLOBAL VARIABLES
****************************************************************************/

/****************************************************************************
                            FUNCTION PROTOTYPES
****************************************************************************/  
void delay_100ms(void);

/*********************************************************************
Function:    main
Input:       void
Output:      void
Dependencies:
Description: The main function
********************************************************************/     
int main(void)            
{          
    uint8_t i = 0;
                                 
    SYSTEM_Initialize();           
 
    for(i = 0; i < 100; i++)
    {
        __delay_ms(10); //startup delay to see splash screen  
    }

    while( 1 )                     
    {     
        LCD_DATA3_Toggle();
        LCD_DATA2_Toggle();
        delay_100ms();
    }                           
    return (0);                        
}                          
   
void delay_100ms(void)
{
    uint8_t i = 0;
    
    for(i = 0; i < 10; i++)
    {
        __delay_ms(10); //startup delay to see splash screen  
    }
}