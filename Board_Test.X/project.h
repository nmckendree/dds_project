/* 
 * File:   project.h
 * Author: Nick
 *
 * Created on November 25, 2016, 9:17 PM
 */

#ifndef MAIN_H
#define	MAIN_H


#ifdef	__cplusplus
extern "C" {
#endif
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include "mcc.h"    
#include "pin_manager.h"    

#ifdef	__cplusplus
}
#endif

#endif	/* PROJECT_H */

