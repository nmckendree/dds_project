/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using MPLAB(c) Code Configurator

  @Description:
    This header file provides implementations for pin APIs for all pins selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 4.0
        Device            :  PIC18F46K22
        Version           :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.40

    Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

    Microchip licenses to you the right to use, modify, copy and distribute
    Software only when embedded on a Microchip microcontroller or digital signal
    controller that is integrated into your product or third party product
    (pursuant to the sublicense terms in the accompanying license agreement).

    You should refer to the license agreement accompanying this Software for
    additional information regarding your rights and obligations.

    SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
    EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
    MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
    IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
    CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
    OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
    INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
    CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
    SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
    (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

*/


#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set LCD_DATA7 aliases
#define LCD_DATA7_TRIS               TRISAbits.TRISA0
#define LCD_DATA7_LAT                LATAbits.LATA0
#define LCD_DATA7_PORT               PORTAbits.RA0
#define LCD_DATA7_ANS                ANSELAbits.ANSA0
#define LCD_DATA7_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define LCD_DATA7_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define LCD_DATA7_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define LCD_DATA7_GetValue()           PORTAbits.RA0
#define LCD_DATA7_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define LCD_DATA7_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define LCD_DATA7_SetAnalogMode()  do { ANSELAbits.ANSA0 = 1; } while(0)
#define LCD_DATA7_SetDigitalMode() do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set LCD_DATA6 aliases
#define LCD_DATA6_TRIS               TRISAbits.TRISA1
#define LCD_DATA6_LAT                LATAbits.LATA1
#define LCD_DATA6_PORT               PORTAbits.RA1
#define LCD_DATA6_ANS                ANSELAbits.ANSA1
#define LCD_DATA6_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define LCD_DATA6_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define LCD_DATA6_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define LCD_DATA6_GetValue()           PORTAbits.RA1
#define LCD_DATA6_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define LCD_DATA6_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define LCD_DATA6_SetAnalogMode()  do { ANSELAbits.ANSA1 = 1; } while(0)
#define LCD_DATA6_SetDigitalMode() do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set LED_DRIVER_EN aliases
#define LED_DRIVER_EN_TRIS               TRISAbits.TRISA2
#define LED_DRIVER_EN_LAT                LATAbits.LATA2
#define LED_DRIVER_EN_PORT               PORTAbits.RA2
#define LED_DRIVER_EN_ANS                ANSELAbits.ANSA2
#define LED_DRIVER_EN_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define LED_DRIVER_EN_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define LED_DRIVER_EN_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define LED_DRIVER_EN_GetValue()           PORTAbits.RA2
#define LED_DRIVER_EN_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define LED_DRIVER_EN_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define LED_DRIVER_EN_SetAnalogMode()  do { ANSELAbits.ANSA2 = 1; } while(0)
#define LED_DRIVER_EN_SetDigitalMode() do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set LCD_DATA4 aliases
#define LCD_DATA4_TRIS               TRISAbits.TRISA3
#define LCD_DATA4_LAT                LATAbits.LATA3
#define LCD_DATA4_PORT               PORTAbits.RA3
#define LCD_DATA4_ANS                ANSELAbits.ANSA3
#define LCD_DATA4_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define LCD_DATA4_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define LCD_DATA4_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define LCD_DATA4_GetValue()           PORTAbits.RA3
#define LCD_DATA4_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define LCD_DATA4_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define LCD_DATA4_SetAnalogMode()  do { ANSELAbits.ANSA3 = 1; } while(0)
#define LCD_DATA4_SetDigitalMode() do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set LCD_DATA1 aliases
#define LCD_DATA1_TRIS               TRISAbits.TRISA4
#define LCD_DATA1_LAT                LATAbits.LATA4
#define LCD_DATA1_PORT               PORTAbits.RA4
#define LCD_DATA1_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define LCD_DATA1_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define LCD_DATA1_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define LCD_DATA1_GetValue()           PORTAbits.RA4
#define LCD_DATA1_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define LCD_DATA1_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)

// get/set LCD_DATA5 aliases
#define LCD_DATA5_TRIS               TRISAbits.TRISA5
#define LCD_DATA5_LAT                LATAbits.LATA5
#define LCD_DATA5_PORT               PORTAbits.RA5
#define LCD_DATA5_ANS                ANSELAbits.ANSA5
#define LCD_DATA5_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define LCD_DATA5_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define LCD_DATA5_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define LCD_DATA5_GetValue()           PORTAbits.RA5
#define LCD_DATA5_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define LCD_DATA5_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define LCD_DATA5_SetAnalogMode()  do { ANSELAbits.ANSA5 = 1; } while(0)
#define LCD_DATA5_SetDigitalMode() do { ANSELAbits.ANSA5 = 0; } while(0)

// get/set LCD_DATA3 aliases
#define LCD_DATA3_TRIS               TRISBbits.TRISB0
#define LCD_DATA3_LAT                LATBbits.LATB0
#define LCD_DATA3_PORT               PORTBbits.RB0
#define LCD_DATA3_WPU                WPUBbits.WPUB0
#define LCD_DATA3_ANS                ANSELBbits.ANSB0
#define LCD_DATA3_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define LCD_DATA3_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define LCD_DATA3_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define LCD_DATA3_GetValue()           PORTBbits.RB0
#define LCD_DATA3_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define LCD_DATA3_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define LCD_DATA3_SetPullup()      do { WPUBbits.WPUB0 = 1; } while(0)
#define LCD_DATA3_ResetPullup()    do { WPUBbits.WPUB0 = 0; } while(0)
#define LCD_DATA3_SetAnalogMode()  do { ANSELBbits.ANSB0 = 1; } while(0)
#define LCD_DATA3_SetDigitalMode() do { ANSELBbits.ANSB0 = 0; } while(0)

// get/set LCD_DATA2 aliases
#define LCD_DATA2_TRIS               TRISBbits.TRISB1
#define LCD_DATA2_LAT                LATBbits.LATB1
#define LCD_DATA2_PORT               PORTBbits.RB1
#define LCD_DATA2_WPU                WPUBbits.WPUB1
#define LCD_DATA2_ANS                ANSELBbits.ANSB1
#define LCD_DATA2_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define LCD_DATA2_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define LCD_DATA2_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define LCD_DATA2_GetValue()           PORTBbits.RB1
#define LCD_DATA2_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define LCD_DATA2_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define LCD_DATA2_SetPullup()      do { WPUBbits.WPUB1 = 1; } while(0)
#define LCD_DATA2_ResetPullup()    do { WPUBbits.WPUB1 = 0; } while(0)
#define LCD_DATA2_SetAnalogMode()  do { ANSELBbits.ANSB1 = 1; } while(0)
#define LCD_DATA2_SetDigitalMode() do { ANSELBbits.ANSB1 = 0; } while(0)

// get/set LCD_DATA0 aliases
#define LCD_DATA0_TRIS               TRISBbits.TRISB2
#define LCD_DATA0_LAT                LATBbits.LATB2
#define LCD_DATA0_PORT               PORTBbits.RB2
#define LCD_DATA0_WPU                WPUBbits.WPUB2
#define LCD_DATA0_ANS                ANSELBbits.ANSB2
#define LCD_DATA0_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define LCD_DATA0_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define LCD_DATA0_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define LCD_DATA0_GetValue()           PORTBbits.RB2
#define LCD_DATA0_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define LCD_DATA0_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define LCD_DATA0_SetPullup()      do { WPUBbits.WPUB2 = 1; } while(0)
#define LCD_DATA0_ResetPullup()    do { WPUBbits.WPUB2 = 0; } while(0)
#define LCD_DATA0_SetAnalogMode()  do { ANSELBbits.ANSB2 = 1; } while(0)
#define LCD_DATA0_SetDigitalMode() do { ANSELBbits.ANSB2 = 0; } while(0)

// get/set BTN_STOP aliases
#define BTN_STOP_TRIS               TRISBbits.TRISB3
#define BTN_STOP_LAT                LATBbits.LATB3
#define BTN_STOP_PORT               PORTBbits.RB3
#define BTN_STOP_WPU                WPUBbits.WPUB3
#define BTN_STOP_ANS                ANSELBbits.ANSB3
#define BTN_STOP_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define BTN_STOP_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define BTN_STOP_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define BTN_STOP_GetValue()           PORTBbits.RB3
#define BTN_STOP_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define BTN_STOP_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define BTN_STOP_SetPullup()      do { WPUBbits.WPUB3 = 1; } while(0)
#define BTN_STOP_ResetPullup()    do { WPUBbits.WPUB3 = 0; } while(0)
#define BTN_STOP_SetAnalogMode()  do { ANSELBbits.ANSB3 = 1; } while(0)
#define BTN_STOP_SetDigitalMode() do { ANSELBbits.ANSB3 = 0; } while(0)

// get/set BTN_PAUSE aliases
#define BTN_PAUSE_TRIS               TRISBbits.TRISB4
#define BTN_PAUSE_LAT                LATBbits.LATB4
#define BTN_PAUSE_PORT               PORTBbits.RB4
#define BTN_PAUSE_WPU                WPUBbits.WPUB4
#define BTN_PAUSE_ANS                ANSELBbits.ANSB4
#define BTN_PAUSE_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define BTN_PAUSE_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define BTN_PAUSE_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define BTN_PAUSE_GetValue()           PORTBbits.RB4
#define BTN_PAUSE_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define BTN_PAUSE_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define BTN_PAUSE_SetPullup()      do { WPUBbits.WPUB4 = 1; } while(0)
#define BTN_PAUSE_ResetPullup()    do { WPUBbits.WPUB4 = 0; } while(0)
#define BTN_PAUSE_SetAnalogMode()  do { ANSELBbits.ANSB4 = 1; } while(0)
#define BTN_PAUSE_SetDigitalMode() do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set BTN_SELECT aliases
#define BTN_SELECT_TRIS               TRISBbits.TRISB5
#define BTN_SELECT_LAT                LATBbits.LATB5
#define BTN_SELECT_PORT               PORTBbits.RB5
#define BTN_SELECT_WPU                WPUBbits.WPUB5
#define BTN_SELECT_ANS                ANSELBbits.ANSB5
#define BTN_SELECT_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define BTN_SELECT_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define BTN_SELECT_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define BTN_SELECT_GetValue()           PORTBbits.RB5
#define BTN_SELECT_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define BTN_SELECT_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define BTN_SELECT_SetPullup()      do { WPUBbits.WPUB5 = 1; } while(0)
#define BTN_SELECT_ResetPullup()    do { WPUBbits.WPUB5 = 0; } while(0)
#define BTN_SELECT_SetAnalogMode()  do { ANSELBbits.ANSB5 = 1; } while(0)
#define BTN_SELECT_SetDigitalMode() do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set freq_FSYNC aliases
#define freq_FSYNC_TRIS               TRISCbits.TRISC0
#define freq_FSYNC_LAT                LATCbits.LATC0
#define freq_FSYNC_PORT               PORTCbits.RC0
#define freq_FSYNC_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define freq_FSYNC_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define freq_FSYNC_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define freq_FSYNC_GetValue()           PORTCbits.RC0
#define freq_FSYNC_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define freq_FSYNC_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)

// get/set freq_Interrupt aliases
#define freq_Interrupt_TRIS               TRISCbits.TRISC1
#define freq_Interrupt_LAT                LATCbits.LATC1
#define freq_Interrupt_PORT               PORTCbits.RC1
#define freq_Interrupt_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define freq_Interrupt_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define freq_Interrupt_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define freq_Interrupt_GetValue()           PORTCbits.RC1
#define freq_Interrupt_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define freq_Interrupt_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)

// get/set freq_Control aliases
#define freq_Control_TRIS               TRISCbits.TRISC2
#define freq_Control_LAT                LATCbits.LATC2
#define freq_Control_PORT               PORTCbits.RC2
#define freq_Control_ANS                ANSELCbits.ANSC2
#define freq_Control_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define freq_Control_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define freq_Control_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define freq_Control_GetValue()           PORTCbits.RC2
#define freq_Control_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define freq_Control_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define freq_Control_SetAnalogMode()  do { ANSELCbits.ANSC2 = 1; } while(0)
#define freq_Control_SetDigitalMode() do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set RC3 procedures
#define RC3_SetHigh()    do { LATCbits.LATC3 = 1; } while(0)
#define RC3_SetLow()   do { LATCbits.LATC3 = 0; } while(0)
#define RC3_Toggle()   do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define RC3_GetValue()         PORTCbits.RC3
#define RC3_SetDigitalInput()   do { TRISCbits.TRISC3 = 1; } while(0)
#define RC3_SetDigitalOutput()  do { TRISCbits.TRISC3 = 0; } while(0)
#define RC3_SetAnalogMode() do { ANSELCbits.ANSC3 = 1; } while(0)
#define RC3_SetDigitalMode()do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set freq_Standby aliases
#define freq_Standby_TRIS               TRISCbits.TRISC4
#define freq_Standby_LAT                LATCbits.LATC4
#define freq_Standby_PORT               PORTCbits.RC4
#define freq_Standby_ANS                ANSELCbits.ANSC4
#define freq_Standby_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define freq_Standby_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define freq_Standby_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define freq_Standby_GetValue()           PORTCbits.RC4
#define freq_Standby_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define freq_Standby_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define freq_Standby_SetAnalogMode()  do { ANSELCbits.ANSC4 = 1; } while(0)
#define freq_Standby_SetDigitalMode() do { ANSELCbits.ANSC4 = 0; } while(0)

// get/set RC5 procedures
#define RC5_SetHigh()    do { LATCbits.LATC5 = 1; } while(0)
#define RC5_SetLow()   do { LATCbits.LATC5 = 0; } while(0)
#define RC5_Toggle()   do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define RC5_GetValue()         PORTCbits.RC5
#define RC5_SetDigitalInput()   do { TRISCbits.TRISC5 = 1; } while(0)
#define RC5_SetDigitalOutput()  do { TRISCbits.TRISC5 = 0; } while(0)
#define RC5_SetAnalogMode() do { ANSELCbits.ANSC5 = 1; } while(0)
#define RC5_SetDigitalMode()do { ANSELCbits.ANSC5 = 0; } while(0)

// get/set RC6 procedures
#define RC6_SetHigh()    do { LATCbits.LATC6 = 1; } while(0)
#define RC6_SetLow()   do { LATCbits.LATC6 = 0; } while(0)
#define RC6_Toggle()   do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define RC6_GetValue()         PORTCbits.RC6
#define RC6_SetDigitalInput()   do { TRISCbits.TRISC6 = 1; } while(0)
#define RC6_SetDigitalOutput()  do { TRISCbits.TRISC6 = 0; } while(0)
#define RC6_SetAnalogMode() do { ANSELCbits.ANSC6 = 1; } while(0)
#define RC6_SetDigitalMode()do { ANSELCbits.ANSC6 = 0; } while(0)

// get/set RC7 procedures
#define RC7_SetHigh()    do { LATCbits.LATC7 = 1; } while(0)
#define RC7_SetLow()   do { LATCbits.LATC7 = 0; } while(0)
#define RC7_Toggle()   do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define RC7_GetValue()         PORTCbits.RC7
#define RC7_SetDigitalInput()   do { TRISCbits.TRISC7 = 1; } while(0)
#define RC7_SetDigitalOutput()  do { TRISCbits.TRISC7 = 0; } while(0)
#define RC7_SetAnalogMode() do { ANSELCbits.ANSC7 = 1; } while(0)
#define RC7_SetDigitalMode()do { ANSELCbits.ANSC7 = 0; } while(0)

// get/set RD0 procedures
#define RD0_SetHigh()    do { LATDbits.LATD0 = 1; } while(0)
#define RD0_SetLow()   do { LATDbits.LATD0 = 0; } while(0)
#define RD0_Toggle()   do { LATDbits.LATD0 = ~LATDbits.LATD0; } while(0)
#define RD0_GetValue()         PORTDbits.RD0
#define RD0_SetDigitalInput()   do { TRISDbits.TRISD0 = 1; } while(0)
#define RD0_SetDigitalOutput()  do { TRISDbits.TRISD0 = 0; } while(0)
#define RD0_SetAnalogMode() do { ANSELDbits.ANSD0 = 1; } while(0)
#define RD0_SetDigitalMode()do { ANSELDbits.ANSD0 = 0; } while(0)

// get/set RD1 procedures
#define RD1_SetHigh()    do { LATDbits.LATD1 = 1; } while(0)
#define RD1_SetLow()   do { LATDbits.LATD1 = 0; } while(0)
#define RD1_Toggle()   do { LATDbits.LATD1 = ~LATDbits.LATD1; } while(0)
#define RD1_GetValue()         PORTDbits.RD1
#define RD1_SetDigitalInput()   do { TRISDbits.TRISD1 = 1; } while(0)
#define RD1_SetDigitalOutput()  do { TRISDbits.TRISD1 = 0; } while(0)
#define RD1_SetAnalogMode() do { ANSELDbits.ANSD1 = 1; } while(0)
#define RD1_SetDigitalMode()do { ANSELDbits.ANSD1 = 0; } while(0)

// get/set LCD_ENABLE_PIN aliases
#define LCD_ENABLE_PIN_TRIS               TRISDbits.TRISD2
#define LCD_ENABLE_PIN_LAT                LATDbits.LATD2
#define LCD_ENABLE_PIN_PORT               PORTDbits.RD2
#define LCD_ENABLE_PIN_ANS                ANSELDbits.ANSD2
#define LCD_ENABLE_PIN_SetHigh()            do { LATDbits.LATD2 = 1; } while(0)
#define LCD_ENABLE_PIN_SetLow()             do { LATDbits.LATD2 = 0; } while(0)
#define LCD_ENABLE_PIN_Toggle()             do { LATDbits.LATD2 = ~LATDbits.LATD2; } while(0)
#define LCD_ENABLE_PIN_GetValue()           PORTDbits.RD2
#define LCD_ENABLE_PIN_SetDigitalInput()    do { TRISDbits.TRISD2 = 1; } while(0)
#define LCD_ENABLE_PIN_SetDigitalOutput()   do { TRISDbits.TRISD2 = 0; } while(0)
#define LCD_ENABLE_PIN_SetAnalogMode()  do { ANSELDbits.ANSD2 = 1; } while(0)
#define LCD_ENABLE_PIN_SetDigitalMode() do { ANSELDbits.ANSD2 = 0; } while(0)

// get/set LCD_RW_PIN aliases
#define LCD_RW_PIN_TRIS               TRISDbits.TRISD3
#define LCD_RW_PIN_LAT                LATDbits.LATD3
#define LCD_RW_PIN_PORT               PORTDbits.RD3
#define LCD_RW_PIN_ANS                ANSELDbits.ANSD3
#define LCD_RW_PIN_SetHigh()            do { LATDbits.LATD3 = 1; } while(0)
#define LCD_RW_PIN_SetLow()             do { LATDbits.LATD3 = 0; } while(0)
#define LCD_RW_PIN_Toggle()             do { LATDbits.LATD3 = ~LATDbits.LATD3; } while(0)
#define LCD_RW_PIN_GetValue()           PORTDbits.RD3
#define LCD_RW_PIN_SetDigitalInput()    do { TRISDbits.TRISD3 = 1; } while(0)
#define LCD_RW_PIN_SetDigitalOutput()   do { TRISDbits.TRISD3 = 0; } while(0)
#define LCD_RW_PIN_SetAnalogMode()  do { ANSELDbits.ANSD3 = 1; } while(0)
#define LCD_RW_PIN_SetDigitalMode() do { ANSELDbits.ANSD3 = 0; } while(0)

// get/set LCD_RS_PIN aliases
#define LCD_RS_PIN_TRIS               TRISDbits.TRISD4
#define LCD_RS_PIN_LAT                LATDbits.LATD4
#define LCD_RS_PIN_PORT               PORTDbits.RD4
#define LCD_RS_PIN_ANS                ANSELDbits.ANSD4
#define LCD_RS_PIN_SetHigh()            do { LATDbits.LATD4 = 1; } while(0)
#define LCD_RS_PIN_SetLow()             do { LATDbits.LATD4 = 0; } while(0)
#define LCD_RS_PIN_Toggle()             do { LATDbits.LATD4 = ~LATDbits.LATD4; } while(0)
#define LCD_RS_PIN_GetValue()           PORTDbits.RD4
#define LCD_RS_PIN_SetDigitalInput()    do { TRISDbits.TRISD4 = 1; } while(0)
#define LCD_RS_PIN_SetDigitalOutput()   do { TRISDbits.TRISD4 = 0; } while(0)
#define LCD_RS_PIN_SetAnalogMode()  do { ANSELDbits.ANSD4 = 1; } while(0)
#define LCD_RS_PIN_SetDigitalMode() do { ANSELDbits.ANSD4 = 0; } while(0)

// get/set BTN_DOWN aliases
#define BTN_DOWN_TRIS               TRISDbits.TRISD5
#define BTN_DOWN_LAT                LATDbits.LATD5
#define BTN_DOWN_PORT               PORTDbits.RD5
#define BTN_DOWN_ANS                ANSELDbits.ANSD5
#define BTN_DOWN_SetHigh()            do { LATDbits.LATD5 = 1; } while(0)
#define BTN_DOWN_SetLow()             do { LATDbits.LATD5 = 0; } while(0)
#define BTN_DOWN_Toggle()             do { LATDbits.LATD5 = ~LATDbits.LATD5; } while(0)
#define BTN_DOWN_GetValue()           PORTDbits.RD5
#define BTN_DOWN_SetDigitalInput()    do { TRISDbits.TRISD5 = 1; } while(0)
#define BTN_DOWN_SetDigitalOutput()   do { TRISDbits.TRISD5 = 0; } while(0)
#define BTN_DOWN_SetAnalogMode()  do { ANSELDbits.ANSD5 = 1; } while(0)
#define BTN_DOWN_SetDigitalMode() do { ANSELDbits.ANSD5 = 0; } while(0)

// get/set BTN_RIGHT aliases
#define BTN_RIGHT_TRIS               TRISDbits.TRISD6
#define BTN_RIGHT_LAT                LATDbits.LATD6
#define BTN_RIGHT_PORT               PORTDbits.RD6
#define BTN_RIGHT_ANS                ANSELDbits.ANSD6
#define BTN_RIGHT_SetHigh()            do { LATDbits.LATD6 = 1; } while(0)
#define BTN_RIGHT_SetLow()             do { LATDbits.LATD6 = 0; } while(0)
#define BTN_RIGHT_Toggle()             do { LATDbits.LATD6 = ~LATDbits.LATD6; } while(0)
#define BTN_RIGHT_GetValue()           PORTDbits.RD6
#define BTN_RIGHT_SetDigitalInput()    do { TRISDbits.TRISD6 = 1; } while(0)
#define BTN_RIGHT_SetDigitalOutput()   do { TRISDbits.TRISD6 = 0; } while(0)
#define BTN_RIGHT_SetAnalogMode()  do { ANSELDbits.ANSD6 = 1; } while(0)
#define BTN_RIGHT_SetDigitalMode() do { ANSELDbits.ANSD6 = 0; } while(0)

// get/set BTN_LEFT aliases
#define BTN_LEFT_TRIS               TRISDbits.TRISD7
#define BTN_LEFT_LAT                LATDbits.LATD7
#define BTN_LEFT_PORT               PORTDbits.RD7
#define BTN_LEFT_ANS                ANSELDbits.ANSD7
#define BTN_LEFT_SetHigh()            do { LATDbits.LATD7 = 1; } while(0)
#define BTN_LEFT_SetLow()             do { LATDbits.LATD7 = 0; } while(0)
#define BTN_LEFT_Toggle()             do { LATDbits.LATD7 = ~LATDbits.LATD7; } while(0)
#define BTN_LEFT_GetValue()           PORTDbits.RD7
#define BTN_LEFT_SetDigitalInput()    do { TRISDbits.TRISD7 = 1; } while(0)
#define BTN_LEFT_SetDigitalOutput()   do { TRISDbits.TRISD7 = 0; } while(0)
#define BTN_LEFT_SetAnalogMode()  do { ANSELDbits.ANSD7 = 1; } while(0)
#define BTN_LEFT_SetDigitalMode() do { ANSELDbits.ANSD7 = 0; } while(0)

// get/set BTN_UP aliases
#define BTN_UP_TRIS               TRISEbits.TRISE0
#define BTN_UP_LAT                LATEbits.LATE0
#define BTN_UP_PORT               PORTEbits.RE0
#define BTN_UP_ANS                ANSELEbits.ANSE0
#define BTN_UP_SetHigh()            do { LATEbits.LATE0 = 1; } while(0)
#define BTN_UP_SetLow()             do { LATEbits.LATE0 = 0; } while(0)
#define BTN_UP_Toggle()             do { LATEbits.LATE0 = ~LATEbits.LATE0; } while(0)
#define BTN_UP_GetValue()           PORTEbits.RE0
#define BTN_UP_SetDigitalInput()    do { TRISEbits.TRISE0 = 1; } while(0)
#define BTN_UP_SetDigitalOutput()   do { TRISEbits.TRISE0 = 0; } while(0)
#define BTN_UP_SetAnalogMode()  do { ANSELEbits.ANSE0 = 1; } while(0)
#define BTN_UP_SetDigitalMode() do { ANSELEbits.ANSE0 = 0; } while(0)

// get/set BTN_POWER aliases
#define BTN_POWER_TRIS               TRISEbits.TRISE1
#define BTN_POWER_LAT                LATEbits.LATE1
#define BTN_POWER_PORT               PORTEbits.RE1
#define BTN_POWER_ANS                ANSELEbits.ANSE1
#define BTN_POWER_SetHigh()            do { LATEbits.LATE1 = 1; } while(0)
#define BTN_POWER_SetLow()             do { LATEbits.LATE1 = 0; } while(0)
#define BTN_POWER_Toggle()             do { LATEbits.LATE1 = ~LATEbits.LATE1; } while(0)
#define BTN_POWER_GetValue()           PORTEbits.RE1
#define BTN_POWER_SetDigitalInput()    do { TRISEbits.TRISE1 = 1; } while(0)
#define BTN_POWER_SetDigitalOutput()   do { TRISEbits.TRISE1 = 0; } while(0)
#define BTN_POWER_SetAnalogMode()  do { ANSELEbits.ANSE1 = 1; } while(0)
#define BTN_POWER_SetDigitalMode() do { ANSELEbits.ANSE1 = 0; } while(0)

// get/set LCD_RESET aliases
#define LCD_RESET_TRIS               TRISEbits.TRISE2
#define LCD_RESET_LAT                LATEbits.LATE2
#define LCD_RESET_PORT               PORTEbits.RE2
#define LCD_RESET_ANS                ANSELEbits.ANSE2
#define LCD_RESET_SetHigh()            do { LATEbits.LATE2 = 1; } while(0)
#define LCD_RESET_SetLow()             do { LATEbits.LATE2 = 0; } while(0)
#define LCD_RESET_Toggle()             do { LATEbits.LATE2 = ~LATEbits.LATE2; } while(0)
#define LCD_RESET_GetValue()           PORTEbits.RE2
#define LCD_RESET_SetDigitalInput()    do { TRISEbits.TRISE2 = 1; } while(0)
#define LCD_RESET_SetDigitalOutput()   do { TRISEbits.TRISE2 = 0; } while(0)
#define LCD_RESET_SetAnalogMode()  do { ANSELEbits.ANSE2 = 1; } while(0)
#define LCD_RESET_SetDigitalMode() do { ANSELEbits.ANSE2 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/