opt subtitle "Microchip Technology Omniscient Code Generator v1.38 (Free mode) build 201607010351"

opt pagewidth 120

	opt lm

	processor	18F46K22
porta	equ	0F80h
portb	equ	0F81h
portc	equ	0F82h
portd	equ	0F83h
porte	equ	0F84h
lata	equ	0F89h
latb	equ	0F8Ah
latc	equ	0F8Bh
latd	equ	0F8Ch
late	equ	0F8Dh
trisa	equ	0F92h
trisb	equ	0F93h
trisc	equ	0F94h
trisd	equ	0F95h
trise	equ	0F96h
pie1	equ	0F9Dh
pir1	equ	0F9Eh
ipr1	equ	0F9Fh
pie2	equ	0FA0h
pir2	equ	0FA1h
ipr2	equ	0FA2h
t3con	equ	0FB1h
tmr3l	equ	0FB2h
tmr3h	equ	0FB3h
ccp1con	equ	0FBDh
ccpr1l	equ	0FBEh
ccpr1h	equ	0FBFh
adcon1	equ	0FC1h
adcon0	equ	0FC2h
adresl	equ	0FC3h
adresh	equ	0FC4h
sspcon2	equ	0FC5h
sspcon1	equ	0FC6h
sspstat	equ	0FC7h
sspadd	equ	0FC8h
sspbuf	equ	0FC9h
t2con	equ	0FCAh
pr2	equ	0FCBh
tmr2	equ	0FCCh
t1con	equ	0FCDh
tmr1l	equ	0FCEh
tmr1h	equ	0FCFh
rcon	equ	0FD0h
wdtcon	equ	0FD1h
lvdcon	equ	0FD2h
osccon	equ	0FD3h
t0con	equ	0FD5h
tmr0l	equ	0FD6h
tmr0h	equ	0FD7h
status	equ	0FD8h
fsr2	equ	0FD9h
fsr2l	equ	0FD9h
fsr2h	equ	0FDAh
plusw2	equ	0FDBh
preinc2	equ	0FDCh
postdec2	equ	0FDDh
postinc2	equ	0FDEh
indf2	equ	0FDFh
bsr	equ	0FE0h
fsr1	equ	0FE1h
fsr1l	equ	0FE1h
fsr1h	equ	0FE2h
plusw1	equ	0FE3h
preinc1	equ	0FE4h
postdec1	equ	0FE5h
postinc1	equ	0FE6h
indf1	equ	0FE7h
wreg	equ	0FE8h
fsr0	equ	0FE9h
fsr0l	equ	0FE9h
fsr0h	equ	0FEAh
plusw0	equ	0FEBh
preinc0	equ	0FECh
postdec0	equ	0FEDh
postinc0	equ	0FEEh
indf0	equ	0FEFh
intcon3	equ	0FF0h
intcon2	equ	0FF1h
intcon	equ	0FF2h
prod	equ	0FF3h
prodl	equ	0FF3h
prodh	equ	0FF4h
tablat	equ	0FF5h
tblptr	equ	0FF6h
tblptrl	equ	0FF6h
tblptrh	equ	0FF7h
tblptru	equ	0FF8h
pcl	equ	0FF9h
pclat	equ	0FFAh
pclath	equ	0FFAh
pclatu	equ	0FFBh
stkptr	equ	0FFCh
tosl	equ	0FFDh
tosh	equ	0FFEh
tosu	equ	0FFFh
clrc   macro
	bcf	status,0
endm
setc   macro
	bsf	status,0
endm
clrz   macro
	bcf	status,2
endm
setz   macro
	bsf	status,2
endm
skipnz macro
	btfsc	status,2
endm
skipz  macro
	btfss	status,2
endm
skipnc macro
	btfsc	status,0
endm
skipc  macro
	btfss	status,0
endm
pushw macro
	movwf postinc1
endm
pushf macro arg1
	movff arg1, postinc1
endm
popw macro
	movf postdec1,f
	movf indf1,w
endm
popf macro arg1
	movf postdec1,f
	movff indf1,arg1
endm
popfc macro arg1
	movff plusw1,arg1
	decfsz fsr1,f
endm
	global	__ramtop
	global	__accesstop
# 51 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELA equ 0F38h ;# 
# 95 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELB equ 0F39h ;# 
# 144 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELC equ 0F3Ah ;# 
# 194 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELD equ 0F3Bh ;# 
# 255 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELE equ 0F3Ch ;# 
# 286 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PMD2 equ 0F3Dh ;# 
# 323 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PMD1 equ 0F3Eh ;# 
# 387 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PMD0 equ 0F3Fh ;# 
# 466 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
VREFCON2 equ 0F40h ;# 
# 471 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DACCON1 equ 0F40h ;# 
# 567 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
VREFCON1 equ 0F41h ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DACCON0 equ 0F41h ;# 
# 686 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
VREFCON0 equ 0F42h ;# 
# 691 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FVRCON equ 0F42h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUICON equ 0F43h ;# 
# 784 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUICONH equ 0F43h ;# 
# 928 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUCONL equ 0F44h ;# 
# 933 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUCON1 equ 0F44h ;# 
# 1081 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUCONH equ 0F45h ;# 
# 1086 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUCON0 equ 0F45h ;# 
# 1192 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SRCON1 equ 0F46h ;# 
# 1253 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SRCON0 equ 0F47h ;# 
# 1323 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPTMRS1 equ 0F48h ;# 
# 1374 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPTMRS0 equ 0F49h ;# 
# 1447 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T6CON equ 0F4Ah ;# 
# 1517 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PR6 equ 0F4Bh ;# 
# 1536 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR6 equ 0F4Ch ;# 
# 1555 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T5GCON equ 0F4Dh ;# 
# 1649 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T5CON equ 0F4Eh ;# 
# 1759 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR5 equ 0F4Fh ;# 
# 1765 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR5L equ 0F4Fh ;# 
# 1784 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR5H equ 0F50h ;# 
# 1803 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T4CON equ 0F51h ;# 
# 1873 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PR4 equ 0F52h ;# 
# 1892 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR4 equ 0F53h ;# 
# 1911 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP5CON equ 0F54h ;# 
# 1974 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR5 equ 0F55h ;# 
# 1980 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR5L equ 0F55h ;# 
# 1999 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR5H equ 0F56h ;# 
# 2018 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP4CON equ 0F57h ;# 
# 2081 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR4 equ 0F58h ;# 
# 2087 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR4L equ 0F58h ;# 
# 2106 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR4H equ 0F59h ;# 
# 2125 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PSTR3CON equ 0F5Ah ;# 
# 2212 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ECCP3AS equ 0F5Bh ;# 
# 2217 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP3AS equ 0F5Bh ;# 
# 2453 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PWM3CON equ 0F5Ch ;# 
# 2522 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP3CON equ 0F5Dh ;# 
# 2603 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR3 equ 0F5Eh ;# 
# 2609 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR3L equ 0F5Eh ;# 
# 2628 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR3H equ 0F5Fh ;# 
# 2647 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SLRCON equ 0F60h ;# 
# 2690 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
WPUB equ 0F61h ;# 
# 2751 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IOCB equ 0F62h ;# 
# 2789 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PSTR2CON equ 0F63h ;# 
# 2964 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ECCP2AS equ 0F64h ;# 
# 2969 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP2AS equ 0F64h ;# 
# 3205 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PWM2CON equ 0F65h ;# 
# 3274 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP2CON equ 0F66h ;# 
# 3355 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR2 equ 0F67h ;# 
# 3361 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR2L equ 0F67h ;# 
# 3380 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR2H equ 0F68h ;# 
# 3399 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2CON3 equ 0F69h ;# 
# 3460 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2MSK equ 0F6Ah ;# 
# 3521 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2CON2 equ 0F6Bh ;# 
# 3698 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2CON1 equ 0F6Ch ;# 
# 3838 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2STAT equ 0F6Dh ;# 
# 4230 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2ADD equ 0F6Eh ;# 
# 4320 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2BUF equ 0F6Fh ;# 
# 4339 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUDCON2 equ 0F70h ;# 
# 4344 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUD2CON equ 0F70h ;# 
# 4646 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCSTA2 equ 0F71h ;# 
# 4651 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RC2STA equ 0F71h ;# 
# 4937 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXSTA2 equ 0F72h ;# 
# 4942 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TX2STA equ 0F72h ;# 
# 5192 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXREG2 equ 0F73h ;# 
# 5197 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TX2REG equ 0F73h ;# 
# 5229 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCREG2 equ 0F74h ;# 
# 5234 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RC2REG equ 0F74h ;# 
# 5266 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRG2 equ 0F75h ;# 
# 5271 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SP2BRG equ 0F75h ;# 
# 5303 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRGH2 equ 0F76h ;# 
# 5308 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SP2BRGH equ 0F76h ;# 
# 5340 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM2CON1 equ 0F77h ;# 
# 5345 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM12CON equ 0F77h ;# 
# 5461 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM2CON0 equ 0F78h ;# 
# 5466 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM2CON equ 0F78h ;# 
# 5740 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM1CON0 equ 0F79h ;# 
# 5745 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM1CON equ 0F79h ;# 
# 6161 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE4 equ 0F7Ah ;# 
# 6192 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR4 equ 0F7Bh ;# 
# 6223 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR4 equ 0F7Ch ;# 
# 6262 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE5 equ 0F7Dh ;# 
# 6293 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR5 equ 0F7Eh ;# 
# 6324 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR5 equ 0F7Fh ;# 
# 6372 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTA equ 0F80h ;# 
# 6658 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTB equ 0F81h ;# 
# 6927 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTC equ 0F82h ;# 
# 7236 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTD equ 0F83h ;# 
# 7478 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTE equ 0F84h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATA equ 0F89h ;# 
# 7802 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATB equ 0F8Ah ;# 
# 7934 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATC equ 0F8Bh ;# 
# 8066 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATD equ 0F8Ch ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATE equ 0F8Dh ;# 
# 8255 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISA equ 0F92h ;# 
# 8260 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRA equ 0F92h ;# 
# 8476 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISB equ 0F93h ;# 
# 8481 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRB equ 0F93h ;# 
# 8697 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISC equ 0F94h ;# 
# 8702 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRC equ 0F94h ;# 
# 8918 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISD equ 0F95h ;# 
# 8923 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRD equ 0F95h ;# 
# 9139 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISE equ 0F96h ;# 
# 9144 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRE equ 0F96h ;# 
# 9254 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
OSCTUNE equ 0F9Bh ;# 
# 9323 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
HLVDCON equ 0F9Ch ;# 
# 9328 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LVDCON equ 0F9Ch ;# 
# 9602 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE1 equ 0F9Dh ;# 
# 9678 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR1 equ 0F9Eh ;# 
# 9754 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR1 equ 0F9Fh ;# 
# 9830 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE2 equ 0FA0h ;# 
# 9915 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR2 equ 0FA1h ;# 
# 10000 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR2 equ 0FA2h ;# 
# 10085 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE3 equ 0FA3h ;# 
# 10208 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR3 equ 0FA4h ;# 
# 10287 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR3 equ 0FA5h ;# 
# 10366 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EECON1 equ 0FA6h ;# 
# 10431 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EECON2 equ 0FA7h ;# 
# 10450 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EEDATA equ 0FA8h ;# 
# 10469 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EEADR equ 0FA9h ;# 
# 10538 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EEADRH equ 0FAAh ;# 
# 10571 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCSTA1 equ 0FABh ;# 
# 10576 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCSTA equ 0FABh ;# 
# 10580 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RC1STA equ 0FABh ;# 
# 11032 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXSTA1 equ 0FACh ;# 
# 11037 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXSTA equ 0FACh ;# 
# 11041 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TX1STA equ 0FACh ;# 
# 11412 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXREG1 equ 0FADh ;# 
# 11417 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXREG equ 0FADh ;# 
# 11421 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TX1REG equ 0FADh ;# 
# 11489 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCREG1 equ 0FAEh ;# 
# 11494 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCREG equ 0FAEh ;# 
# 11498 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RC1REG equ 0FAEh ;# 
# 11566 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRG1 equ 0FAFh ;# 
# 11571 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRG equ 0FAFh ;# 
# 11575 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SP1BRG equ 0FAFh ;# 
# 11643 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRGH1 equ 0FB0h ;# 
# 11648 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRGH equ 0FB0h ;# 
# 11652 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SP1BRGH equ 0FB0h ;# 
# 11720 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T3CON equ 0FB1h ;# 
# 11829 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR3 equ 0FB2h ;# 
# 11835 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR3L equ 0FB2h ;# 
# 11854 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR3H equ 0FB3h ;# 
# 11873 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T3GCON equ 0FB4h ;# 
# 11967 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ECCP1AS equ 0FB6h ;# 
# 11972 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ECCPAS equ 0FB6h ;# 
# 12348 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PWM1CON equ 0FB7h ;# 
# 12353 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PWMCON equ 0FB7h ;# 
# 12601 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUDCON1 equ 0FB8h ;# 
# 12606 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUDCON equ 0FB8h ;# 
# 12610 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUDCTL equ 0FB8h ;# 
# 12614 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUD1CON equ 0FB8h ;# 
# 13390 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PSTR1CON equ 0FB9h ;# 
# 13395 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PSTRCON equ 0FB9h ;# 
# 13563 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T2CON equ 0FBAh ;# 
# 13633 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PR2 equ 0FBBh ;# 
# 13652 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR2 equ 0FBCh ;# 
# 13671 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP1CON equ 0FBDh ;# 
# 13752 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR1 equ 0FBEh ;# 
# 13758 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR1L equ 0FBEh ;# 
# 13777 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR1H equ 0FBFh ;# 
# 13796 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADCON2 equ 0FC0h ;# 
# 13866 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADCON1 equ 0FC1h ;# 
# 13933 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADCON0 equ 0FC2h ;# 
# 14057 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADRES equ 0FC3h ;# 
# 14063 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADRESL equ 0FC3h ;# 
# 14082 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADRESH equ 0FC4h ;# 
# 14101 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1CON2 equ 0FC5h ;# 
# 14106 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPCON2 equ 0FC5h ;# 
# 14544 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1CON1 equ 0FC6h ;# 
# 14549 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPCON1 equ 0FC6h ;# 
# 14823 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1STAT equ 0FC7h ;# 
# 14828 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPSTAT equ 0FC7h ;# 
# 15534 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1ADD equ 0FC8h ;# 
# 15539 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPADD equ 0FC8h ;# 
# 15871 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1BUF equ 0FC9h ;# 
# 15876 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPBUF equ 0FC9h ;# 
# 15924 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1MSK equ 0FCAh ;# 
# 15929 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPMSK equ 0FCAh ;# 
# 16045 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1CON3 equ 0FCBh ;# 
# 16050 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPCON3 equ 0FCBh ;# 
# 16166 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T1GCON equ 0FCCh ;# 
# 16260 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T1CON equ 0FCDh ;# 
# 16372 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR1 equ 0FCEh ;# 
# 16378 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR1L equ 0FCEh ;# 
# 16397 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR1H equ 0FCFh ;# 
# 16416 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCON equ 0FD0h ;# 
# 16548 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
WDTCON equ 0FD1h ;# 
# 16575 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
OSCCON2 equ 0FD2h ;# 
# 16631 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
OSCCON equ 0FD3h ;# 
# 16713 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T0CON equ 0FD5h ;# 
# 16782 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR0 equ 0FD6h ;# 
# 16788 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR0L equ 0FD6h ;# 
# 16807 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR0H equ 0FD7h ;# 
# 16826 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
STATUS equ 0FD8h ;# 
# 16904 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR2 equ 0FD9h ;# 
# 16910 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR2L equ 0FD9h ;# 
# 16929 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR2H equ 0FDAh ;# 
# 16935 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PLUSW2 equ 0FDBh ;# 
# 16954 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PREINC2 equ 0FDCh ;# 
# 16973 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTDEC2 equ 0FDDh ;# 
# 16992 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTINC2 equ 0FDEh ;# 
# 17011 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INDF2 equ 0FDFh ;# 
# 17030 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BSR equ 0FE0h ;# 
# 17036 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR1 equ 0FE1h ;# 
# 17042 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR1L equ 0FE1h ;# 
# 17061 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR1H equ 0FE2h ;# 
# 17067 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PLUSW1 equ 0FE3h ;# 
# 17086 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PREINC1 equ 0FE4h ;# 
# 17105 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTDEC1 equ 0FE5h ;# 
# 17124 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTINC1 equ 0FE6h ;# 
# 17143 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INDF1 equ 0FE7h ;# 
# 17162 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
WREG equ 0FE8h ;# 
# 17199 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR0 equ 0FE9h ;# 
# 17205 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR0L equ 0FE9h ;# 
# 17224 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR0H equ 0FEAh ;# 
# 17230 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PLUSW0 equ 0FEBh ;# 
# 17249 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PREINC0 equ 0FECh ;# 
# 17268 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTDEC0 equ 0FEDh ;# 
# 17287 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTINC0 equ 0FEEh ;# 
# 17306 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INDF0 equ 0FEFh ;# 
# 17325 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INTCON3 equ 0FF0h ;# 
# 17416 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INTCON2 equ 0FF1h ;# 
# 17485 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INTCON equ 0FF2h ;# 
# 17601 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PROD equ 0FF3h ;# 
# 17607 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PRODL equ 0FF3h ;# 
# 17626 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PRODH equ 0FF4h ;# 
# 17645 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TABLAT equ 0FF5h ;# 
# 17666 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TBLPTR equ 0FF6h ;# 
# 17672 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TBLPTRL equ 0FF6h ;# 
# 17691 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TBLPTRH equ 0FF7h ;# 
# 17710 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TBLPTRU equ 0FF8h ;# 
# 17740 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PCLAT equ 0FF9h ;# 
# 17747 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PC equ 0FF9h ;# 
# 17753 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PCL equ 0FF9h ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PCLATH equ 0FFAh ;# 
# 17791 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PCLATU equ 0FFBh ;# 
# 17797 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
STKPTR equ 0FFCh ;# 
# 17902 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TOS equ 0FFDh ;# 
# 17908 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TOSL equ 0FFDh ;# 
# 17927 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TOSH equ 0FFEh ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TOSU equ 0FFFh ;# 
# 51 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELA equ 0F38h ;# 
# 95 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELB equ 0F39h ;# 
# 144 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELC equ 0F3Ah ;# 
# 194 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELD equ 0F3Bh ;# 
# 255 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELE equ 0F3Ch ;# 
# 286 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PMD2 equ 0F3Dh ;# 
# 323 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PMD1 equ 0F3Eh ;# 
# 387 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PMD0 equ 0F3Fh ;# 
# 466 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
VREFCON2 equ 0F40h ;# 
# 471 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DACCON1 equ 0F40h ;# 
# 567 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
VREFCON1 equ 0F41h ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DACCON0 equ 0F41h ;# 
# 686 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
VREFCON0 equ 0F42h ;# 
# 691 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FVRCON equ 0F42h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUICON equ 0F43h ;# 
# 784 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUICONH equ 0F43h ;# 
# 928 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUCONL equ 0F44h ;# 
# 933 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUCON1 equ 0F44h ;# 
# 1081 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUCONH equ 0F45h ;# 
# 1086 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUCON0 equ 0F45h ;# 
# 1192 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SRCON1 equ 0F46h ;# 
# 1253 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SRCON0 equ 0F47h ;# 
# 1323 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPTMRS1 equ 0F48h ;# 
# 1374 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPTMRS0 equ 0F49h ;# 
# 1447 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T6CON equ 0F4Ah ;# 
# 1517 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PR6 equ 0F4Bh ;# 
# 1536 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR6 equ 0F4Ch ;# 
# 1555 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T5GCON equ 0F4Dh ;# 
# 1649 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T5CON equ 0F4Eh ;# 
# 1759 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR5 equ 0F4Fh ;# 
# 1765 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR5L equ 0F4Fh ;# 
# 1784 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR5H equ 0F50h ;# 
# 1803 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T4CON equ 0F51h ;# 
# 1873 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PR4 equ 0F52h ;# 
# 1892 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR4 equ 0F53h ;# 
# 1911 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP5CON equ 0F54h ;# 
# 1974 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR5 equ 0F55h ;# 
# 1980 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR5L equ 0F55h ;# 
# 1999 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR5H equ 0F56h ;# 
# 2018 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP4CON equ 0F57h ;# 
# 2081 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR4 equ 0F58h ;# 
# 2087 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR4L equ 0F58h ;# 
# 2106 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR4H equ 0F59h ;# 
# 2125 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PSTR3CON equ 0F5Ah ;# 
# 2212 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ECCP3AS equ 0F5Bh ;# 
# 2217 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP3AS equ 0F5Bh ;# 
# 2453 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PWM3CON equ 0F5Ch ;# 
# 2522 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP3CON equ 0F5Dh ;# 
# 2603 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR3 equ 0F5Eh ;# 
# 2609 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR3L equ 0F5Eh ;# 
# 2628 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR3H equ 0F5Fh ;# 
# 2647 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SLRCON equ 0F60h ;# 
# 2690 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
WPUB equ 0F61h ;# 
# 2751 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IOCB equ 0F62h ;# 
# 2789 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PSTR2CON equ 0F63h ;# 
# 2964 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ECCP2AS equ 0F64h ;# 
# 2969 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP2AS equ 0F64h ;# 
# 3205 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PWM2CON equ 0F65h ;# 
# 3274 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP2CON equ 0F66h ;# 
# 3355 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR2 equ 0F67h ;# 
# 3361 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR2L equ 0F67h ;# 
# 3380 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR2H equ 0F68h ;# 
# 3399 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2CON3 equ 0F69h ;# 
# 3460 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2MSK equ 0F6Ah ;# 
# 3521 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2CON2 equ 0F6Bh ;# 
# 3698 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2CON1 equ 0F6Ch ;# 
# 3838 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2STAT equ 0F6Dh ;# 
# 4230 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2ADD equ 0F6Eh ;# 
# 4320 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2BUF equ 0F6Fh ;# 
# 4339 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUDCON2 equ 0F70h ;# 
# 4344 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUD2CON equ 0F70h ;# 
# 4646 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCSTA2 equ 0F71h ;# 
# 4651 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RC2STA equ 0F71h ;# 
# 4937 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXSTA2 equ 0F72h ;# 
# 4942 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TX2STA equ 0F72h ;# 
# 5192 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXREG2 equ 0F73h ;# 
# 5197 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TX2REG equ 0F73h ;# 
# 5229 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCREG2 equ 0F74h ;# 
# 5234 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RC2REG equ 0F74h ;# 
# 5266 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRG2 equ 0F75h ;# 
# 5271 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SP2BRG equ 0F75h ;# 
# 5303 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRGH2 equ 0F76h ;# 
# 5308 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SP2BRGH equ 0F76h ;# 
# 5340 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM2CON1 equ 0F77h ;# 
# 5345 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM12CON equ 0F77h ;# 
# 5461 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM2CON0 equ 0F78h ;# 
# 5466 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM2CON equ 0F78h ;# 
# 5740 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM1CON0 equ 0F79h ;# 
# 5745 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM1CON equ 0F79h ;# 
# 6161 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE4 equ 0F7Ah ;# 
# 6192 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR4 equ 0F7Bh ;# 
# 6223 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR4 equ 0F7Ch ;# 
# 6262 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE5 equ 0F7Dh ;# 
# 6293 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR5 equ 0F7Eh ;# 
# 6324 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR5 equ 0F7Fh ;# 
# 6372 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTA equ 0F80h ;# 
# 6658 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTB equ 0F81h ;# 
# 6927 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTC equ 0F82h ;# 
# 7236 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTD equ 0F83h ;# 
# 7478 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTE equ 0F84h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATA equ 0F89h ;# 
# 7802 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATB equ 0F8Ah ;# 
# 7934 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATC equ 0F8Bh ;# 
# 8066 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATD equ 0F8Ch ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATE equ 0F8Dh ;# 
# 8255 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISA equ 0F92h ;# 
# 8260 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRA equ 0F92h ;# 
# 8476 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISB equ 0F93h ;# 
# 8481 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRB equ 0F93h ;# 
# 8697 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISC equ 0F94h ;# 
# 8702 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRC equ 0F94h ;# 
# 8918 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISD equ 0F95h ;# 
# 8923 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRD equ 0F95h ;# 
# 9139 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISE equ 0F96h ;# 
# 9144 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRE equ 0F96h ;# 
# 9254 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
OSCTUNE equ 0F9Bh ;# 
# 9323 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
HLVDCON equ 0F9Ch ;# 
# 9328 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LVDCON equ 0F9Ch ;# 
# 9602 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE1 equ 0F9Dh ;# 
# 9678 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR1 equ 0F9Eh ;# 
# 9754 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR1 equ 0F9Fh ;# 
# 9830 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE2 equ 0FA0h ;# 
# 9915 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR2 equ 0FA1h ;# 
# 10000 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR2 equ 0FA2h ;# 
# 10085 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE3 equ 0FA3h ;# 
# 10208 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR3 equ 0FA4h ;# 
# 10287 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR3 equ 0FA5h ;# 
# 10366 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EECON1 equ 0FA6h ;# 
# 10431 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EECON2 equ 0FA7h ;# 
# 10450 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EEDATA equ 0FA8h ;# 
# 10469 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EEADR equ 0FA9h ;# 
# 10538 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EEADRH equ 0FAAh ;# 
# 10571 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCSTA1 equ 0FABh ;# 
# 10576 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCSTA equ 0FABh ;# 
# 10580 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RC1STA equ 0FABh ;# 
# 11032 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXSTA1 equ 0FACh ;# 
# 11037 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXSTA equ 0FACh ;# 
# 11041 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TX1STA equ 0FACh ;# 
# 11412 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXREG1 equ 0FADh ;# 
# 11417 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXREG equ 0FADh ;# 
# 11421 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TX1REG equ 0FADh ;# 
# 11489 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCREG1 equ 0FAEh ;# 
# 11494 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCREG equ 0FAEh ;# 
# 11498 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RC1REG equ 0FAEh ;# 
# 11566 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRG1 equ 0FAFh ;# 
# 11571 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRG equ 0FAFh ;# 
# 11575 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SP1BRG equ 0FAFh ;# 
# 11643 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRGH1 equ 0FB0h ;# 
# 11648 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRGH equ 0FB0h ;# 
# 11652 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SP1BRGH equ 0FB0h ;# 
# 11720 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T3CON equ 0FB1h ;# 
# 11829 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR3 equ 0FB2h ;# 
# 11835 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR3L equ 0FB2h ;# 
# 11854 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR3H equ 0FB3h ;# 
# 11873 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T3GCON equ 0FB4h ;# 
# 11967 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ECCP1AS equ 0FB6h ;# 
# 11972 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ECCPAS equ 0FB6h ;# 
# 12348 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PWM1CON equ 0FB7h ;# 
# 12353 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PWMCON equ 0FB7h ;# 
# 12601 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUDCON1 equ 0FB8h ;# 
# 12606 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUDCON equ 0FB8h ;# 
# 12610 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUDCTL equ 0FB8h ;# 
# 12614 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUD1CON equ 0FB8h ;# 
# 13390 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PSTR1CON equ 0FB9h ;# 
# 13395 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PSTRCON equ 0FB9h ;# 
# 13563 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T2CON equ 0FBAh ;# 
# 13633 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PR2 equ 0FBBh ;# 
# 13652 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR2 equ 0FBCh ;# 
# 13671 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP1CON equ 0FBDh ;# 
# 13752 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR1 equ 0FBEh ;# 
# 13758 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR1L equ 0FBEh ;# 
# 13777 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR1H equ 0FBFh ;# 
# 13796 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADCON2 equ 0FC0h ;# 
# 13866 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADCON1 equ 0FC1h ;# 
# 13933 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADCON0 equ 0FC2h ;# 
# 14057 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADRES equ 0FC3h ;# 
# 14063 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADRESL equ 0FC3h ;# 
# 14082 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADRESH equ 0FC4h ;# 
# 14101 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1CON2 equ 0FC5h ;# 
# 14106 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPCON2 equ 0FC5h ;# 
# 14544 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1CON1 equ 0FC6h ;# 
# 14549 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPCON1 equ 0FC6h ;# 
# 14823 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1STAT equ 0FC7h ;# 
# 14828 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPSTAT equ 0FC7h ;# 
# 15534 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1ADD equ 0FC8h ;# 
# 15539 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPADD equ 0FC8h ;# 
# 15871 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1BUF equ 0FC9h ;# 
# 15876 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPBUF equ 0FC9h ;# 
# 15924 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1MSK equ 0FCAh ;# 
# 15929 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPMSK equ 0FCAh ;# 
# 16045 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1CON3 equ 0FCBh ;# 
# 16050 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPCON3 equ 0FCBh ;# 
# 16166 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T1GCON equ 0FCCh ;# 
# 16260 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T1CON equ 0FCDh ;# 
# 16372 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR1 equ 0FCEh ;# 
# 16378 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR1L equ 0FCEh ;# 
# 16397 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR1H equ 0FCFh ;# 
# 16416 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCON equ 0FD0h ;# 
# 16548 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
WDTCON equ 0FD1h ;# 
# 16575 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
OSCCON2 equ 0FD2h ;# 
# 16631 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
OSCCON equ 0FD3h ;# 
# 16713 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T0CON equ 0FD5h ;# 
# 16782 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR0 equ 0FD6h ;# 
# 16788 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR0L equ 0FD6h ;# 
# 16807 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR0H equ 0FD7h ;# 
# 16826 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
STATUS equ 0FD8h ;# 
# 16904 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR2 equ 0FD9h ;# 
# 16910 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR2L equ 0FD9h ;# 
# 16929 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR2H equ 0FDAh ;# 
# 16935 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PLUSW2 equ 0FDBh ;# 
# 16954 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PREINC2 equ 0FDCh ;# 
# 16973 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTDEC2 equ 0FDDh ;# 
# 16992 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTINC2 equ 0FDEh ;# 
# 17011 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INDF2 equ 0FDFh ;# 
# 17030 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BSR equ 0FE0h ;# 
# 17036 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR1 equ 0FE1h ;# 
# 17042 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR1L equ 0FE1h ;# 
# 17061 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR1H equ 0FE2h ;# 
# 17067 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PLUSW1 equ 0FE3h ;# 
# 17086 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PREINC1 equ 0FE4h ;# 
# 17105 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTDEC1 equ 0FE5h ;# 
# 17124 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTINC1 equ 0FE6h ;# 
# 17143 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INDF1 equ 0FE7h ;# 
# 17162 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
WREG equ 0FE8h ;# 
# 17199 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR0 equ 0FE9h ;# 
# 17205 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR0L equ 0FE9h ;# 
# 17224 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR0H equ 0FEAh ;# 
# 17230 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PLUSW0 equ 0FEBh ;# 
# 17249 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PREINC0 equ 0FECh ;# 
# 17268 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTDEC0 equ 0FEDh ;# 
# 17287 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTINC0 equ 0FEEh ;# 
# 17306 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INDF0 equ 0FEFh ;# 
# 17325 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INTCON3 equ 0FF0h ;# 
# 17416 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INTCON2 equ 0FF1h ;# 
# 17485 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INTCON equ 0FF2h ;# 
# 17601 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PROD equ 0FF3h ;# 
# 17607 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PRODL equ 0FF3h ;# 
# 17626 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PRODH equ 0FF4h ;# 
# 17645 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TABLAT equ 0FF5h ;# 
# 17666 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TBLPTR equ 0FF6h ;# 
# 17672 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TBLPTRL equ 0FF6h ;# 
# 17691 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TBLPTRH equ 0FF7h ;# 
# 17710 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TBLPTRU equ 0FF8h ;# 
# 17740 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PCLAT equ 0FF9h ;# 
# 17747 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PC equ 0FF9h ;# 
# 17753 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PCL equ 0FF9h ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PCLATH equ 0FFAh ;# 
# 17791 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PCLATU equ 0FFBh ;# 
# 17797 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
STKPTR equ 0FFCh ;# 
# 17902 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TOS equ 0FFDh ;# 
# 17908 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TOSL equ 0FFDh ;# 
# 17927 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TOSH equ 0FFEh ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TOSU equ 0FFFh ;# 
# 51 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELA equ 0F38h ;# 
# 95 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELB equ 0F39h ;# 
# 144 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELC equ 0F3Ah ;# 
# 194 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELD equ 0F3Bh ;# 
# 255 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ANSELE equ 0F3Ch ;# 
# 286 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PMD2 equ 0F3Dh ;# 
# 323 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PMD1 equ 0F3Eh ;# 
# 387 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PMD0 equ 0F3Fh ;# 
# 466 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
VREFCON2 equ 0F40h ;# 
# 471 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DACCON1 equ 0F40h ;# 
# 567 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
VREFCON1 equ 0F41h ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DACCON0 equ 0F41h ;# 
# 686 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
VREFCON0 equ 0F42h ;# 
# 691 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FVRCON equ 0F42h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUICON equ 0F43h ;# 
# 784 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUICONH equ 0F43h ;# 
# 928 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUCONL equ 0F44h ;# 
# 933 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUCON1 equ 0F44h ;# 
# 1081 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUCONH equ 0F45h ;# 
# 1086 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CTMUCON0 equ 0F45h ;# 
# 1192 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SRCON1 equ 0F46h ;# 
# 1253 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SRCON0 equ 0F47h ;# 
# 1323 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPTMRS1 equ 0F48h ;# 
# 1374 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPTMRS0 equ 0F49h ;# 
# 1447 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T6CON equ 0F4Ah ;# 
# 1517 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PR6 equ 0F4Bh ;# 
# 1536 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR6 equ 0F4Ch ;# 
# 1555 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T5GCON equ 0F4Dh ;# 
# 1649 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T5CON equ 0F4Eh ;# 
# 1759 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR5 equ 0F4Fh ;# 
# 1765 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR5L equ 0F4Fh ;# 
# 1784 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR5H equ 0F50h ;# 
# 1803 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T4CON equ 0F51h ;# 
# 1873 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PR4 equ 0F52h ;# 
# 1892 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR4 equ 0F53h ;# 
# 1911 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP5CON equ 0F54h ;# 
# 1974 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR5 equ 0F55h ;# 
# 1980 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR5L equ 0F55h ;# 
# 1999 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR5H equ 0F56h ;# 
# 2018 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP4CON equ 0F57h ;# 
# 2081 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR4 equ 0F58h ;# 
# 2087 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR4L equ 0F58h ;# 
# 2106 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR4H equ 0F59h ;# 
# 2125 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PSTR3CON equ 0F5Ah ;# 
# 2212 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ECCP3AS equ 0F5Bh ;# 
# 2217 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP3AS equ 0F5Bh ;# 
# 2453 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PWM3CON equ 0F5Ch ;# 
# 2522 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP3CON equ 0F5Dh ;# 
# 2603 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR3 equ 0F5Eh ;# 
# 2609 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR3L equ 0F5Eh ;# 
# 2628 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR3H equ 0F5Fh ;# 
# 2647 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SLRCON equ 0F60h ;# 
# 2690 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
WPUB equ 0F61h ;# 
# 2751 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IOCB equ 0F62h ;# 
# 2789 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PSTR2CON equ 0F63h ;# 
# 2964 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ECCP2AS equ 0F64h ;# 
# 2969 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP2AS equ 0F64h ;# 
# 3205 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PWM2CON equ 0F65h ;# 
# 3274 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP2CON equ 0F66h ;# 
# 3355 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR2 equ 0F67h ;# 
# 3361 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR2L equ 0F67h ;# 
# 3380 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR2H equ 0F68h ;# 
# 3399 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2CON3 equ 0F69h ;# 
# 3460 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2MSK equ 0F6Ah ;# 
# 3521 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2CON2 equ 0F6Bh ;# 
# 3698 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2CON1 equ 0F6Ch ;# 
# 3838 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2STAT equ 0F6Dh ;# 
# 4230 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2ADD equ 0F6Eh ;# 
# 4320 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP2BUF equ 0F6Fh ;# 
# 4339 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUDCON2 equ 0F70h ;# 
# 4344 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUD2CON equ 0F70h ;# 
# 4646 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCSTA2 equ 0F71h ;# 
# 4651 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RC2STA equ 0F71h ;# 
# 4937 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXSTA2 equ 0F72h ;# 
# 4942 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TX2STA equ 0F72h ;# 
# 5192 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXREG2 equ 0F73h ;# 
# 5197 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TX2REG equ 0F73h ;# 
# 5229 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCREG2 equ 0F74h ;# 
# 5234 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RC2REG equ 0F74h ;# 
# 5266 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRG2 equ 0F75h ;# 
# 5271 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SP2BRG equ 0F75h ;# 
# 5303 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRGH2 equ 0F76h ;# 
# 5308 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SP2BRGH equ 0F76h ;# 
# 5340 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM2CON1 equ 0F77h ;# 
# 5345 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM12CON equ 0F77h ;# 
# 5461 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM2CON0 equ 0F78h ;# 
# 5466 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM2CON equ 0F78h ;# 
# 5740 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM1CON0 equ 0F79h ;# 
# 5745 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CM1CON equ 0F79h ;# 
# 6161 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE4 equ 0F7Ah ;# 
# 6192 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR4 equ 0F7Bh ;# 
# 6223 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR4 equ 0F7Ch ;# 
# 6262 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE5 equ 0F7Dh ;# 
# 6293 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR5 equ 0F7Eh ;# 
# 6324 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR5 equ 0F7Fh ;# 
# 6372 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTA equ 0F80h ;# 
# 6658 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTB equ 0F81h ;# 
# 6927 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTC equ 0F82h ;# 
# 7236 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTD equ 0F83h ;# 
# 7478 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PORTE equ 0F84h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATA equ 0F89h ;# 
# 7802 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATB equ 0F8Ah ;# 
# 7934 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATC equ 0F8Bh ;# 
# 8066 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATD equ 0F8Ch ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LATE equ 0F8Dh ;# 
# 8255 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISA equ 0F92h ;# 
# 8260 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRA equ 0F92h ;# 
# 8476 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISB equ 0F93h ;# 
# 8481 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRB equ 0F93h ;# 
# 8697 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISC equ 0F94h ;# 
# 8702 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRC equ 0F94h ;# 
# 8918 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISD equ 0F95h ;# 
# 8923 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRD equ 0F95h ;# 
# 9139 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TRISE equ 0F96h ;# 
# 9144 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
DDRE equ 0F96h ;# 
# 9254 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
OSCTUNE equ 0F9Bh ;# 
# 9323 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
HLVDCON equ 0F9Ch ;# 
# 9328 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
LVDCON equ 0F9Ch ;# 
# 9602 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE1 equ 0F9Dh ;# 
# 9678 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR1 equ 0F9Eh ;# 
# 9754 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR1 equ 0F9Fh ;# 
# 9830 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE2 equ 0FA0h ;# 
# 9915 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR2 equ 0FA1h ;# 
# 10000 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR2 equ 0FA2h ;# 
# 10085 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIE3 equ 0FA3h ;# 
# 10208 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PIR3 equ 0FA4h ;# 
# 10287 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
IPR3 equ 0FA5h ;# 
# 10366 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EECON1 equ 0FA6h ;# 
# 10431 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EECON2 equ 0FA7h ;# 
# 10450 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EEDATA equ 0FA8h ;# 
# 10469 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EEADR equ 0FA9h ;# 
# 10538 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
EEADRH equ 0FAAh ;# 
# 10571 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCSTA1 equ 0FABh ;# 
# 10576 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCSTA equ 0FABh ;# 
# 10580 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RC1STA equ 0FABh ;# 
# 11032 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXSTA1 equ 0FACh ;# 
# 11037 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXSTA equ 0FACh ;# 
# 11041 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TX1STA equ 0FACh ;# 
# 11412 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXREG1 equ 0FADh ;# 
# 11417 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TXREG equ 0FADh ;# 
# 11421 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TX1REG equ 0FADh ;# 
# 11489 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCREG1 equ 0FAEh ;# 
# 11494 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCREG equ 0FAEh ;# 
# 11498 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RC1REG equ 0FAEh ;# 
# 11566 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRG1 equ 0FAFh ;# 
# 11571 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRG equ 0FAFh ;# 
# 11575 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SP1BRG equ 0FAFh ;# 
# 11643 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRGH1 equ 0FB0h ;# 
# 11648 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SPBRGH equ 0FB0h ;# 
# 11652 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SP1BRGH equ 0FB0h ;# 
# 11720 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T3CON equ 0FB1h ;# 
# 11829 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR3 equ 0FB2h ;# 
# 11835 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR3L equ 0FB2h ;# 
# 11854 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR3H equ 0FB3h ;# 
# 11873 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T3GCON equ 0FB4h ;# 
# 11967 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ECCP1AS equ 0FB6h ;# 
# 11972 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ECCPAS equ 0FB6h ;# 
# 12348 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PWM1CON equ 0FB7h ;# 
# 12353 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PWMCON equ 0FB7h ;# 
# 12601 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUDCON1 equ 0FB8h ;# 
# 12606 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUDCON equ 0FB8h ;# 
# 12610 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUDCTL equ 0FB8h ;# 
# 12614 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BAUD1CON equ 0FB8h ;# 
# 13390 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PSTR1CON equ 0FB9h ;# 
# 13395 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PSTRCON equ 0FB9h ;# 
# 13563 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T2CON equ 0FBAh ;# 
# 13633 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PR2 equ 0FBBh ;# 
# 13652 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR2 equ 0FBCh ;# 
# 13671 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCP1CON equ 0FBDh ;# 
# 13752 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR1 equ 0FBEh ;# 
# 13758 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR1L equ 0FBEh ;# 
# 13777 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
CCPR1H equ 0FBFh ;# 
# 13796 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADCON2 equ 0FC0h ;# 
# 13866 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADCON1 equ 0FC1h ;# 
# 13933 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADCON0 equ 0FC2h ;# 
# 14057 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADRES equ 0FC3h ;# 
# 14063 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADRESL equ 0FC3h ;# 
# 14082 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
ADRESH equ 0FC4h ;# 
# 14101 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1CON2 equ 0FC5h ;# 
# 14106 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPCON2 equ 0FC5h ;# 
# 14544 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1CON1 equ 0FC6h ;# 
# 14549 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPCON1 equ 0FC6h ;# 
# 14823 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1STAT equ 0FC7h ;# 
# 14828 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPSTAT equ 0FC7h ;# 
# 15534 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1ADD equ 0FC8h ;# 
# 15539 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPADD equ 0FC8h ;# 
# 15871 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1BUF equ 0FC9h ;# 
# 15876 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPBUF equ 0FC9h ;# 
# 15924 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1MSK equ 0FCAh ;# 
# 15929 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPMSK equ 0FCAh ;# 
# 16045 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSP1CON3 equ 0FCBh ;# 
# 16050 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
SSPCON3 equ 0FCBh ;# 
# 16166 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T1GCON equ 0FCCh ;# 
# 16260 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T1CON equ 0FCDh ;# 
# 16372 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR1 equ 0FCEh ;# 
# 16378 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR1L equ 0FCEh ;# 
# 16397 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR1H equ 0FCFh ;# 
# 16416 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
RCON equ 0FD0h ;# 
# 16548 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
WDTCON equ 0FD1h ;# 
# 16575 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
OSCCON2 equ 0FD2h ;# 
# 16631 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
OSCCON equ 0FD3h ;# 
# 16713 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
T0CON equ 0FD5h ;# 
# 16782 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR0 equ 0FD6h ;# 
# 16788 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR0L equ 0FD6h ;# 
# 16807 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TMR0H equ 0FD7h ;# 
# 16826 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
STATUS equ 0FD8h ;# 
# 16904 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR2 equ 0FD9h ;# 
# 16910 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR2L equ 0FD9h ;# 
# 16929 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR2H equ 0FDAh ;# 
# 16935 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PLUSW2 equ 0FDBh ;# 
# 16954 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PREINC2 equ 0FDCh ;# 
# 16973 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTDEC2 equ 0FDDh ;# 
# 16992 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTINC2 equ 0FDEh ;# 
# 17011 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INDF2 equ 0FDFh ;# 
# 17030 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
BSR equ 0FE0h ;# 
# 17036 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR1 equ 0FE1h ;# 
# 17042 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR1L equ 0FE1h ;# 
# 17061 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR1H equ 0FE2h ;# 
# 17067 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PLUSW1 equ 0FE3h ;# 
# 17086 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PREINC1 equ 0FE4h ;# 
# 17105 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTDEC1 equ 0FE5h ;# 
# 17124 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTINC1 equ 0FE6h ;# 
# 17143 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INDF1 equ 0FE7h ;# 
# 17162 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
WREG equ 0FE8h ;# 
# 17199 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR0 equ 0FE9h ;# 
# 17205 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR0L equ 0FE9h ;# 
# 17224 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
FSR0H equ 0FEAh ;# 
# 17230 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PLUSW0 equ 0FEBh ;# 
# 17249 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PREINC0 equ 0FECh ;# 
# 17268 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTDEC0 equ 0FEDh ;# 
# 17287 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
POSTINC0 equ 0FEEh ;# 
# 17306 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INDF0 equ 0FEFh ;# 
# 17325 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INTCON3 equ 0FF0h ;# 
# 17416 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INTCON2 equ 0FF1h ;# 
# 17485 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
INTCON equ 0FF2h ;# 
# 17601 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PROD equ 0FF3h ;# 
# 17607 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PRODL equ 0FF3h ;# 
# 17626 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PRODH equ 0FF4h ;# 
# 17645 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TABLAT equ 0FF5h ;# 
# 17666 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TBLPTR equ 0FF6h ;# 
# 17672 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TBLPTRL equ 0FF6h ;# 
# 17691 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TBLPTRH equ 0FF7h ;# 
# 17710 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TBLPTRU equ 0FF8h ;# 
# 17740 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PCLAT equ 0FF9h ;# 
# 17747 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PC equ 0FF9h ;# 
# 17753 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PCL equ 0FF9h ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PCLATH equ 0FFAh ;# 
# 17791 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
PCLATU equ 0FFBh ;# 
# 17797 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
STKPTR equ 0FFCh ;# 
# 17902 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TOS equ 0FFDh ;# 
# 17908 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TOSL equ 0FFDh ;# 
# 17927 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TOSH equ 0FFEh ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v1.38\include\pic18f46k22.h"
TOSU equ 0FFFh ;# 
	FNCALL	_main,_SYSTEM_Initialize
	FNCALL	_main,_delay_100ms
	FNCALL	_SYSTEM_Initialize,_OSCILLATOR_Initialize
	FNCALL	_SYSTEM_Initialize,_PIN_MANAGER_Initialize
	FNROOT	_main
	global	_ANSELA
_ANSELA	set	0xF38
	global	_ANSELB
_ANSELB	set	0xF39
	global	_ANSELC
_ANSELC	set	0xF3A
	global	_ANSELD
_ANSELD	set	0xF3B
	global	_ANSELE
_ANSELE	set	0xF3C
	global	_INTCON2bits
_INTCON2bits	set	0xFF1
	global	_LATA
_LATA	set	0xF89
	global	_LATB
_LATB	set	0xF8A
	global	_LATBbits
_LATBbits	set	0xF8A
	global	_LATC
_LATC	set	0xF8B
	global	_LATD
_LATD	set	0xF8C
	global	_LATE
_LATE	set	0xF8D
	global	_OSCCON
_OSCCON	set	0xFD3
	global	_OSCCON2
_OSCCON2	set	0xFD2
	global	_OSCTUNE
_OSCTUNE	set	0xF9B
	global	_TRISA
_TRISA	set	0xF92
	global	_TRISB
_TRISB	set	0xF93
	global	_TRISC
_TRISC	set	0xF94
	global	_TRISD
_TRISD	set	0xF95
	global	_TRISE
_TRISE	set	0xF96
	global	_WPUB
_WPUB	set	0xF61
	global	_PLLRDY
_PLLRDY	set	0x7E97
; #config settings
global __CFG_FOSC$HSMP
__CFG_FOSC$HSMP equ 0x0
global __CFG_PLLCFG$ON
__CFG_PLLCFG$ON equ 0x0
global __CFG_PRICLKEN$ON
__CFG_PRICLKEN$ON equ 0x0
global __CFG_FCMEN$OFF
__CFG_FCMEN$OFF equ 0x0
global __CFG_IESO$OFF
__CFG_IESO$OFF equ 0x0
global __CFG_PWRTEN$OFF
__CFG_PWRTEN$OFF equ 0x0
global __CFG_BOREN$SBORDIS
__CFG_BOREN$SBORDIS equ 0x0
global __CFG_BORV$190
__CFG_BORV$190 equ 0x0
global __CFG_WDTEN$OFF
__CFG_WDTEN$OFF equ 0x0
global __CFG_WDTPS$32768
__CFG_WDTPS$32768 equ 0x0
global __CFG_CCP2MX$PORTC1
__CFG_CCP2MX$PORTC1 equ 0x0
global __CFG_PBADEN$ON
__CFG_PBADEN$ON equ 0x0
global __CFG_CCP3MX$PORTB5
__CFG_CCP3MX$PORTB5 equ 0x0
global __CFG_HFOFST$ON
__CFG_HFOFST$ON equ 0x0
global __CFG_T3CMX$PORTC0
__CFG_T3CMX$PORTC0 equ 0x0
global __CFG_P2BMX$PORTD2
__CFG_P2BMX$PORTD2 equ 0x0
global __CFG_MCLRE$EXTMCLR
__CFG_MCLRE$EXTMCLR equ 0x0
global __CFG_STVREN$ON
__CFG_STVREN$ON equ 0x0
global __CFG_LVP$ON
__CFG_LVP$ON equ 0x0
global __CFG_XINST$OFF
__CFG_XINST$OFF equ 0x0
global __CFG_DEBUG$ON
__CFG_DEBUG$ON equ 0x0
global __CFG_CP0$OFF
__CFG_CP0$OFF equ 0x0
global __CFG_CP1$OFF
__CFG_CP1$OFF equ 0x0
global __CFG_CP2$OFF
__CFG_CP2$OFF equ 0x0
global __CFG_CP3$OFF
__CFG_CP3$OFF equ 0x0
global __CFG_CPB$OFF
__CFG_CPB$OFF equ 0x0
global __CFG_CPD$OFF
__CFG_CPD$OFF equ 0x0
global __CFG_WRT0$OFF
__CFG_WRT0$OFF equ 0x0
global __CFG_WRT1$OFF
__CFG_WRT1$OFF equ 0x0
global __CFG_WRT2$OFF
__CFG_WRT2$OFF equ 0x0
global __CFG_WRT3$OFF
__CFG_WRT3$OFF equ 0x0
global __CFG_WRTC$OFF
__CFG_WRTC$OFF equ 0x0
global __CFG_WRTB$OFF
__CFG_WRTB$OFF equ 0x0
global __CFG_WRTD$OFF
__CFG_WRTD$OFF equ 0x0
global __CFG_EBTR0$OFF
__CFG_EBTR0$OFF equ 0x0
global __CFG_EBTR1$OFF
__CFG_EBTR1$OFF equ 0x0
global __CFG_EBTR2$OFF
__CFG_EBTR2$OFF equ 0x0
global __CFG_EBTR3$OFF
__CFG_EBTR3$OFF equ 0x0
global __CFG_EBTRB$OFF
__CFG_EBTRB$OFF equ 0x0
	file	"build\default\production\main.as"
	line	#
psect	cinit,class=CODE,delta=1,reloc=2
global __pcinit
__pcinit:
global start_initialization
start_initialization:

global __initialization
__initialization:
psect cinit,class=CODE,delta=1
global end_of_initialization,__end_of__initialization

;End of C runtime variable initialization code

end_of_initialization:
__end_of__initialization:
movlb 0
goto _main	;jump to C main() function
psect	cstackCOMRAM,class=COMRAM,space=1,noexec
global __pcstackCOMRAM
__pcstackCOMRAM:
?_SYSTEM_Initialize:	; 1 bytes @ 0x0
??_SYSTEM_Initialize:	; 1 bytes @ 0x0
?_delay_100ms:	; 1 bytes @ 0x0
??_delay_100ms:	; 1 bytes @ 0x0
?_PIN_MANAGER_Initialize:	; 1 bytes @ 0x0
??_PIN_MANAGER_Initialize:	; 1 bytes @ 0x0
?_OSCILLATOR_Initialize:	; 1 bytes @ 0x0
??_OSCILLATOR_Initialize:	; 1 bytes @ 0x0
?_main:	; 2 bytes @ 0x0
	ds   1
	global	delay_100ms@i
delay_100ms@i:	; 1 bytes @ 0x1
	ds   1
??_main:	; 1 bytes @ 0x2
	ds   1
	global	main@i
main@i:	; 1 bytes @ 0x3
	ds   1
;!
;!Data Sizes:
;!    Strings     0
;!    Constant    0
;!    Data        0
;!    BSS         0
;!    Persistent  0
;!    Stack       0
;!
;!Auto Spaces:
;!    Space          Size  Autos    Used
;!    COMRAM           95      4       4
;!    BANK0           160      0       0
;!    BANK1           256      0       0
;!    BANK2           256      0       0
;!    BANK3           256      0       0
;!    BANK4           256      0       0
;!    BANK5           256      0       0
;!    BANK6           256      0       0
;!    BANK7           256      0       0
;!    BANK8           256      0       0
;!    BANK9           256      0       0
;!    BANK10          256      0       0
;!    BANK11          256      0       0
;!    BANK12          256      0       0
;!    BANK13          256      0       0
;!    BANK14          256      0       0
;!    BANK15           56      0       0

;!
;!Pointer List with Targets:
;!
;!    None.


;!
;!Critical Paths under _main in COMRAM
;!
;!    _main->_delay_100ms
;!
;!Critical Paths under _main in BANK0
;!
;!    None.
;!
;!Critical Paths under _main in BANK1
;!
;!    None.
;!
;!Critical Paths under _main in BANK2
;!
;!    None.
;!
;!Critical Paths under _main in BANK3
;!
;!    None.
;!
;!Critical Paths under _main in BANK4
;!
;!    None.
;!
;!Critical Paths under _main in BANK5
;!
;!    None.
;!
;!Critical Paths under _main in BANK6
;!
;!    None.
;!
;!Critical Paths under _main in BANK7
;!
;!    None.
;!
;!Critical Paths under _main in BANK8
;!
;!    None.
;!
;!Critical Paths under _main in BANK9
;!
;!    None.
;!
;!Critical Paths under _main in BANK10
;!
;!    None.
;!
;!Critical Paths under _main in BANK11
;!
;!    None.
;!
;!Critical Paths under _main in BANK12
;!
;!    None.
;!
;!Critical Paths under _main in BANK13
;!
;!    None.
;!
;!Critical Paths under _main in BANK14
;!
;!    None.
;!
;!Critical Paths under _main in BANK15
;!
;!    None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;!
;!Call Graph Tables:
;!
;! ---------------------------------------------------------------------------------
;! (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;! ---------------------------------------------------------------------------------
;! (0) _main                                                 2     2      0      60
;!                                              2 COMRAM     2     2      0
;!                  _SYSTEM_Initialize
;!                        _delay_100ms
;! ---------------------------------------------------------------------------------
;! (1) _delay_100ms                                          2     2      0      30
;!                                              0 COMRAM     2     2      0
;! ---------------------------------------------------------------------------------
;! (1) _SYSTEM_Initialize                                    0     0      0       0
;!              _OSCILLATOR_Initialize
;!             _PIN_MANAGER_Initialize
;! ---------------------------------------------------------------------------------
;! (2) _PIN_MANAGER_Initialize                               0     0      0       0
;! ---------------------------------------------------------------------------------
;! (2) _OSCILLATOR_Initialize                                0     0      0       0
;! ---------------------------------------------------------------------------------
;! Estimated maximum stack depth 2
;! ---------------------------------------------------------------------------------
;!
;! Call Graph Graphs:
;!
;! _main (ROOT)
;!   _SYSTEM_Initialize
;!     _OSCILLATOR_Initialize
;!     _PIN_MANAGER_Initialize
;!   _delay_100ms
;!

;! Address spaces:

;!Name               Size   Autos  Total    Cost      Usage
;!BITCOMRAM           5F      0       0       0        0.0%
;!EEDATA             400      0       0       0        0.0%
;!NULL                 0      0       0       0        0.0%
;!CODE                 0      0       0       0        0.0%
;!COMRAM              5F      4       4       1        4.2%
;!STACK                0      0       0       2        0.0%
;!DATA                 0      0       0       3        0.0%
;!BITBANK0            A0      0       0       4        0.0%
;!BANK0               A0      0       0       5        0.0%
;!BITBANK1           100      0       0       6        0.0%
;!BANK1              100      0       0       7        0.0%
;!BITBANK2           100      0       0       8        0.0%
;!BANK2              100      0       0       9        0.0%
;!BITBANK3           100      0       0      10        0.0%
;!BANK3              100      0       0      11        0.0%
;!ABS                  0      0       0      12        0.0%
;!BITBANK4           100      0       0      13        0.0%
;!BANK4              100      0       0      14        0.0%
;!BITBANK5           100      0       0      15        0.0%
;!BANK5              100      0       0      16        0.0%
;!BITBANK6           100      0       0      17        0.0%
;!BANK6              100      0       0      18        0.0%
;!BITBANK7           100      0       0      19        0.0%
;!BANK7              100      0       0      20        0.0%
;!BITBANK8           100      0       0      21        0.0%
;!BANK8              100      0       0      22        0.0%
;!BITBANK9           100      0       0      23        0.0%
;!BANK9              100      0       0      24        0.0%
;!BITBANK10          100      0       0      25        0.0%
;!BANK10             100      0       0      26        0.0%
;!BITBANK11          100      0       0      27        0.0%
;!BANK11             100      0       0      28        0.0%
;!BITBANK12          100      0       0      29        0.0%
;!BANK12             100      0       0      30        0.0%
;!BITBANK13          100      0       0      31        0.0%
;!BANK13             100      0       0      32        0.0%
;!BITBANK14          100      0       0      33        0.0%
;!BANK14             100      0       0      34        0.0%
;!BITBANK15           38      0       0      35        0.0%
;!BANK15              38      0       0      36        0.0%
;!BIGRAM             F37      0       0      37        0.0%
;!BITSFR_1             0      0       0      40        0.0%
;!SFR_1                0      0       0      40        0.0%
;!BITSFR               0      0       0      40        0.0%
;!SFR                  0      0       0      40        0.0%

	global	_main

;; *************** function _main *****************
;; Defined at:
;;		line 60 in file "main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    3[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;                  2   10[None  ] int 
;; Registers used:
;;		wreg, status,2, status,0, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_SYSTEM_Initialize
;;		_delay_100ms
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	text0,class=CODE,space=0,reloc=2
	file	"main.c"
	line	60
global __ptext0
__ptext0:
psect	text0
	file	"main.c"
	line	60
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:
;incstack = 0
	opt	stack 29
	line	62
	
l702:
	movlw	low(0)
	movwf	((c:main@i)),c
	line	64
	
l704:
	call	_SYSTEM_Initialize	;wreg free
	line	66
	
l706:
	movlw	low(0)
	movwf	((c:main@i)),c
	
l708:
		movlw	064h-1
	cpfsgt	((c:main@i)),c
	goto	u41
	goto	u40

u41:
	goto	l712
u40:
	goto	l716
	
l710:
	goto	l716
	line	67
	
l11:
	line	68
	
l712:
	movlw	104
movwf	(??_main+0+0)&0ffh,c,f
	movlw	228
u87:
decfsz	wreg,f
	goto	u87
	decfsz	(??_main+0+0)&0ffh,c,f
	goto	u87
	nop2

	line	66
	
l714:
	incf	((c:main@i)),c
		movlw	064h-1
	cpfsgt	((c:main@i)),c
	goto	u51
	goto	u50

u51:
	goto	l712
u50:
	goto	l716
	
l12:
	goto	l716
	line	71
	
l13:
	goto	l716
	line	73
	
l14:
	
l716:
	btfsc	((c:3978)),c,0	;volatile
	goto	u61
	goto	u60
u61:
	movlw	1
	goto	u66
u60:
	movlw	0
u66:
	xorlw	0ffh
	movwf	(??_main+0+0)&0ffh,c
	movf	((c:3978)),c,w	;volatile
	xorwf	((??_main+0+0)),c,w
	andlw	not ((1<<1)-1)
	xorwf	((??_main+0+0)),c,w
	movwf	((c:3978)),c	;volatile
	goto	l16
	
l15:
	line	74
	
l16:
	btfsc	((c:3978)),c,1	;volatile
	goto	u71
	goto	u70
u71:
	movlw	1
	goto	u76
u70:
	movlw	0
u76:
	xorlw	0ffh
	movwf	(??_main+0+0)&0ffh,c
	rlncf	((??_main+0+0)),c
	movf	((c:3978)),c,w	;volatile
	xorwf	((??_main+0+0)),c,w
	andlw	not (((1<<1)-1)<<1)
	xorwf	((??_main+0+0)),c,w
	movwf	((c:3978)),c	;volatile
	goto	l718
	
l17:
	line	75
	
l718:
	call	_delay_100ms	;wreg free
	goto	l716
	line	76
	
l18:
	line	71
	goto	l716
	
l19:
	line	78
;	Return value of _main is never used
	
l20:
	global	start
	goto	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
	signat	_main,90
	global	_delay_100ms

;; *************** function _delay_100ms *****************
;; Defined at:
;;		line 80 in file "main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text1,class=CODE,space=0,reloc=2
	line	80
global __ptext1
__ptext1:
psect	text1
	file	"main.c"
	line	80
	global	__size_of_delay_100ms
	__size_of_delay_100ms	equ	__end_of_delay_100ms-_delay_100ms
	
_delay_100ms:
;incstack = 0
	opt	stack 30
	line	82
	
l692:
	movlw	low(0)
	movwf	((c:delay_100ms@i)),c
	line	84
	movlw	low(0)
	movwf	((c:delay_100ms@i)),c
	
l694:
		movlw	0Ah-1
	cpfsgt	((c:delay_100ms@i)),c
	goto	u21
	goto	u20

u21:
	goto	l698
u20:
	goto	l25
	
l696:
	goto	l25
	line	85
	
l23:
	line	86
	
l698:
	movlw	104
movwf	(??_delay_100ms+0+0)&0ffh,c,f
	movlw	228
u97:
decfsz	wreg,f
	goto	u97
	decfsz	(??_delay_100ms+0+0)&0ffh,c,f
	goto	u97
	nop2

	line	84
	
l700:
	incf	((c:delay_100ms@i)),c
		movlw	0Ah-1
	cpfsgt	((c:delay_100ms@i)),c
	goto	u31
	goto	u30

u31:
	goto	l698
u30:
	goto	l25
	
l24:
	line	88
	
l25:
	return	;funcret
	opt stack 0
GLOBAL	__end_of_delay_100ms
	__end_of_delay_100ms:
	signat	_delay_100ms,89
	global	_SYSTEM_Initialize

;; *************** function _SYSTEM_Initialize *****************
;; Defined at:
;;		line 111 in file ".\mcc_generated_files\mcc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, status,2, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_OSCILLATOR_Initialize
;;		_PIN_MANAGER_Initialize
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text2,class=CODE,space=0,reloc=2
	file	".\mcc_generated_files\mcc.c"
	line	111
global __ptext2
__ptext2:
psect	text2
	file	".\mcc_generated_files\mcc.c"
	line	111
	global	__size_of_SYSTEM_Initialize
	__size_of_SYSTEM_Initialize	equ	__end_of_SYSTEM_Initialize-_SYSTEM_Initialize
	
_SYSTEM_Initialize:
;incstack = 0
	opt	stack 29
	line	115
	
l690:
	call	_PIN_MANAGER_Initialize	;wreg free
	line	116
	call	_OSCILLATOR_Initialize	;wreg free
	line	121
	
l40:
	return	;funcret
	opt stack 0
GLOBAL	__end_of_SYSTEM_Initialize
	__end_of_SYSTEM_Initialize:
	signat	_SYSTEM_Initialize,89
	global	_PIN_MANAGER_Initialize

;; *************** function _PIN_MANAGER_Initialize *****************
;; Defined at:
;;		line 51 in file ".\mcc_generated_files\pin_manager.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_SYSTEM_Initialize
;; This function uses a non-reentrant model
;;
psect	text3,class=CODE,space=0,reloc=2
	file	".\mcc_generated_files\pin_manager.c"
	line	51
global __ptext3
__ptext3:
psect	text3
	file	".\mcc_generated_files\pin_manager.c"
	line	51
	global	__size_of_PIN_MANAGER_Initialize
	__size_of_PIN_MANAGER_Initialize	equ	__end_of_PIN_MANAGER_Initialize-_PIN_MANAGER_Initialize
	
_PIN_MANAGER_Initialize:
;incstack = 0
	opt	stack 29
	line	56
	
l684:
	movlw	low(0)
	movwf	((c:3981)),c	;volatile
	line	57
	movlw	low(0)
	movwf	((c:3980)),c	;volatile
	line	58
	movlw	low(0)
	movwf	((c:3977)),c	;volatile
	line	59
	movlw	low(0)
	movwf	((c:3978)),c	;volatile
	line	60
	movlw	low(0)
	movwf	((c:3979)),c	;volatile
	line	65
	movlw	low(03h)
	movwf	((c:3990)),c	;volatile
	line	66
	movlw	low(0C0h)
	movwf	((c:3986)),c	;volatile
	line	67
	movlw	low(0F8h)
	movwf	((c:3987)),c	;volatile
	line	68
	movlw	low(082h)
	movwf	((c:3988)),c	;volatile
	line	69
	movlw	low(0E3h)
	movwf	((c:3989)),c	;volatile
	line	74
	movlw	low(0)
	movlb	15	; () banked
	movwf	((3898))&0ffh	;volatile
	line	75
	movlw	low(0)
	movlb	15	; () banked
	movwf	((3897))&0ffh	;volatile
	line	76
	movlw	low(04h)
	movlb	15	; () banked
	movwf	((3899))&0ffh	;volatile
	line	77
	movlw	low(0)
	movlb	15	; () banked
	movwf	((3900))&0ffh	;volatile
	line	78
	movlw	low(0)
	movlb	15	; () banked
	movwf	((3896))&0ffh	;volatile
	line	83
	movlw	low(0C0h)
	movwf	((c:3937)),c	;volatile
	line	84
	
l686:; BSR set to: 15

	bcf	((c:4081)),c,7	;volatile
	line	91
	
l83:; BSR set to: 15

	return	;funcret
	opt stack 0
GLOBAL	__end_of_PIN_MANAGER_Initialize
	__end_of_PIN_MANAGER_Initialize:
	signat	_PIN_MANAGER_Initialize,89
	global	_OSCILLATOR_Initialize

;; *************** function _OSCILLATOR_Initialize *****************
;; Defined at:
;;		line 123 in file ".\mcc_generated_files\mcc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_SYSTEM_Initialize
;; This function uses a non-reentrant model
;;
psect	text4,class=CODE,space=0,reloc=2
	file	".\mcc_generated_files\mcc.c"
	line	123
global __ptext4
__ptext4:
psect	text4
	file	".\mcc_generated_files\mcc.c"
	line	123
	global	__size_of_OSCILLATOR_Initialize
	__size_of_OSCILLATOR_Initialize	equ	__end_of_OSCILLATOR_Initialize-_OSCILLATOR_Initialize
	
_OSCILLATOR_Initialize:; BSR set to: 15

;incstack = 0
	opt	stack 29
	line	126
	
l688:
	movlw	low(030h)
	movwf	((c:4051)),c	;volatile
	line	128
	movlw	low(04h)
	movwf	((c:4050)),c	;volatile
	line	130
	movlw	low(0)
	movwf	((c:3995)),c	;volatile
	line	134
	goto	l43
	
l44:
	line	136
	
l43:
	line	134
	btfss	c:(32407/8),(32407)&7	;volatile
	goto	u11
	goto	u10
u11:
	goto	l43
u10:
	goto	l46
	
l45:
	line	137
	
l46:
	return	;funcret
	opt stack 0
GLOBAL	__end_of_OSCILLATOR_Initialize
	__end_of_OSCILLATOR_Initialize:
	signat	_OSCILLATOR_Initialize,89
	GLOBAL	__activetblptr
__activetblptr	EQU	0
	psect	intsave_regs,class=BIGRAM,space=1,noexec
	PSECT	rparam,class=COMRAM,space=1,noexec
	GLOBAL	__Lrparam
	FNCONF	rparam,??,?
	GLOBAL	___rparam_used
	___rparam_used EQU 1
	GLOBAL	___param_bank
	___param_bank EQU 16
GLOBAL	__Lparam, __Hparam
GLOBAL	__Lrparam, __Hrparam
__Lparam	EQU	__Lrparam
__Hparam	EQU	__Hrparam
	end
